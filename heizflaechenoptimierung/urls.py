from django.conf.urls import url
import heizflaechenoptimierung.views as views

urlpatterns = [
        url(r'^m_raum/(?P<raumid>\d+)/heizflaechenoptimierung/regelstrategie/$', views.get_regelstrategie),
        ]
