# coding=utf-8

from heizmanager.render import render_response, render_redirect
from heizmanager.models import Raum, Haus, Regelung
from django.http import HttpResponse
import os
import subprocess
from heizmanager.abstracts.base_mode import BaseMode


def get_name():
    return u'Homekit (Apple)'


def is_togglable():
    return True


def get_cache_ttl():
    return 3600


def calculate_always_anew():
    return False


def is_hidden_offset():
    return False


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/homebridge/'>Apple Homekit</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Bedienung über Apple Homekit."
    desc_link = "https://support.controme.com/apple-home-integration/"
    return desc, desc_link


def get_mac(username):
    """
    This method takes the mac adress from the username and calculates the new number to atteched
    :param username: line in config.json
    :return: the mac-adr which is the actual username, and the already attached number + 1
    """
    start = 21  # place in string username, where the mac address begins
    if username[start:start+8] == 'B8:27:EB' or username[start:start+8] == 'DC:A6:32':
        mac = username[start:start + 17]
        mac_all = username[start:-3]
        attached_number = username[start + 18:-3]
        if attached_number == '':
           attached_number = "0"
        attach_number = int(attached_number) + 1
        info = (mac, attach_number, mac_all)
        return info


def reset_pairing():
    """
    This method reset pairing/accessories and change username in config.json
    to connect the rpi to another Home account.
    """
    # 1) delete -hombridge/persists and .homebridge/accessories
    subprocess.call(['rm', '-r', '/home/pi/.homebridge/persist'])
    subprocess.call(['rm', '-r', '/home/pi/.homebridge/accessories'])

    # 2) Change username in config.json
    subprocess.call(['touch', 'newconfig.json'], cwd="/home/pi/.homebridge")
    username = subprocess.check_output(['grep', 'username', 'config.json'], cwd="/home/pi/.homebridge")
    info = get_mac(username)
    mac = info[0]
    attach_number = info[1]
    mac_all = info[2]
    file_ = open("/home/pi/.homebridge/newconfig.json", "w")
    subprocess.call(['sed', 's/' + str(mac_all) + '/' + str(mac) + ':' + str(attach_number) + '/g',
                    '/home/pi/.homebridge/config.json'], stdout=file_)
    subprocess.call(['mv', '-f', '/home/pi/.homebridge/newconfig.json', '/home/pi/.homebridge/config.json'])


def get_global_settings_page(request, haus, action=None, entityid=None):
    if not isinstance(haus, Haus):
        try:
           haus = Haus.objects.get(pk=long(haus))
        except Haus.DoesNotExist:
           return HttpResponse("Error", status=400)

    module_params = haus.get_spec_module_params('homebridge')

    if request.method == "GET":
        context = {'haus': haus, 'm_params': module_params}
        return render_response(request, "m_settings_homebridge.html", context)

    elif request.method == "POST":

        module_params['temperature_type'] = request.POST.get('temperature_type')
        module_params['duration'] = int(request.POST.get('duration', 300))
        haus.set_spec_module_params('homebridge', module_params)
        reset_pairing()
        return render_redirect(request, '/m_setup/%s/homebridge/' % haus.id)


def set_jsonapi(data, haus, usr, entityid):

    module_params = haus.get_spec_module_params('homebridge')
    temperature_type = module_params.get('temperature_type', 'permanent')
    # get room
    try:
        raum = Raum.objects.get(pk=long(entityid))
    except (Raum.DoesNotExist, TypeError):
        return {'message': 'not object for given entity id'}

    # temperature
    temperature = data.get('temperature', None)
    if temperature is not None:
        try:
            temperature = float(temperature)
        except ValueError:
            return {'message': 'error in setting temperature'}

    # decide what to set, base on data entry
    if temperature_type == 'permanent':
        ret = BaseMode.get_active_mode_raum(raum)
        if ret:
            ret['activated_mode'].set_mode_temperature_raum(raum, temperature)
        else:
            raum.solltemp = temperature
            raum.save()
        return {'message': 'done'}

    elif temperature_type == 'temporal':
        duration = module_params.get('duration', 300)
        from quickui.views import QuickuiMode
        q_mode = QuickuiMode(temperature, duration)
        q_mode.set_mode_raum(raum)
        return {'message': 'done'}

    return dict()


def get_jsonapi(haus, usr, entityid=None):

    module_params = haus.get_spec_module_params('homebridge')
    temperature_mode = module_params.get('temperature_type', 'temporal')
    duration = module_params.get('duration', 180)
    if temperature_mode == 'permanent':
        return dict(temperature_mode='permanent', duration=duration)
    else:
        return dict(temperature_mode='temporary', duration=duration)
