from heizmanager.models import sig_get_outputs_from_regelung, sig_get_possible_outputs_from_regelung, sig_create_regelung, sig_create_output_regelung, Raum
from django.dispatch import receiver
import vorlauftemperaturregelung.views as mr
import heizmanager.cache_helper as ch
import logging


@receiver(sig_get_outputs_from_regelung)
def mr_get_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']
    if raum is not None:
        return {}
    regelung = kwargs['regelung']
    return mr.get_outputs(haus, raum, regelung)


@receiver(sig_get_possible_outputs_from_regelung)
def mr_get_possible_outputs(sender, **kwargs):
    haus = kwargs['haus']
    return mr.get_possible_outputs(haus)


@receiver(sig_create_regelung)
def mr_create_regelung(sender, **kwargs):
    # called when a sensor is added or deleted and requires changes to a regelung
    # currently not necessary here
    pass


@receiver(sig_create_output_regelung)
def mr_create_output(sender, **kwargs):
    logging.warning("mr_create_output: %s %s" % (sender, str(kwargs)))
    regelung = kwargs['regelung']
    if regelung.regelung == 'vorlauftemperaturregelung':
        outs = kwargs['outs']
        ctrldevice = kwargs['ctrldevice']
        haus = ctrldevice.haus
        params = haus.get_module_parameters()
        params.setdefault('vorlauftemperaturregelung', dict())

        if sender == 'delete':
            for mid, values in params.get('vorlauftemperaturregelung', dict()).items():
                if values.get('hrgw') == ctrldevice.id:
                    outputs = values.get('outputs', dict())
                    _newouts = dict()
                    for name, _outs in outputs.items():
                        _newouts[name] = list(set(_outs)-set(outs))
                    logging.warning("mr_co _newouts: %s" % str(_newouts))
                    params['vorlauftemperaturregelung'][mid]['outputs'] = _newouts

        elif sender == 'create':
            sensor = kwargs['sensor']
            mid = sensor.split('_')[1]

            params['vorlauftemperaturregelung'].setdefault(mid, dict())

            output_type = params['vorlauftemperaturregelung'][mid].get('output_type')
            params['vorlauftemperaturregelung'][mid].setdefault('outputs', dict())
            if output_type == 'analog':
                if sensor.endswith('ana'):
                    params['vorlauftemperaturregelung'][mid]['outputs']['ana'] = [int(o) for o in outs]
                else:
                    pass
            elif output_type == 'digital':
                if sensor.endswith('don'):
                    params['vorlauftemperaturregelung'][mid]['outputs']['don'] = [int(o) for o in outs]
                elif sensor.endswith('doff'):
                    params['vorlauftemperaturregelung'][mid]['outputs']['doff'] = [int(o) for o in outs]
                else:
                    pass
            else:  # output_type is None
                return

        haus.set_module_parameters(params)

