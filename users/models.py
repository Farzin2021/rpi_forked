from django.db import models
from heizmanager import cache_helper as ch
from heizmanager.models import Haus
from django.contrib.auth.signals import user_logged_in
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from caching.base import CachingManager, CachingMixin
import ast


class UserProfile(CachingMixin, models.Model):
    ROLE_CHOICES = (
        ('A', 'Admin'),
        ('M', 'Mitarbeiter'),
        ('P', 'Partner'),
        ('K', 'Kunde'),
    )
    user = models.ForeignKey(User, related_name='userprofile')
    vorname = models.CharField(max_length=30)
    nachname = models.CharField(max_length=30)
    role = models.CharField(max_length=1, choices=ROLE_CHOICES)
    parameters = models.TextField(null=True)
    remote_id = models.BigIntegerField(default=0)
    remote_user_id = models.BigIntegerField(default=0)

    objects = CachingManager()

    def save(self, *args, **kwargs):
        super(UserProfile, self).save(*args, **kwargs)
        ch.set('profile'+str(self.user.id), self)

    def delete(self, *args, **kwargs):
        ch.delete('profile' + str(self.user.id))
        for haus in Haus.objects.all():
            if self.user.id in haus.besitzer:
                haus.besitzer.remove(self.user.id)
            if haus.eigentuemer == self.user:
                haus.eigentuemer = None
            haus.save()
        UserProfile.objects.filter(pk=self.pk).delete()

    def get_parameters(self):
        try:
            return ast.literal_eval(self.parameters)
        except (ValueError, SyntaxError):
            return {}

    def set_parameters(self, params):
        self.parameters = str(params)
        self.save()
        return True

    class Meta:
        app_label = 'users'


@receiver(post_save, sender=User)
def create_user_profile_handler(sender, **kwargs):
    # legt ein userprofile an, wenn ein benutzer angelegt wurde
    if kwargs['created']:
        try:
            profile = UserProfile.objects.get(user=kwargs['instance'])
        except UserProfile.DoesNotExist:
            profile = UserProfile(user=kwargs['instance'], role='K')
            profile.save()
            ch.set('profile'+str(kwargs['instance'].id), profile)
