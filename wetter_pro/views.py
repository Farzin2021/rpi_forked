
# -*- coding: utf-8 -*-

import logging
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Raum, Haus, Regelung
import heizmanager.cache_helper as ch
import pytz
from datetime import timedelta, datetime
from django.http import HttpResponse
from heizmanager import network_helper
import json


def get_name():
    return u'WetterPro'


def is_togglable():
    return True


def get_cache_ttl():
    return 3600


def calculate_always_anew():
    return False


def is_hidden_offset():
    return False


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/wetter_pro/'>Wetter-Pro</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Macht fast alle denkbaren Regelungen mit Wettervorhersage möglich."
    desc_link = "http://support.controme.com/wetter-pro/"
    return desc, desc_link


def ret_values(wetter_value, f_list):
    """
    :param int: number that given respective value of Wetter-Wert
    :param list: forecast list
    :return: a tuple (value, hour) list of respective wetter-wert selected
    """
    values = []

    # check if f_list has old version of data
    if not f_list or any('FCTTIME' in v for v in enumerate(f_list)):
        return []

    if wetter_value == 1: # Temperatur /temperature ['temp']["metric"]
        values = [(v['tt_C'], v['periodname']) for i, v in enumerate(f_list) if v is not None]

    if wetter_value == 2: # Taupunkt in °C / dewpoint in °C: v['dewpoint']["metric"]
        values = [('0', v['periodname']) for i, v in enumerate(f_list) if v is not None]

    if wetter_value == 3: # v['dewpoint']["english"]
        values = [('0', v['periodname']) for i, v in enumerate(f_list) if v is not None]

    if wetter_value == 4: # Bedeckungsgrad in % / cloud cover in % ['sky']
        for i, v in enumerate(f_list):
            if v is not None:
                cloud = v['wm'][0:2]
                percent = '0'
                if cloud == 'bd' or cloud == 'md': # Overcast sky
                    percent = '100'
                elif cloud == 'bw' or cloud == 'mw': # Cloudy
                    percent = '75'
                elif cloud == 'wb' or cloud == 'mb': # Various clouds
                    percent = '50'
                elif cloud == 'ms' or cloud == 'mm': # Mostly sunny
                    percent = '25'
                values.append((percent, v['periodname']))

    if wetter_value == 5: # UV-Index v['uvi']
        values = [('0', v['periodname']) for i, v in enumerate(f_list) if v is not None]

    if wetter_value == 6: # Luftfeuchtigkeit in % / humidity in % ['humidity']
        values = [(v['rh'], v['periodname']) for i, v in enumerate(f_list) if v is not None]

    if wetter_value == 7: # Regen in mm nächste 3h / rain in mm next 3h ['qpf']['metric']
        values = [(v['rr_mm'], v['periodname']) for i, v in enumerate(f_list) if v is not None]

    if wetter_value == 8: # Regenwahrscheinlichkeit / Probability of Precipitation in %
        values = [(v['pop'], v['periodname']) for i, v in enumerate(f_list) if v is not None]

    if wetter_value == 9: # Luftdruck in hPa / air pressure hPa v['mslp']['metric']
        values = [(v['pp_hpa'], v['periodname']) for i, v in enumerate(f_list) if v is not None]

    return values


def median(lst):
    sortedLst = sorted(lst)
    lstLen = len(lst)
    index = (lstLen - 1) // 2

    if (lstLen % 2):
        return sortedLst[index]
    else:
        return (sortedLst[index] + sortedLst[index + 1]) / 2.0


def avg(lst):
    return sum(lst) / len(lst)


def avg_without_min(lst):
    _min = min(lst)
    lst = filter(lambda d: d != _min, lst)
    if not len(lst):
        return _min
    else:
        return avg(lst)


def avg_without_max(lst):
    _max = max(lst)
    lst = filter(lambda d: d != _max, lst)
    if not len(lst):
        return _max
    else:
        return avg(lst)


def avg_without_min_max(lst):
    _max = max(lst)
    _min = min(lst)
    ret_lst = filter(lambda d: d != _max and d != _min, lst)
    if not len(ret_lst):
        return avg(lst)
    else:
        return avg(ret_lst)


def respective_func(val):

    func_dict = {
        1: min,
        2: max,
        3: median,
        4: avg,
        5: avg_without_min,
        6: avg_without_max,
        7: avg_without_min_max
    }
    return func_dict.get(int(val), None)


def calculation(lst, val):
    return respective_func(val)(lst) if len(lst) else None


def calculating(forecast_list, wetter_value, assign_value, period):
    """

    :param forecast_list: forecast list
    :param int: an int number value that is related to one of values of wetter-wert selectbox
    :param int: an int number value that is related to one of values of Verknüpfung  selectbox
    :param tuple(start,end): a period of hour that is related to slider range selected by user under Wetter-Zeitraum
    :return tuple(calculated_assign_result, limited list): returns a  tuple 0: calculated assign_reuslt from respective
    function, 1: a list that limited by period
    """

    if not forecast_list:
        return None, []
    # check there is at lease 49 element(-24,0,+24)
    if len(forecast_list) < 49:
        return None, []

    # period of slider range selected by user
    f_list = forecast_list[period[0] + 24: period[1] + 25]
    # remove the forecast we've have not had it yet
    f_list = filter(lambda x: x is not None, f_list)
    # fill a list of wetter-wert value in forecast
    wetter_values_list = ret_values(int(wetter_value), f_list)

    if f_list and wetter_values_list and assign_value:
        # remove hours from list
        wetter_values_list_for_calculation = list(zip(*wetter_values_list)[0])
        assign_result = calculation(map(float, wetter_values_list_for_calculation), assign_value)
    else:
        assign_result = None

    return assign_result, wetter_values_list


def get_assign_by_id(haus, w_id):
    """

    :param haus:
    :param w_id: wetter requested id
    :return: calculated assign value
    """
    params = haus.get_module_parameters()
    wetter_p = params.get("wetter_pro", {}).get('wetterpro_items', {})
    f_list = ch.get('weatherpro_updating_forecast_%s' % haus.id)
    wetter_value = wetter_p.get(w_id, {}).get('wetter_value', None)
    assign_value = wetter_p.get(w_id, {}).get('assign_value', None)
    period = wetter_p.get(w_id, {}).get('period_a', 0), wetter_p.get(w_id, {}).get('period_b', 0)
    calculated_result = None
    if wetter_value is not None and assign_value is not None:
        if f_list:
            calculated_result = calculating(f_list, wetter_value, assign_value, period)[0]
    return calculated_result


def get_wetter_values_list_by_id(haus, w_id):
    """
    :param haus:
    :param w_id: wetter requested id, these are saved in house params['wetter_pro']['wetterpro_items']
    :return: returns a list of forecast that is limited by user selected period
    """
    params = haus.get_module_parameters()
    wetter_p = params.get("wetter_pro", {}).get('wetterpro_items', {})
    period = wetter_p.get(w_id, {}).get('period_a', 0), wetter_p.get(w_id, {}).get('period_b', 0)
    f_list = ch.get('weatherpro_updating_forecast_%s' % haus.id)

    wetter_values_list = []
    if f_list:
        f_list = f_list[period[0] + 24: period[1]+25]
        wetter_value = wetter_p.get(w_id, {}).get('wetter_value', None)

        if wetter_value:
            wetter_values_list = ret_values(int(wetter_value), f_list)
    return wetter_values_list


def get_list_of_assign_values(haus):
    """
    :param haus:
    :return: returns a triple tuple list... 0: id,name, caculated assign value
    """
    params = haus.get_module_parameters()
    wetter_p = params.get("wetter_pro", {}).get('wetterpro_items', {})
    wetter_regs = []
    for w_id, param in wetter_p.items():
        calculated_result = get_assign_by_id(haus, w_id)
        wetter_regs.append((w_id, param['wetter_req_name'], calculated_result))
    wetter_regs.sort(key=lambda x: x[0])
    return wetter_regs


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        try:
            haus = Haus.objects.get(pk=long(haus))
        except Haus.DoesNotExist:
            return HttpResponse("Error", status=400)

    if request.method == "GET":
        if action == 'edit':
            params = haus.get_module_parameters()
            ret = {}
            ret['haus'] = haus
            if(entityid):
                w_id = int(entityid)
                ret['wid'] = w_id
                wetter_req_name = params.get("wetter_pro", {}).get('wetterpro_items', {})\
                    .get(int(entityid), {}).get('wetter_req_name', "")

                wetter_value = params.get("wetter_pro", {}).get('wetterpro_items', {}) \
                    .get(int(entityid), {}).get('wetter_value', "0")

                assign_value = params.get("wetter_pro", {}).get('wetterpro_items', {}) \
                    .get(int(entityid), {}).get('assign_value', "")

                period = params.get("wetter_pro", {}).get('wetterpro_items', {}) \
                    .get(int(entityid), {}).get('period_a', 0),\
                params.get("wetter_pro", {}).get('wetterpro_items', {}) \
                    .get(int(entityid), {}).get('period_b', 0),

                ret['wetter_req_name'] = wetter_req_name
                try:
                    ret['wetter_value'] = int(wetter_value)
                except ValueError as ex:
                    ret['wetter_value'] = 1

                try:
                    ret['assign_value'] = int(assign_value)
                except ValueError as ex:
                    ret['assign_value'] = 1
                
                ret['period'] = period

                calculated_result = get_assign_by_id(haus, w_id)
                wetter_values_list = get_wetter_values_list_by_id(haus, w_id)
                ret["wetter_values_list"] = ["%s (%s)" % (v, h) for v, h in wetter_values_list]
                ret["assign_result"] = calculated_result

            return render_response(request, "m_settings_wetterpro_edit.html", ret)

        elif action == 'delete':
            try:
                entityid = int(entityid)
            except TypeError:
                return HttpResponse(status=400)

            params = haus.get_module_parameters()
            wetter_items = params.get("wetter_pro", {}).get('wetterpro_items', {})
            if entityid in wetter_items:
                del wetter_items[entityid]
            else:
                return HttpResponse(status=400)

            params['wetter_pro']['wetterpro_items'] = wetter_items
            haus.set_module_parameters(params)
            ch.set('weatherpro_assign_values_%s' % haus.id, None, time=3601)
            return render_redirect(request, "/m_setup/%s/wetter_pro/" % haus.id)

        elif action == "weather_data":
            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)
            now_without_min = datetime(now.year, now.month, now.day, now.hour, 0)

            lastupdated = ch.get('weatherpro_lasttime_list_updated_%s' % haus.id)
            if lastupdated:
                lastupdated = lastupdated.strftime('%d.%m.%Y; %H:%M uhr')

            params = haus.get_module_parameters()
            is_active = params.get('wetter_pro', {}).get('is_active', False)
            f_list=[]
            if is_active:
                f_list = ch.get('weatherpro_updating_forecast_%s' % haus.id)
                if f_list and all('FCTTIME' not in v for v in enumerate(f_list)):
                    f_list = f_list[0:49]
                    has_bold = False
                    first_id = 0
                    for id, item in enumerate(f_list):

                        if not isinstance(item, dict):
                            continue

                        time = item['date'] + ' ' + item['periodname']
                        # time = datetime(int(time['year']), int(time['mon']), int(time['mday']),
                        #                 int(time['hour']), int(time['min']))
                        time = datetime.strptime(time, '%Y-%m-%d %H:%M')

                        f_list[id]['time'] = time.strftime("%d.%m.%Y %H:%M")
                        time_diff = (time - now_without_min.replace(tzinfo=None)).total_seconds() // 3600

                        if time_diff < 0:
                            f_list[id]['class'] = 'gray'
                        elif time_diff == 0:
                            f_list[id]['class'] = 'bold'
                            f_list[id]['jump'] = True
                            has_bold = True
                        else:
                            f_list[id]['class'] = 'upto24'
                            if first_id == 0:
                                first_id = id

                        f_list[id]['diff'] = int(time_diff)
                    if not has_bold:
                        f_list[first_id]['class'] = 'bold'
                        f_list[first_id]['jump'] = True

            return render_response(request, "m_settings_wetterpro_weatherdata.html",
                                   {'haus': haus, 'last_updated': lastupdated, 'f_list': f_list
                                    , 'is_active':is_active})

        elif action == "get_wetter_value_list":
            f_list = ch.get('weatherpro_updating_forecast_%s' % haus.id)
            ret = {}
            period = int(request.GET.get('period_a','0')), int(request.GET.get('period_b','0'))
            wetter_value = int(request.GET.get('wetter_value','1'))
            if f_list:
                ret_list = calculating(f_list, wetter_value, None, period)[1]
                ret["wetter_values_list"] = ret_list
            else:
                ret["message"] = "there is no forecast data"
            return HttpResponse(json.dumps(ret), content_type='application/json')

        elif action == "get_assign_value":
            f_list = ch.get('weatherpro_updating_forecast_%s' % haus.id)
            ret = {}
            period = int(request.GET.get('period_a','0')), int(request.GET.get('period_b','0'))
            assign_value = int(request.GET.get('assign_value','1'))
            wetter_value = int(request.GET.get('wetter_value','1'))
            if f_list:
                calculated = calculating(f_list, wetter_value, assign_value, period)[0]
                ret["assign_value"] = calculated
            else:
                ret["message"] = "there is no forecast data"
            return HttpResponse(json.dumps(ret), content_type='application/json')

        else:
            params = haus.get_module_parameters()
            is_active = params.get('wetter_pro', {}).get('is_active', False)
            is_there_address = False
            if 'address' in params:
                is_there_address = True

            context = {}
            context['is_active'] = is_active
            context['is_there_address'] = is_there_address

            context['haus'] = haus
            context['last_cals_updated'] = ch.get('weatherpro_lasttime_list_updated_%s' % haus.id)
            if context['last_cals_updated']:
                context['last_cals_updated'] = context['last_cals_updated'].strftime('%d.%m.%Y/ %H:%M')

            wetter_regs = ch.get('weatherpro_assign_values_%s' % haus.id)
            if wetter_regs is None:
                wetter_regs = get_list_of_assign_values(haus)
                ch.set('weatherpro_assign_values_%s' % haus.id, wetter_regs, time=3601)
            context['wetter_regs'] = wetter_regs
            return render_response(request, "m_settings_wetterpro.html", context)

    elif request.method == "POST":
        if action == "edit":
            params = haus.get_module_parameters()
            wetter_p = params.get("wetter_pro", {}).get('wetterpro_items', {})
            if entityid:
                key = int(entityid)
            else:
                try:
                    key = max(wetter_p, key=int) + 1
                except ValueError:
                    key = 1
                    if 'wetter_pro' not in params:
                        params['wetter_pro'] = {}

            if key not in wetter_p:
                wetter_p[key] = {}

            if 'wetter_req_name' not in wetter_p[key]:
                wetter_p[key] = {}

            wetter_p[key]['wetter_req_name'] = request.POST['wetter_req_name']
            wetter_p[key]['wetter_value'] = int(request.POST['wetter_value'])
            wetter_p[key]['assign_value'] = int(request.POST['assign_value'])
            wetter_p[key]['period_a'] = int(request.POST['perioda'])
            wetter_p[key]['period_b'] = int(request.POST['periodb'])
            params['wetter_pro']['wetterpro_items'] = wetter_p
            haus.set_module_parameters(params)
            ch.set('weatherpro_assign_values_%s' % haus.id, None, time=3601)
            return render_redirect(request, "/m_setup/%s/wetter_pro/" % haus.id)

        elif action == "get_update":
            params = haus.get_module_parameters()
            is_active = params.get("wetter_pro", {}).get('is_active', False)
            ret = {}
            ret["refresh"] = False
            if not is_active:
                ret["success"] = False
                ret["message"] = 'deaktivieren' # module is deactive
            else:
                from tasks import periodic_calculation
                f_list = ch.get('weatherpro_updating_forecast_%s' % haus.id)
                if not f_list or all(v is None for v in f_list) or len(f_list)<50: # All elements in list are same
                    periodic_calculation()
                    f_list2 = ch.get('weatherpro_updating_forecast_%s' % haus.id)
                    if not f_list2 or all(v is None for v in f_list2):
                        ret["success"] = False
                    else:
                        ret["success"] = True
                        ret["refresh"] = True
                else:
                    ret["success"] = True
            return HttpResponse(json.dumps(ret), content_type='application/json')

        return render_redirect(request, '/config')


def get_global_settings_page_help(request, haus):
    pass


def get_local_settings_page_haus(request, haus):
    pass


def get_local_settings_link(request, raum):
    pass


def get_local_settings_page(request, raum):
    pass


def get_offset(haus, raum=None):
    offset = 0
    return offset


def get_module_variables(haus):
    variables_list = []
    wetters = get_list_of_assign_values(haus)
    if wetters is None:
        wetters = []
    wetter_regs = []
    for w_id, name, calculated_result in wetters:
        wetter_regs.append((w_id, name, calculated_result))

    variables_list.append({'module_name': 'wetterpro', 'variables': wetter_regs, 'verbose_module_name': get_name()})

    return variables_list


def deactivate(hausoderraum):
    mods = hausoderraum.get_modules()
    try:
        mods.remove('wetter')
        hausoderraum.set_modules(mods)
    except ValueError:  # nicht in modules
        pass


def activate(hausoderraum):
    if not 'wetter' in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['wetter'])
