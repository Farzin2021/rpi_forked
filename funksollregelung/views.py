# -*- coding: utf-8 -*-

import heizmanager.cache_helper as ch
from heizmanager.render import render_response, render_redirect
import logging
from heizmanager.models import AbstractSensor, Raum
from heizmanager.mobile.m_temp import LocalPage, get_module_offsets, get_offsets_icon
from rf.models import RFAktor, RFAusgang
from heizmanager.models import sig_get_outputs_from_output, sig_get_possible_outputs_from_output, sig_get_ctrl_devices
from django.http import HttpResponse
from itertools import chain
from heizmanager.abstracts.base_mode import BaseMode

logger = logging.getLogger("logmonitor")


def get_name():
    return u"Funksollregelung"


def managesoutputs():
    return False


def is_hidden_offset():
    return True


def do_rfausgang(regelung, controllerid):
    # aktor_id und value kommen aus dem JSON, das /set/ bekommt

    regparams = regelung.get_parameters()
    raum = regelung.get_raum()

    mode = regparams.get('mode', 'auto')

    controller = hktlast = None

    if mode == 'auto':

        ret = dict()

        # damit wir bei mehreren HKT in einem Raum nur einmal rumtun müssen
        hkts = list(chain(RFAktor.objects.filter(raum=raum, type__startswith="hkt", controller_id=controllerid), RFAktor.objects.filter(raum=raum, type='wt', controller_id=controllerid)))
        if not hkts:
            return ret

        for hkt in hkts:
            ret[hkt.name] = dict()

        offset = zieloffset = 0.0
        try:
            mode = BaseMode.get_active_mode_raum(raum)
            raum.refresh_from_db()
        except Exception as e:
            logging.exception("fsr: exception getting mode for raum %s" % raum.id)
            mode = None
        if mode is None or mode.get('use_offsets', False):
            offset = get_module_offsets(raum, only_clear_offsets=False)
        else:
            offset = 0.0

        ziel = raum.solltemp + offset
        ms = raum.get_mainsensor()
        if ms:

            rsfaktor = regparams.get('rsfaktor', 0)
            schwelle = regparams.get('schwelle', 0.5)
            last = ms.get_wert()

            if last:
                try:
                    if ms in hkts:
                        zieloffset = 0.0
                    else:
                        zieloffset = _get_zieloffset(raum.id, ziel, last, rsfaktor, schwelle)
                except TypeError:
                    zieloffset = 0.0
                ziel += zieloffset
                ziel = round(ziel, 1)

            else:
                # wenn wir keinen Wert haben, koennen wir auch nichts machen
                pass

        logging.warning("fsr || ziel: %s; zieloffset: %s; raumsoll: %s; offset: %s" % (ziel, zieloffset, raum.solltemp, offset))

        for hkt in hkts:
            ret[hkt.name] = {'soll': raum.solltemp, 'offset': offset, 'zieloffset': zieloffset}

        logging.warning("get_ret returning %s for raum %s" % (str(ret), raum.name))

        return ret

    elif mode == 'dvc':

        hkts = RFAktor.objects.filter(raum=raum, type__startswith="hkt", controller_id=controllerid)
        ms = raum.get_mainsensor()
        if ms:
            last = ms.get_wert()
            try:
                mode = BaseMode.get_active_mode_raum(raum)
                raum.refresh_from_db()
            except Exception as e:
                logging.exception("fsr: exception getting mode for raum %s" % raum.id)
                mode = None
            if mode is None or mode.get('use_offsets', False):
                offset = get_module_offsets(raum, only_clear_offsets=False)
            else:
                offset = 0.0

            zieloffset = 0.0
            if last:
                rsfaktor = regparams.get('rsfaktor', 0)
                schwelle = regparams.get('schwelle', 0.5)
                try:
                    if ms in hkts:
                        zieloffset = 0.0
                    else:
                        zieloffset = _get_zieloffset(raum.solltemp + offset, last, rsfaktor, schwelle)
                except TypeError:
                    zieloffset = 0.0

            if last and last[0] > raum.solltemp + offset + zieloffset:
                val = 0
            else:
                val = 255

        else:
            val = 0

        ret = dict()
        for hkt in hkts:
            ret[hkt.name] = dict()
            ret[hkt.name][0x40] = ("", "Direct Valve Control")
            ret[hkt.name][0x26] = ("Level", val)

        return ret

    else:
        return None


def _get_zieloffset(raum_id, ziel, last, rsfaktor, schwelle):
    if ziel < last[0]-0.1:
        zieloffset = round((ziel-last[0])*rsfaktor, 1)  # zo negativ
    elif ziel > last[0]+0.1:
        zieloffset = round((ziel-last[0])*rsfaktor, 1)  # zo positiv
    else:
        zieloffset = 0.0

    # workaround fuer dlc selbstverstellung / nicht-annahme von werten
    rfa = RFAktor.objects.filter(raum_id=raum_id).values_list("protocol", "type")
    if ("zwave", "hkt") in rfa and not (ziel+zieloffset) % 0.5:
        if ziel < last[0]:
            zieloffset -= 0.1
        else:
            zieloffset += 0.1

    deviation = ziel - last[0] if len(last) else -99
    # If deviation is more than this value, the module shall generate an fixed offset of +10K resp
    #  -10K instead of calculating with the other slider.
    if deviation > schwelle:
        zieloffset = 10.00

    if zieloffset <= -10:
        zieloffset = -10.0
    elif zieloffset >= 10:
        zieloffset = 10.0
    return zieloffset


def do_ausgang(regelung, ausgang):
    # damit in einem Raum gemischt FBH und Heizkörper sein können
    # und damit man FSR in Raumanforderung verwenden kann

    raum = regelung.get_raum()
    ms = raum.get_mainsensor()

    if ms is None:
        return 1

    else:
        last = ms.get_wert()
        if last:
            try:
                mode = BaseMode.get_active_mode_raum(raum)
                raum.refresh_from_db()
            except Exception as e:
                logging.exception("fsr: exception getting mode for raum %s" % raum.id)
                mode = None
            if mode is None or mode.get('use_offsets', False):
                offset = get_module_offsets(raum, only_clear_offsets=False)
            else:
                offset = 0.0

            ziel = raum.solltemp + offset
            if last[0] > ziel:
                return HttpResponse("<0>")
            else:
                return HttpResponse("<1>")
        else:
            return HttpResponse("<1>")


def get_config():
    # eine moeglichkeit, config parameter zurueckzugeben an rpi
    # in einem von get_ret() separaten aufruf
    # muessen wir schauen, ob wir das brauchen.
    pass


def get_outputs(haus, raum, regelung):

    belegte_ausgaenge = dict()
    ret = sig_get_outputs_from_output.send(sender='fsr', raum=raum, regelung=regelung)
    for cbfunc, outputs in ret:
        for gw, raum, regelung, ausgang, namen in outputs:
            ms = raum.get_mainsensor()
            if ms:
                belegte_ausgaenge.setdefault(gw, list())
                for name in namen:
                    belegte_ausgaenge[gw].append((name, ausgang, ms, True))

    for rfa in RFAktor.objects.filter(haus=haus, type__startswith='hkt', raum=raum):
        ms = raum.get_mainsensor()
        if ms:
            belegte_ausgaenge.setdefault(rfa.controller, list())
            belegte_ausgaenge[rfa.controller].append((rfa.name, None, ms, True))

    return belegte_ausgaenge


def get_possible_outputs(haus, raumdict=dict()):

    ret = sig_get_possible_outputs_from_output.send(sender='funksollregelung', haus=haus)
    ctrldevices = sig_get_ctrl_devices.send(sender='fsr', haus=haus)
    ctrldevices = [d for cbf, devs in ctrldevices for d in devs]
    regler = dict((cd, list()) for cd in ctrldevices)

    if not len(raumdict):
        raumdict = {}
        for etage in haus.etagen.all():
            for raum in Raum.objects.filter_for_user(haus.eigentuemer, etage_id=etage.id).values_list("id", "name"):
                raumdict[raum[0]] = u"%s/%s" % (etage.name, raum[1])

    for cbfunc, r2s in ret:
        for raum, sensoren in r2s.items():
            ms = raum.get_mainsensor()
            if ms:
                for sensor, ctrldevice in sensoren:
                    if ms == sensor:
                        regler[ctrldevice].append([ms.get_identifier(), u"%s %s" % (raumdict[raum.id], unicode(ms))])
                        for ctrld in ctrldevices:
                            if type(ctrld) != type(ctrldevice):
                                regler[ctrld].append([ms.get_identifier(), u"%s %s" % (raumdict[raum.id], unicode(ms))])

    return regler


def get_global_settings_link(request, haus):
    pass


def get_global_description_link():
    desc = None
    desc_link = None
    return desc, desc_link


def get_global_settings_page(request, haus):
    pass


def get_local_settings_link(request, raum):
    try:
        mode = BaseMode.get_active_mode_raum(raum)
        raum.refresh_from_db()
    except Exception as e:
        logging.exception("fsr: exception getting mode for raum %s" % raum.id)
        mode = None
    if mode is None or mode.get('use_offsets', False):
        offset = get_module_offsets(raum)
    else:
        offset = 0.0
    ziel = (raum.solltemp or 4.0) + offset
    ms = raum.get_mainsensor()
    if ms.get_identifier().find('RFAktor') != -1 and ms.type.startswith('hkt'):
        return
    
    regparams = raum.get_regelung().get_parameters()
    state = "0.00"
    if ms and regparams['rsactive']:

        rsfaktor = regparams.get('rsfaktor', 0)
        schwelle = regparams.get('schwelle', 0.5)
        last = ms.get_wert()

        if last:
            try:
                zieloffset = _get_zieloffset(raum.id, ziel, last, rsfaktor, schwelle)
            except TypeError:
                zieloffset = 0.0
            state = "%.2f" % zieloffset

    return "Zieltemperaturanpassung Funkregelung", state, 75, "/m_raum/%s/funksollregelung/" % raum.id


def get_local_settings_page(request, raum):

    regelung = raum.regelung
    regparams = regelung.get_parameters()


    if request.method == "GET":

        schwelle = regparams.get('schwelle', 0.5)
        f = regparams.get('rsfaktor', 2)
        has_stellaz = False
        try:
            rfausgang = RFAusgang.objects.get(regelung=regelung)
            for aktorid in rfausgang.aktor:
                aktor = RFAktor.objects.get(pk=long(aktorid))
                aparams = aktor.get_parameters()
                if aparams.get('manufacturer_id') == "0148" and aparams.get('product_type') == "0001" and \
                        aparams.get('product_id') == "0001":
                    has_stellaz = True
                    break
        except RFAusgang.DoesNotExist:
            pass

        # aktoren sind nicht notwendig ueber rfausgang zugeordnet
        for aktor in RFAktor.objects.filter(raum=raum):
            aparams = aktor.get_parameters()
            if aparams.get('manufacturer_id') == "0148" and aparams.get('product_type') == "0001" and \
                    aparams.get('product_id') == "0001":
                has_stellaz = True
                break

        fsr_mode = regparams.get('mode', 'auto')

        if not has_stellaz and fsr_mode == 'dvc':
            fsr_mode = 'auto'

        try:
            fsr_offset = get_local_settings_link(request, raum)[1]
        except:
            fsr_offset = "-"

        return render_response(request, "m_raum_fsr.html",
                               {"raum": raum, "f": f, "schwelle": schwelle, 'has_stellaz': has_stellaz, 'mode': fsr_mode, 'fsr_offset':fsr_offset})

    elif request.method == "POST":
        name = request.POST['name']
        if name == 'f':
            number = float(request.POST.get("number", 2))
            regparams["rsfaktor"] = number
        if name == 'schwelle':
            number = float(request.POST.get("number", 0.5))
            regparams["schwelle"] = number

        mode = request.POST.get('modus', 'auto')
        regparams['mode'] = mode

        raum.regelung.set_parameters(regparams)
        # return render_redirect(request, "/m_raum/%s/funksollregelung/" % raum.id)
        return HttpResponse()


def get_local_link(regelung, request, raum):
    pass


def get_local_page(regelung, request, raum):

    if request.method == "GET":

        if 'gets' in request.GET:

            sensoren = raum.get_sensor_overview()

            return render_response(request, 'm_raum_sensoren.html', {'raum': raum, 'sensoren': sensoren})

        else:

            lines = list()

            import importlib
            haus = raum.etage.haus
            modules = haus.get_modules()
            ch.delete("%s_roffsets_dict" % raum.id)
            try:
                mode = BaseMode.get_active_mode_raum(raum)
                raum.refresh_from_db()
            except Exception as e:
                logging.exception("fsr: exception getting mode for raum %s" % raum.id)
                mode = None
            if mode is None or mode.get('use_offsets', False):
                offsets = get_module_offsets(raum, only_clear_offsets=False)
            else:
                offsets = 0.0
            mods = list()
            for modulename in sorted(set(modules+['funksollregelung'])):
                module = None
                try:
                    module = importlib.import_module(modulename.strip() + '.views')
                except ImportError:
                    if modulename.strip() in ['module', 'setup_rpi']:
                        module = importlib.import_module('heizmanager.modules.' + modulename)
                try:
                    link = module.get_local_settings_link(request, raum)
                    if link:
                        mods.append(link)
                except (AttributeError, KeyError):
                    pass
            api_raum = ch.get("api_roomoffset_%s" % raum.id) or {}
            api_haus = ch.get("api_houseoffset_%s" % haus.id) or {}
            for mod in set(api_raum.keys() + api_haus.keys()):
                offset = api_raum.get(mod, 0.0) + api_haus.get(mod, 0.0)
                mods.append((mod, offset, 75, "#"))

            # ziel = (raum.solltemp or 21.0) + offsets
            # zieloffset = 0.0

            # ms = raum.get_mainsensor()
            # last = None
            # if ms:
            #     last = ms.get_wert()

                # if last:
                    # rsfaktor = raum.regelung.get_parameters().get('rsfaktor',0)
                    # schwelle = raum.regelung.get_parameters().get('schwelle', 0.5)
                    # try:
                    #     if ms.get_identifier().find('RFAktor') != -1:
                    #         zieloffset = 0.0
                    #     else:
                    #         zieloffset = _get_zieloffset(raum.id, ziel, last, rsfaktor, schwelle)
                    # except TypeError:
                    #     zieloffset = 0.0
                    # ziel += zieloffset
                    # ziel = round(ziel, 1)

                # else:
                    # wenn wir keinen Wert haben, koennen wir auch nichts machen
                    # pass

            # if last and last[0]:
            #     temp = "%.2f" % last[0]
            # else:
            #     temp = '-'

            offset = offsets  # + zieloffset offset should not be shown in summary
            return LocalPage(lines=lines, mods=mods, offset=offset)

    elif request.method == "POST" and Raum.objects.get_for_user(request.user, pk=raum.id):
        try:
            soll = float(request.POST['slidernumber'].replace(',', '.'))
            logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur auf %.2f gesetzt (F1)" % soll))
            raum.solltemp = soll
            raum.save()
        except ValueError:  # wilde Eingabe
            pass
        return render_redirect(request, '/m_raum/%s/' % raum.id)


def get_local_html(regelung, request, raum):

    try:
        mode = BaseMode.get_active_mode_raum(raum)
        raum.refresh_from_db()
    except Exception as e:
        logging.exception("rlr: exception getting mode for raum %s" % raum.id)
        mode = None

    if mode is None:
        offsets, icon = get_offsets_icon(raum)
    else:
        icon = mode['icon']
        if mode.get('use_offsets', False):
            offsets, icon = get_offsets_icon(raum)
        else:
            offsets = 0.0

    max_offset_module_html = ""
    if icon:
        max_offset_module_html = """<span class="module-iconn"><img src="/static/icons/%s.png" alt="icon"/></span>""" % icon

    haus = raum.etage.haus
    hmodules = haus.get_modules()
    hparams = haus.get_module_parameters()
    temp = None
    ms = raum.get_mainsensor()
    if ms:
        last = ms.get_wert()
        if last:
            temp = last[0]

    if temp is None:
        temp = '-'
    else:
        temp = '%.2f' % temp

    soll = raum.solltemp

    if soll is None:  # kann passieren
        soll = 0

    if offsets < 0:
        pom = "-"''
        offset_str = ' <span class="heat5_room_offset offstr blue-str" {color}>%05.2f</span>' % abs(offsets)
    else:
        pom = "+"
        offset_str = ' <span class="heat5_room_offset offstr red-str" {color}>%05.2f</span>' % offsets

    soll_offset_color = ""
    hand_time = "&nbsp"
    hand_picture = "<img src='/static/icons/icon-clock.png' width='30' height='30'>"
    mode_id = -99
    ziel_color = "#779A45"
    if mode is not None:
        hand_time = mode['activated_mode_text']
        mode_id = mode['activated_mode_id']
        if 'heizprogramm' not in hmodules:
            hand_picture = "&nbsp"
        else:
            hand_picture = "<img src='/static/icons/%s.png' width='30' height='30'>" % mode['icon']

        soll = mode.get('soll', 21.0)
        if mode_id == -1:
            soll_offset_color = " style='color: gray;'"
            ziel_color = "#d79a2d"

    else:
        if 'heizprogramm' not in hmodules or not hparams.get('is_switchingmode_enabled_%s' % haus.id, True):
            hand_picture = "&nbsp"

    ziel = float(soll) + float(offsets)

    try:
        if float(temp) > float(ziel):
            sign = "<"
        else:
            sign = ">"
    except:
        sign = "-"

    is_hide = ""
    from benutzerverwaltung.decorators import show_pro_visualbar
    if not show_pro_visualbar(request):
        is_hide = "hide"

    bis = ""
    if mode_id == -1:
        bis = "-"

    if len(hand_time) > 10:
        hand_time = hand_time[0:7] + "..."

    html = u'''<a data-role="none" href="/m_raum/{id}/" class="ui-link-inherit">
            <div class="main" data-id={room_id}>
              <div class="left_ui">
                  <div class="ui_details">
                    <span class="room_icon"><img src="/static/icons/{icon}.png" alt="icon"/></span>
                    <span class="room_name">{name}</span></div>
                    <input type='hidden' id="room_row_id" value={room_id}>
                    {html_slider}
                  <div class="ui_details">{embed_beam}</div>
              </div>
              <div class="right_ui">
                <div class="ui_details temperature">
                    <div class="details_left"><span class="soll" id="{room_id}" {color}>{soll}</span></div>
                    <div class="details_sign signed" id="pom-sign">{pom}</div>
                    <div class="details_right {color}" id="offset">{offset_str}</div>
                    <div class="details_leaf"></div>
                </div>
                <div class="ui_details temperature">
                    <div class="details_left"><span id="{room_id}" class="ziel" style="color:{ziel_color};">{ziel}</span></div>
                    <div class="details_sign"><f class="sign">{sign}</f></div>
                    <div class="details_right"><span class="temp bold underline">{temp}</span></div>
                    <div class="details_leaf">{max_offset_module}</span></div>
                </div>
                <div class="ui_details hand_click">
                    <div class="details_left hand" id="mode_icon">{hand_picture}</div>
                    <div class="details_sign" id="signـbis">{bis}</div>
                    <div class="details_right hand_time" id="mode_name" data-id="{mode_id}">{hand_time}</div>
                    <div class="details_leaf"></div>
                </div>
              </div>
            </div>
         '''
    if ziel < 10 or ziel > 35:
        html_slider = '''
            <div class="ui_details {hide}">
              <div class="range"><span>10</span></div>
              <div class="visualbar_slider">
                <div class="visual-bar">
                  <div class="slider-place">
                    <div role="application" class="ui-slider-track   ui-corner-all ui-mini" style="height:7px;">
                      <div class="ui-slider-bg ui-btn-active" style="width: {visual_temp}%;"></div>
                      <span id="vbar_soll_s2"  class="soll" role="slider" aria-valuemin="10" aria-valuemax="35"  style="left: {soll_converted}%;"></span>
                      {arrow_bar}
                    </div>
                  </div>
                </div>
              </div>
              <div class="range"><span>35</span></div>
            </div>
             '''
    else:
        html_slider = '''
                    <div class="ui_details {hide}">
                      <div class="range"><span>10</span></div>
                      <div class="visualbar_slider">
                        <div class="visual-bar">
                          <div class="slider-place">
                            <div role="application" class="ui-slider-track   ui-corner-all ui-mini" style="height:7px;">
                              <div class="ui-slider-bg ui-btn-active" style="width: {visual_temp}%;"></div>
                              <span id="vbar_soll_s2"  class="soll" role="slider" aria-valuemin="10" aria-valuemax="35"  style="left: {soll_converted}%;"></span>
                              <span id="vbar_ziel_s2"  class="ziel" role="slider" aria-valuemin="10" aria-valuemax="35"  style="left: {ziel_converted}%; background-color: {ziel_color};"></span>
                              {arrow_bar}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="range"><span>35</span></div>
                    </div>
                     '''

    html_beam = ""
    mvas = RFAktor.objects.filter(raum=raum, type="hkta52001")
    for mva in mvas:
        vals = ch.get_last_vals("%s_%s_val" % (mva.controller.name, mva.name))
        if isinstance(vals, tuple):
            cv = int(vals[0].get('CV', 0))
        else:
            cv = 0
        html_beam += '''<div class="visualbar_slider">
                        <div class="visual-bar">
                            <div class="slider-place">
                            <div role="application" class="ui-slider-track   ui-corner-all ui-mini" style="height:5px;color: #e61873;">
                                <div class="ui-slider-bg" style="width: {value}%; background-color: #e61873"></div>
                                <input type="hidden" class="beam-width-value" value="{value}">
                            </div>
                            </div>
                        </div>
                        </div>
                    '''.format(value=int(cv))
    
    if len(html_beam):
        embed_html = '''<div class="beam_place">{html_beam}</div>'''.format(html_beam=html_beam)
    else:
        embed_html = ""

    visual_temp = 0
    try:
        visual_temp = float(temp)
        # convert to %
        visual_temp = min(((visual_temp - 10) * 100) / 25, 100)
        if visual_temp < 0:
            visual_temp = 0
    except:
        pass

    soll = float(soll)
    ziel = float(ziel)
    offset_converted = (abs(float(offsets)) * 100) / 25

    overflow = True
    if soll <= 10:
        soll_converted = 0

    elif soll >= 35:
        soll_converted = 100
    else:
        soll_converted = (float(soll - 10) * 100) / 25
        overflow = False

    if ziel <= 10:
        ziel_converted = 0
    elif ziel >= 35:
        ziel_converted = 100
    else:
        ziel_converted = (float(ziel - 10) * 100) / 25
        overflow = False

    arrow_bar = ""
    delta = 100 - soll_converted
    if not overflow:
        if offsets <= -0.1 and ziel < 10 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-left" style="left:{ziel_converted}%;width:{soll_converted}%"></div><div class ="arrow-left" style="left:{ziel_converted}%;"></div>'
        if offsets <= -0.1 and ziel > 10 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-left" style="left:{ziel_converted}%;width:{offset_converted}%"></div><div class ="arrow-left" style="left:{ziel_converted}%;"></div>'
        if offsets >= 0.1 and ziel < 35 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-right" style="left:{soll_converted}%;width:{offset_converted}%"></div><div class ="arrow-right" style="left:{ziel_converted}%;"></div>'
        if offsets >= 0.1 and ziel > 35 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-right" style="left:{soll_converted}%;width:{delta}%"></div><div class ="arrow-right" style="left:{ziel_converted}%;"></div>'

    html = html.format(temp=temp, offset_str=offset_str, soll='%.2f' % soll, sign=sign,
                       ziel="%.2f" % ziel, ziel_color=ziel_color, pom=pom, max_offset_module=max_offset_module_html, id=raum.id,
                       icon=raum.icon, delta=delta,
                       name=raum.name, html_slider=html_slider, embed_beam=embed_html, mode_id=mode_id, hand_time=hand_time
                       , room_id=raum.id, hand_picture=hand_picture,
                       color=soll_offset_color, bis=bis)

    html = html.format(color=soll_offset_color,
                       arrow_bar=arrow_bar, soll_converted=soll_converted,
                       visual_temp=visual_temp, delta=delta,
                       ziel_converted=ziel_converted, hide=is_hide, ziel_color=ziel_color)
    html = html.format(delta=delta, ziel_converted=ziel_converted, soll_converted=soll_converted, offset_converted=offset_converted)

    html += "</a>"

    return html


def get_local_settings_page_haus(request, haus):

    if request.method == "GET":

        eundr = []
        for etage in haus.etagen.all():
            e = {etage: []}
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                regelung = raum.regelung
                regparams = regelung.get_parameters()
                e[etage].append((raum, regparams.get("rsfaktor", 2), regparams.get("schwelle", 0.5)))
            eundr.append(e)

        return render_response(request, "m_funksollregelung.html", {"haus": haus, "eundr": eundr})

    elif request.method == "POST":

        if request.POST.get('singleroom', False):
            name = request.POST['name']
            number = float(request.POST.get("number", 0.0))
            obj_id = long(request.POST.get('id', 0))
            
            raum = Raum.objects.get(id=obj_id)
            regelung = raum.regelung
            regparams = regelung.get_parameters()

            if name == 'rsfaktor':
                regparams["rsfaktor"] = number        
            if name == 'schwelle':
                regparams["schwelle"] = number    

            raum.regelung.set_parameters(regparams)
            return HttpResponse()

        else:
            rsfaktor = request.POST.get("rsfaktor", 2)
            if "rsfaktor_checkbox" not in request.POST:
                rsfaktor = None
            schwelle = request.POST.get("schwelle", 0.5)
            if "schwelle_checkbox" not in request.POST:
                schwelle = None

            raeume = request.POST.getlist('rooms')
            for raum_id in raeume:
                raum = Raum.objects.get(id=long(raum_id))
                regelung = raum.regelung
                regparams = regelung.get_parameters()
                if rsfaktor:
                    regparams["rsfaktor"] = rsfaktor        
                if schwelle:
                    regparams["schwelle"] = schwelle 
                raum.regelung.set_parameters(regparams)

            return render_redirect(request, "/m_funksollregelung/%s/" % haus.id)

