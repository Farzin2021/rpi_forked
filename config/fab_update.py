import requests
from fabric.operations import local as lrun
from fabric.api import lcd, settings, sudo
from fabric.state import env
import time
import json


env.hosts = ["localhost"]
path = "/home/pi/rpi"
server = "http://controme-main.appspot.com"


def update(update_override=False):
    with open('/sys/class/net/eth0/address') as f:
        macaddr = f.read().strip().replace(':', '-')

    try:
        from heizmanager.models import Haus, RPi
        rpi = RPi.objects.get(name=macaddr)
        haus = Haus.objects.get(pk=rpi.haus_id)
        hparams = haus.get_module_parameters()
        do_update = hparams.get('rpi', dict()).get(macaddr, dict()).get('automatic_updates', True)
    except:
        do_update = True

    try:
        r = None
        if update_override:
            r = requests.get("%s/get/update/%s/?mu" % (server, macaddr), timeout=20)
        elif not do_update and not update_override:
            r = requests.get("%s/get/update/%s/?du" % (server, macaddr), timeout=20)
        elif do_update:
            r = requests.get("%s/get/update/%s/" % (server, macaddr), timeout=20)
        else:
            return
    except:
        return

    if r is not None and len(r.content):
        try:
            resp = json.loads(rpi.crypt_keys.decrypt(r.content).rsplit('}', 1)[0] + '}')
        except:
            pass
        else:
            for k, v in resp.items():
                if k == "ts": continue
                elif k == 'permissions':
                    for perm, has_perm in v.items():

                        if perm == 'alexa_interface':
                            is_active = hparams.get('alexa_interface', dict()).get('is_active', False)
                            if is_active and not has_perm:
                                from alexa_interface.views import stop_service
                                stop_service(haus)
                            elif not is_active and has_perm:
                                from alexa_interface.views import start_service
                                start_service(haus)
                        
                        elif perm == "wetter_pro":
                            is_active = hparams.get('wetter_pro', dict()).get('is_active', False)
                            if is_active and not has_perm:
                                hparams['wetter_pro']['is_active'] = False
                                haus.set_module_parameters(hparams)
                            elif not is_active and has_perm:
                                hparams.setdefault('wetter_pro', dict())
                                hparams['wetter_pro']['is_active'] = True
                                haus.set_module_parameters(hparams)

                        elif perm == "rfonly":
                            current = hparams.get('rfonly', False)
                            if current != has_perm:
                                hparams['rfonly'] = has_perm
                                haus.set_module_parameters(hparams)

                elif k == 'visible_modules':
                    try:
                        from heizmanager.modules.visible_modules import visible_modules
                    except ImportError:
                        visible_modules = "None"
                    if v != visible_modules:
                        if v == "None" and visible_modules is not None:
                            lrun("""echo "visible_modules = %s" > /home/pi/rpi/heizmanager/modules/visible_modules.py""" % v)
                            lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi")
                        elif v != "None":
                            lrun("""echo "visible_modules = '%s'" > /home/pi/rpi/heizmanager/modules/visible_modules.py""" % v)
                            active_modules = lrun("sqlite3 /home/pi/rpi/db.sqlite3 'select modules from heizmanager_hausprofil'", capture=True)
                            active_modules = set(active_modules.split(','))
                            lrun("""sqlite3 /home/pi/rpi/db.sqlite3 'update heizmanager_hausprofil set modules = "%s"'""" % ','.join(list(active_modules & set(v.split(',')))))
                            lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi")

                else:
                    try:
                        from heizmanager.models import RPi
                        _rpi = RPi.objects.get(name=k)
                        if _rpi.local_ip != v:
                            from rpi.server import use_host
                            if use_host == _rpi.local_ip:
                                lrun("""echo "use_localhost = True; use_host = '%s';" > /home/pi/rpi/rpi/server.py""" % v)
                            _rpi.local_ip = v
                            _rpi.save()
                    except:
                        pass

    if r.status_code == 200 and (do_update or update_override):

        try:
            lrun("sudo service ntp stop")
            lrun("sudo ntpd -qg")
            lrun("sudo service ntp start")
        except:
            pass

        with lcd(path) and settings(warn_only=True):
            result = lrun("git show HEAD", capture=True)
            head = result.split("\n")[0].split(' ')[1]

            result = lrun("git pull", capture=True)
            if result.return_code != 0:
                if update_override:
                    lrun("touch /home/pi/rpi/failed_pull")
                if 'fatal: loose object' in result:
                    while not revert():
                        time.sleep(1)
            else:
                try:
                    lrun("rm /home/pi/rpi/failed_pull")
                except:
                    pass

            lrun("fab -f config/fab_upgrade.py upgrade:%s" % head)

    else:
        with lcd(path):
            result = lrun("git fsck", capture=True)
            if "fatal: loose object" in result:
                while not revert():
                    time.sleep(1)
            try:
                lrun("rm OZW_Log.txt")
            except:
                pass

            res = lrun("git diff --stat", capture=True)
            if len(res):
                files = res.split('\n')
                for f in files[:-1]:
                    _file = f.split('|')[0].strip()
                    lrun("git checkout %s" % _file)

    lrun("touch /home/pi/rpi/last_update")


def revert():
    print "trying to revert ..."
    with lcd("/home/pi/"):
        lrun("cp rpi/rpi/aeskey.py .")
        lrun("cp rpi/rpi/secret_key.py .")
        lrun("cp rpi/rpi/verification_code.py .")
        lrun("cp rpi/rpi/server.py .")
        lrun("cp rpi/db.sqlite3 .")
        lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf stop all")
        lrun("rm -rf rpi")
        lrun("git clone https://hcerny@bitbucket.org/hcerny/rpi.git rpi/")
        lrun("mv aeskey.py rpi/rpi/")
        lrun("mv secret_key.py rpi/rpi/")
        lrun("mv verification_code.py rpi/rpi/")
        lrun("mv db.sqlite3 rpi/")
        lrun("mv server.py rpi/rpi/")
        lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf start all")
    return True
        #result = lrun('git reset --hard @{1}')
