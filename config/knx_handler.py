import asyncio
from xknx import XKNX
from xknx.devices import Light, Climate, Switch
from xknx.telegram import PhysicalAddress
from xknx.exceptions.exception import XKNXException
from xknx.io import ConnectionConfig, ConnectionType
import zmq
from zmq.asyncio import Context
import requests
import time
import logging
import json
from datetime import datetime

#logging.basicConfig(level=logging.DEBUG)


class KNXHandler:

    def __init__(self, config):
        self.ctx = Context.instance()
        self.outsocket = self.ctx.socket(zmq.DEALER)
        self.outsocket.connect("tcp://127.0.0.1:5557")
        self.insocket = self.ctx.socket(zmq.SUB)
        self.insocket.connect("tcp://127.0.0.1:%s" % 5558)
        self.insocket.setsockopt(zmq.SUBSCRIBE, b'knx')

        if config['controller_address'].count('.') == 2:  # knx addr
            self.xknx = XKNX(own_address=PhysicalAddress(config['controller_address']), telegram_received_cb=self.telegram_received_cb, device_updated_cb=self.device_updated_cb)
            self.cc = None
        elif config['controller_address'].count('.') == 3:  # ip addr
            self.xknx = XKNX(telegram_received_cb=self.telegram_received_cb, device_updated_cb=self.device_updated_cb)
            self.cc = ConnectionConfig(connection_type=ConnectionType.TUNNELING, gateway_ip=config['controller_address'], gateway_port=3671)
        self.controller_address = config['controller_address']
        self.last_update = datetime.now()
        self.device_values = {}

    async def start(self):
        if self.cc is not None:
            await self.xknx.start(connection_config=self.cc)
        else:
            await self.xknx.start()

    async def periodic_update(self):
        while True:
            if (datetime.now() - self.last_update).total_seconds() > 300:  # todo kuerzer?
                print("requesting periodic update")
                await self.send_to_msgsrv({self.controller_address: {}})
            await asyncio.sleep(10)

    async def recv_from_msgsrv(self):
        while True:
            try:
                msg = await self.insocket.recv()
            except zmq.error.Again:
                pass
            except Exception as e:
                logging.exception("e")
                await asyncio.sleep(1)
            else:
                message = json.loads(msg.split(b'knx ')[1])
                if message.get('do_update', False):
                    await self.send_to_msgsrv({self.controller_address: {}})
                await self.update_devices(message['devices'])

    async def send_to_msgsrv(self, msg):
        self.outsocket.send_multipart([b'knx', b'', json.dumps(msg).encode('utf-8')])

    async def telegram_received_cb(self, telegram):
        logging.warning("{0} telegram received. group address: {1} -- payload: {2}".format(datetime.now(), telegram.group_address, telegram.payload))
        if str(telegram.group_address) in self.xknx.devices:
            dev = self.xknx.devices[str(telegram.group_address)]
            if isinstance(dev, Climate) and dev.temperature.value == dev.temperature.from_knx(telegram.payload):
                await self.send_to_msgsrv({self.controller_address: {'group_address': str(telegram.group_address), 'value': dev.temperature.from_knx(telegram.payload)}})

    async def device_updated_cb(self, device):
        # kommt nur bei wertaenderung
        print("{0} device updated: {1} -- {2}".format(datetime.now(), device.name, device.__dict__))
        if isinstance(device, Switch) and device.switch.from_knx(device.switch.payload) != self.device_values[str(device.name)]:  # damits beim schalten nicht hin und her geht
            await self.send_to_msgsrv({self.controller_address: {'group_address': str(device.name), 'value': device.switch.from_knx(device.switch.payload)}})
        elif isinstance(device, Climate):
            await self.send_to_msgsrv({self.controller_address: {'group_address': str(device.name), 'value': device.temperature.value}})

    async def update_devices(self, data):  # was passiert hier, wenn ein geraet nicht erreicht werden kann?
        self.last_update = datetime.now()
        print(f"{self.last_update} updating devices: {data}")
        for device in data:
            if device['group_address'] not in self.xknx.devices:
                if device['dpt'] == '1.001':
                    dev = Switch(self.xknx, name=device['group_address'], group_address=device['group_address'])
                    self.xknx.devices.add(dev)
                elif device['dpt'] == '5.001':
                    dev = Light(self.xknx, name=device['group_address'], group_address_brightness=device['group_address'])
                    self.xknx.devices.add(dev)
                elif device['dpt'] == '9.001':
                    dev = Climate(self.xknx, name=device['group_address'], group_address_temperature=device['group_address'])
                    self.xknx.devices.add(dev)
            else:
                dev = self.xknx.devices[device['group_address']]

            if isinstance(dev, Switch):
                self.device_values[device['group_address']] = device.get('value')
                if device['value']:
                    await dev.set_on()
                else:
                    await dev.set_off()
            elif isinstance(dev, Light):
                self.device_values[device['group_address']] = device.get('value')
                await dev.set_brightness(device.get('value'))
            elif isinstance(dev, Climate):
                self.device_values[device['group_address']] = device.get('value')
                await dev.set_target_temperature(device.get('value'))

async def main():

    config = None
    while config is None:
        try:
            ret = requests.get("http://localhost/get/knx/config/").json()
            if 'config' not in ret:
                raise Exception('no gateway declared')
            break
        except Exception as e:
            logging.exception("exc retrieving config")
            await asyncio.sleep(600)

    handler = KNXHandler(ret['config'])
    try:
        await handler.start()
    except XKNXException as e:
        if str(e) == "No Gateways found":
            pass  # todo an miniserver melden
        raise
    else:
        await handler.send_to_msgsrv({handler.controller_address: {}})
    await handler.update_devices(ret['devices'])
    pu = asyncio.create_task(handler.periodic_update())
    rfm = asyncio.create_task(handler.recv_from_msgsrv())
    await asyncio.wait([pu, rfm])
    await handler.xknx.stop()


if __name__ == "__main__":
    import sys
    if sys.version_info < (3, 7):
        time.sleep(3000)
        sys.exit(0)
    asyncio.run(main())#, debug=True)
