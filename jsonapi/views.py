from heizmanager.models import Haus, Etage, Luftfeuchtigkeitssensor, Raum, AbstractSensor, Regelung, GatewayAusgang, Gateway, Sensor
from heizmanager.models import sig_get_ctrl_devices, sig_get_all_outputs_from_output, sig_new_temp_value
from heizmanager.mobile.m_temp import get_module_offsets_regelung, get_module_upcoming_events


try:
    from rf.models import RFSensor, RFAktor
except ImportError:
    RFSensor = RFAktor = None
import json
from django.http import HttpResponse
import importlib
import heizmanager.cache_helper as ch
import logging
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from datetime import timedelta, datetime
import pytz
from django.contrib.auth.models import User
from itertools import chain
from quickui.views import QuickuiMode
from django.http import JsonResponse
from heizmanager.abstracts.base_mode import BaseMode


lmlogger = logging.getLogger("logmonitor")


try:
    import logger.models as logger
except ImportError:
    logger = None


def get_json(request, jsonver, hausid, data, entityid=None):

    if request.method == "GET":
        status = 200
        try:
            haus = Haus.objects.get(pk=long(hausid))
        except Haus.DoesNotExist:
            return HttpResponse(json.dumps([]), status=404, content_type='application/json')

        usr = request.META.get('HTTP_FROM')
        if usr:
            try:
                usr = User.objects.get(email=usr)
            except User.DoesNotExist:
                usr = haus.eigentuemer
        else:
            usr = haus.eigentuemer

        ret = []
        if data.lower() == 'rooms':

            # [
            #   {
            #       "id": etage.id
            #       "etagenname": etage.name,
            #       "raeume: [
            #           {
            #               "id": raum.id
            #               "name": raum.name,
            #               "icon": raum.icon
            #           },
            #           ...
            #       ]
            #   },
            #   ...
            # ]

            etagen = Etage.objects.filter(haus=haus)
            for etage in etagen:
                raeume = []
                for raum in Raum.objects.filter_for_user(usr, etage_id=etage.id).order_by("position").values_list("id", "name", "icon"):
                    raeume.append({
                        "id": raum[0],
                        "name": raum[1],
                        "icon": raum[2]
                    })

                ret.append({
                    "id": etage.id,
                    "etagenname": etage.name,
                    "raeume": raeume
                })

        elif data.lower() == 'hr':
            hparams = haus.get_module_parameters()

            drs = Regelung.objects.filter_for_user(usr, regelung="differenzregelung")
            for dr in drs:
                dreg_id = dr.get_parameters().get('dreg')
                dreg = hparams.get('differenzregelung', dict()).get(dreg_id)
                if dreg:
                    regelung_offsets = get_module_offsets_regelung(haus, dreg_id, 'differenzregelung', as_dict=True)
                    regelung_offset = sum(regelung_offsets.values())
                    try:
                        max_s2 = float(dreg['max_s2'])
                    except:
                        max_s2 = 75
                    ziel = max_s2 + regelung_offset

                    ret.append({
                        'name': 'Differenzregelung %s' % dreg['name'] if len(dreg['name']) else dreg_id,
                        'ziel': ziel,
                    })

                    state = ch.get("%s%sstatus" % ('differenzregelung', dr.id))
                    if state:
                        state = state.get('position')
                    else:
                        for ausgang in GatewayAusgang.objects.filter(regelung=dr):
                            if ausgang.gateway.name.startswith("b8"):
                                state = ch.get("cc_%s" % ausgang.gateway.name)
                                break
                        else:
                            state = None

                    ret[-1]['beam'] = False
                    if isinstance(state, dict):  # ausgang an hrgw
                        for dregid, values in state.items():
                            if dregid == dreg_id:
                                if values.get('position'):
                                    ret[-1]['beam'] = True
                                else:
                                    ret[-1]['beam'] = False
                    else:  # ausgang an anderem gw
                        ret[-1]['beam'] = state

                    for s in ['s1', 's2', 's3']:
                        sensor = AbstractSensor.get_sensor(dreg[s])
                        if sensor is not None:
                            val = sensor.get_wert()
                            ret[-1][s.upper()] = ("%.2f" % val[0]) if val and val[0] else "-"

            mrs = Regelung.objects.filter_for_user(usr, regelung="vorlauftemperaturregelung")
            for mr in mrs:
                mid = mr.get_parameters().get('mid')
                mreg = hparams.get('vorlauftemperaturregelung', dict()).get(mid)
                if mreg:

                    try:
                        hrgw = Gateway.objects.get(pk=mreg['hrgw'])
                    except Gateway.DoesNotExist:
                        continue
                    else:
                        states = ch.get("mischer_%s" % hrgw.name)
                        if states is not None:
                            state = states.get(mid)
                        else:
                            state = None

                        ret.append({
                            'name': 'Vorlauftemperaturregelung %s' % mreg['name'] if len(mreg['name']) else mid,
                            'state': state
                        })

            # pls = Regelung.objects.filter_for_user(usr, regelung="pumpenlogik")
            # for pl in pls:
            #     pass

        elif data.lower() == 'temps':

            # [
            #   {
            #       "id": etage.id
            #       "etagenname": etage.name,
            #       "raeume": [
            #           {
            #               "id": raum.id
            #               "name": raum.name,
            #               "solltemperatur": raum.solltemp,
            #               "total_offset": total_offset,
            #               "offsets": offsets,
            #               "temperatur": last,
            #               "sensoren": [
            #                   {
            #                       "name": sensor.name,
            #                       "beschreibung": sensor.description,
            #                       "wert": last_val,
            #                       "letzte_uebertragung": last_ts,
            #                       "raumtemperatursensor": true/false
            #                   },
            #                   ...
            #               ]
            #           },
            #           ...
            #       ]
            #   },
            #   ...
            # ]

            if entityid:
                try:
                    raum = Raum.objects.get_for_user(usr, pk=long(entityid))
                except (Raum.DoesNotExist, TypeError):
                    return HttpResponse(json.dumps(ret), content_type="application/json")

                ret.append(_get_room_temps(haus, raum))

            else:
                etagen = Etage.objects.filter(haus=haus)
                for etage in etagen:
                    raeume = []
                    for raum in Raum.objects.filter_for_user(usr, etage_id=etage.id):

                        raeume.append(_get_room_temps(haus, raum))

                    ret.append({
                        "id": etage.id,
                        "etagenname": etage.name,
                        "raeume": raeume
                    })

        elif data.lower() == 'roomoffsets':

            try:
                raum = Raum.objects.get(pk=long(entityid))
            except (Raum.DoesNotExist, TypeError):
                return HttpResponse(json.dumps(ret), content_type="application/json")

            offsets = _get_room_offsets(haus, raum)

            ret.append({
                "id": raum.id,
                "name": raum.name,
                "offsets": offsets
            })

        elif data.lower() == 'rltemps':

            # [
            #   {
            #       "id": sensor.id,
            #       "name": sensor.name,
            #       "beschreibung": sensor.description,
            #       "wert": last[0] if last else None,
            #       "letzte_uebertragung": last[1] if last else None,
            #       "ausgang": lastget[0] if lastget else None
            #   },
            #   ...
            # ]

            try:
                raum = Raum.objects.get(pk=long(entityid))
            except (Raum.DoesNotExist, TypeError):
                return HttpResponse(json.dumps(ret), content_type="application/json")

            reg = raum.regelung
            if reg.regelung != 'ruecklaufregelung':
                return HttpResponse(json.dumps(ret), content_type="application/json")

            params = reg.get_parameters()
            for ssn, outs in params.get('rlsensorssn', dict()).items():
                sensor = AbstractSensor.get_sensor(ssn)
                if sensor:
                    last = sensor.get_wert()
                    lastget = ch.get('lastout_%s_%s' % (reg.id, 0))

                    ret.append({
                        "id": sensor.id,
                        "name": sensor.name,
                        "beschreibung": sensor.description,
                        "wert": last[0] if last else None,
                        "letzte_uebertragung": last[1].strftime("%H:%M:%S %d.%m.%Y") if last else None,
                        "ausgang": lastget[0] if lastget else None
                    })

        elif data.lower() == 'outs':

            # [
            #   {
            #       "id": etage.id
            #       "etagenname": etage.name,
            #       "raeume": [
            #           {
            #               "id": raum.id
            #               "name": raum.name,
            #               "ausgang": {
            #                   "[[nummer]]": 1/0,
            #                   ...
            #               }
            #           },
            #           ...
            #       ]
            #   },
            #   ...
            # ]

            etagen = Etage.objects.filter(haus=haus)
            for etage in etagen:
                raeume = []
                for raum in Raum.objects.filter_for_user(usr, etage_id=etage.id):
                    gwausgang = raum.get_regelung().get_ausgang()
                    sget = {}
                    if gwausgang and len(gwausgang.ausgang):
                        ausgaenge = gwausgang.ausgang.split(',')
                        for ausgang in ausgaenge:
                            lastget = ch.get("lastout_%s_%s" % (raum.regelung_id, ausgang.strip()))
                            if lastget:
                                val = lastget[0]
                                sget[ausgang] = int(val) if len(val) else None
                            else:
                                val = gwausgang.get_regelung().do_ausgang(ausgaenge[0].strip()).content.translate(None, '<>!')
                                sget[ausgang] = int(val) if len(val) else None

                    raeume.append({
                        "id": raum.id,
                        "name": raum.name,
                        "ausgang": sget
                    })

                ret.append({
                    "id": etage.id,
                    "etagenname": etage.name,
                    "raeume": raeume
                })

        elif data.lower() == 'allouts':

            cb2func = sig_get_all_outputs_from_output.send(sender="jsonapi", haus=haus)
            ctrl2outs = {}
            for cb, func in cb2func:
                for out in func:
                    ctrl2outs.setdefault(out[0].name, list())
                    ctrl2outs[out[0].name] += out[4]
            for ctrl, outs in sorted(ctrl2outs.items()):
                ret.append({ctrl: sorted(outs)})

        elif data.lower() == 'allsensors':

            sensoren = Sensor.objects.all().order_by("name").select_related("gateway")
            rfsensoren = RFSensor.objects.all().order_by("name").select_related("controller")
            rfaktoren = RFAktor.objects.filter(type__in=['wt']).order_by("name").select_related("controller")

            _ret = {}
            for s in list(chain(sensoren, rfsensoren, rfaktoren)):
                try:
                    _ret.setdefault(s.gateway.name, list())
                    _ret[s.gateway.name].append((s.get_identifier(), s.name))
                except Exception as e:
                    logging.exception("wat")
                    _ret.setdefault(s.controller.name, list())
                    _ret[s.controller.name].append((s.get_identifier(), s.name))
            ret.append(_ret)

        elif data.lower() == 'marker':
            cb2ctrldevs = sig_get_ctrl_devices.send(sender='jsonapi', haus=haus)
            ctrldevs = [c for ctrldev in [cd[1] for cd in cb2ctrldevs] for c in ctrldev]
            for ctrldev in ctrldevs:
                try:
                    marker = ctrldev.get_marker()
                except TypeError:
                    marker = ctrldev.get_marker(ctrldev.name, ctrldev.get_shortest_wakeup())
                _dev = {'name': ctrldev.name, 'marker': marker, 'aktoren': {}, 'sensoren': {}}
                for sensor in ctrldev.get_sensoren():
                    smarker = sensor.get_marker()
                    _dev['sensoren'][sensor.name] = smarker
                try:
                    for aktor in ctrldev.get_aktoren():
                        amarker = aktor.get_marker()
                        _dev['aktoren'][aktor.name] = amarker
                except:
                    pass

                ret.append(_dev)

        elif data.lower() == 'alarm':
            if entityid is None:
                for etage in haus.etagen.all():
                    raeume = []
                    for raum in Raum.objects.filter_for_user(usr, etage_id=etage.id):
                        try:
                            if len(RFSensor.objects.filter(raum=raum, type="multisensor")):
                                alarm = raum.get_alarm_setting()
                                raeume.append({
                                    "id": raum.id,
                                    "name": raum.name,
                                    "alarm": alarm['notify']
                                })
                        except:
                            pass

                    ret.append({
                        "id": etage.id,
                        "etagenname": etage.name,
                        "raeume": raeume
                    })

            else:
                raum = Raum.objects.get_for_user(usr, pk=long(entityid))
                try:
                    if len(RFSensor.objects.filter(raum=raum, type="multisensor")):
                        alarm = raum.get_alarm_setting()
                        ret.append({
                            "id": raum.id,
                            "name": raum.name,
                            "alarm": alarm['notify']
                        })
                except:
                    pass

        elif data.lower() == 'roomlogs':
            if entityid is not None:
                try:
                    raum = Raum.objects.get(pk=int(entityid))
                except Raum.DoesNotExist:
                    return HttpResponse(status=404)

                berlin = pytz.timezone('Europe/Berlin')
                now = datetime.now(berlin)

                try:
                    von = request.GET.get('von')
                    if von is None:
                        von = (now-timedelta(days=1)).strftime("%Y-%m-%dT00:00")
                    ts = datetime.strptime(von, "%Y-%m-%dT%H:%M")
                    ts = berlin.localize(ts, is_dst=None).astimezone(pytz.UTC)
                    von = ts.strftime("%Y-%m-%dT%H:%M")

                    bis = request.GET.get('bis')
                    if bis is None:
                        bis = now.strftime("%Y-%m-%dT%H:%M")
                    ts = datetime.strptime(bis, "%Y-%m-%dT%H:%M")
                    ts = berlin.localize(ts, is_dst=None).astimezone(pytz.UTC)
                    bis = ts.strftime("%Y-%m-%dT%H:%M")
                except Exception as e:
                    logging.exception("roomlogs von/bis fehler")
                    return HttpResponse(status=400)

                for sensor in raum.get_sensoren():
                    try:
                        data = logger.SimpleSensorLog.objects.getlogs(sensor.get_identifier(), von, bis)
                    except:
                        data = []
                    else:
                        ret.append([unicode(sensor), data])

                regelung = raum.get_regelung()
                ausgang = regelung.get_ausgang()
                if ausgang is not None:
                    _ausgang = ausgang.ausgang.split(',')
                    _ausgang = _ausgang[0].strip()
                    if len(_ausgang):
                        name = "%s_%s" % (ausgang.gateway.name, _ausgang)
                        data = logger.SimpleOutputLog.objects.getlogs(name, von, bis)
                        ret.append(["Zielwert", data])

            else:
                return HttpResponse(status=404)

        elif data.lower() == 'regelunglogs':
            pass

        elif data.lower() == 'upcomingoffsets':

            if entityid is None:
                ret = {}
                ret['houses'] = {}
                ret['rooms'] = {}
                ret['regelungs'] = {'differenzregelung': {}, 'vorlauftemperaturregelung': {}}

                # getting houses
                for haus in Haus.objects.all():
                    coming_offsets = get_module_upcoming_events(haus, haus, haus.id)
                    if coming_offsets:
                        ret['houses'][haus.id] = coming_offsets
                #

                # getting rooms
                for etage in haus.etagen.all():
                    for raum in Raum.objects.filter(etage_id=etage.id):
                        coming_offsets = get_module_upcoming_events(haus, raum, raum.id)
                        if coming_offsets:
                            ret['rooms'][raum.id] = coming_offsets

                # getting differenzregelung
                regs = Regelung.objects.filter(regelung='differenzregelung')
                for reg in regs:
                    dreg_id = reg.get_parameters()['dreg']
                    coming_offsets = get_module_upcoming_events(haus, reg, dreg_id)
                    if coming_offsets:
                        ret['regelungs']['differenzregelung'][dreg_id] = coming_offsets

                # getting vorlauftemperaturregelung
                regs = Regelung.objects.filter(regelung='vorlauftemperaturregelung')
                for reg in regs:
                    m_id = reg.get_parameters()['mid']
                    coming_offsets = get_module_upcoming_events(haus, reg, m_id)
                    if coming_offsets:
                        ret['regelungs']['vorlauftemperaturregelung'][m_id] = coming_offsets

                return JsonResponse(ret, safe=False, status=200)
            else:
                return HttpResponse(status=404)

        # get json from respective module
        else:
            try:
                module = importlib.import_module(data.lower() + '.views')
                if data.lower() not in haus.get_modules():
                    raise Exception("module is not activated")
                ret = module.get_jsonapi(haus, usr, entityid=entityid)
            except ImportError:
                ret = {}
                status = 200
            except Exception as e:
                logging.warning(str(e))
                ret = {}
                status = 200

        return JsonResponse(ret, safe=False, status=status)


@csrf_exempt
def set_json(request, jsonver, hausid, data, entityid=None):

    # wie ist die cache_ttl? oder kann der benutzer die setzen? braucht der benutzer eine moeglichkeit,
    # programmatisch zu pruefen, ob ein bestimmter offset noch gesetzt ist?

    # naechste schritte:
    # - set fuer externen sensor
    # - get fuer externen ausgang
    # - "status", falls mehrere (unabhaengige) ausgaenge in einem raum
    # - "batterie", fuer rfsensoren

    try:
        haus = Haus.objects.get(pk=long(hausid))
    except (Haus.DoesNotExist, TypeError):
        return HttpResponse(status=404)

    if request.method == "POST":

        usr = request.POST.get('user', '')
        pw = request.POST.get('password', '')
        user = authenticate(username=usr, password=pw)
        if request.META['REMOTE_ADDR'] in ['127.0.0.1', 'localhost'] or data.lower() == "set":
            user = haus.eigentuemer
            request.user = user
        if user is None:
            return HttpResponse(status=403)

        if data.lower() == "soll":
            try:
                raum = Raum.objects.get(pk=long(entityid))
            except (Raum.DoesNotExist, TypeError):
                return HttpResponse(status=404)

            soll = request.POST.get('soll', None)
            try:
                lmlogger.warning(u"%s|%s" % (raum.id, u"Solltemperatur via API auf %.2f gesetzt (A1)" % float(soll)))
            except:
                pass

            if soll is not None and soll != "":
                soll = float(soll)
                ret = BaseMode.get_active_mode_raum(raum)
                if ret:
                    ret['activated_mode'].set_mode_temperature_raum(raum, soll)
                else:
                    raum.solltemp = soll
                    raum.save()
                return HttpResponse()
            else:
                return HttpResponse(status=400)

        elif data.lower() == 'roomoffset':

            try:
                raum = Raum.objects.get(pk=long(entityid))
            except (Raum.DoesNotExist, TypeError):
                return HttpResponse(status=404)

            offset = request.POST.get('offset', '')
            offset_name = request.POST.get('offset_name', '')
            if not len(offset_name) or not len(offset):
                return HttpResponse(status=400)

            try:
                offset = float(offset)
            except ValueError:
                return HttpResponse(status=400)

            offsets = ch.get("api_roomoffset_%s" % raum.id)
            if offsets is None:
                offsets = {}
            offsets[offset_name] = offset
            ch.delete("%s_roffsets_dict" % raum.id)
            ch.set("api_roomoffset_%s" % raum.id, offsets, time=610)

            return HttpResponse()

        elif data.lower() == 'houseoffset':

            offset = request.POST.get('offset', '')
            offset_name = request.POST.get('offset_name', '').decode('utf-8')
            if not len(offset_name) or not len(offset):
                return HttpResponse(status=400)

            try:
                offset = float(offset)
            except ValueError:
                return HttpResponse(status=400)

            offsets = ch.get("api_houseoffset_%s" % haus.id)
            if offsets is None:
                offsets = {}
            offsets[offset_name] = offset
            for etage in haus.etagen.all():
                for rid in Raum.objects.filter_for_user(haus.eigentuemer, etage_id=etage.id).values_list("id"):
                    ch.delete("%s_roffsets_dict" % rid[0])
            ch.set("api_houseoffset_%s" % haus.id, offsets, time=610)

            return HttpResponse()

        elif data.lower() == 'alarm':

            if entityid is None:
                value = True if request.POST.get('alarm', 'aus') == 'an' else False  # sanitize
                for etage in haus.etagen.all():
                    for raum in Raum.objects.filter_for_user(haus.eigentuemer, etage_id=etage.id):
                        raum.set_alarm_setting({'notify': value})

            else:
                try:
                    raum = Raum.objects.get(pk=long(entityid))
                except (Raum.DoesNotExist, TypeError):
                    return HttpResponse(status=404)

                value = True if request.POST.get('alarm', 'aus') == 'an' else False  # sanitize
                raum.set_alarm_setting({'notify': value})

            return HttpResponse()

        elif data.lower() == 'ziel':
            hparams = haus.get_module_parameters()
            try:
                raum = Raum.objects.get(pk=long(entityid))
            except (Raum.DoesNotExist, TypeError):
                return HttpResponse(status=404)

            if 'duration' in request.POST and 'ziel' in request.POST:

                duration = request.POST['duration'] if request.POST['duration'] != 'default' else hparams.get('quickui', {}).get('default_duration', 180)
                QuickuiMode(float(request.POST['ziel']), duration).set_mode_raum(raum)
                return HttpResponse()
            else:
                return HttpResponse(status=404)

        elif data.lower() == "set":
            sensorid = request.POST.get('sensorid')
            try:
                sensor = list(chain(Sensor.objects.filter(name=sensorid.lower()), Luftfeuchtigkeitssensor.objects.filter(name=sensorid.lower())))[0]                
            except:
                return HttpResponse(404)
            value = request.POST.get('value')
            try:
                value = float(value)
            except:
                return HttpResponse(400)
            ch.set_new_temp(sensorid.lower(), value, hausid)
            sig_new_temp_value.send(sender="jsonapi_set", name=sensor.get_identifier(), value=value, modules=haus.get_modules(), timestamp=datetime.now())
            return HttpResponse()

        else:
            try:
                status = 200
                module = importlib.import_module(data.lower() + '.views')
                if data.lower() != 'quickui' and data.lower() not in haus.get_modules():
                    raise Exception("module is not activated")
                ret = module.set_jsonapi(request.POST, haus, haus.eigentuemer, entityid)

            except ImportError:
                ret = {'message': 'please check the url'}
                status = 400
            except Exception as e:
                logging.warning(str(e))
                ret = {'message': str(e)}
                status = 400
            finally:
                return JsonResponse(ret, safe=False, status=status)


def _get_room_temps(haus, raum):
    ms = raum.get_mainsensor()

    offsets = _get_room_offsets(haus, raum)

    total_offset = sum([sum([_v for _v in v.values() if (isinstance(_v, float) or isinstance(_v, int))]) for v in offsets.values()])

    sensoren = raum.get_sensoren()
    s = []
    temp = None
    for sensor in sensoren:
        last = sensor.get_wert()
        rts = True if sensor.mainsensor and ms == sensor else False
        if rts:
            temp = last
            if isinstance(temp, tuple):
                temp = temp[0]

        if sensor.get_type() == "RFSensor":
            vals = {'Luminance': 'Helligkeit', 'Temperature': 'Temperatur', 'Relative Humidity': 'Relative Luftfeuchte', 'Alarm': 'Bewegung'}
            lst = {}
            for k, v in vals.items():
                sgwk = sensor.get_wert(k)
                if sgwk is not None:
                    lst[v] = sgwk[0]
                else:
                    lst[v] = None
            _sgw = sensor.get_wert()
            if _sgw:
                last_trans = _sgw[1]
            else:
                last_trans = None
            last = (lst, last_trans)

        s.append({
            "name": sensor.name,
            "beschreibung": sensor.description,
            "wert": last[0] if (isinstance(last, list) or isinstance(last, tuple)) else last if isinstance(last, float) else None,
            "letzte_uebertragung": last[1].strftime("%d.%m.%Y %H:%M") if last and isinstance(last, tuple) and last[1] and not isinstance(last[1], list) and not isinstance(last[1], tuple) else None,
            "raumtemperatursensor": rts
        })

    lf = raum.get_humidity()
    if lf is not None:
        if lf[0] == -1:
            lf = "kein aktueller Wert vorhanden"
        else:
            lf = lf[0]
    else:
        lf = "kein Sensor vorhanden"

    return {
        "id": raum.id,
        "name": raum.name,
        "solltemperatur": raum.solltemp,
        "total_offset": total_offset,
        "offsets": offsets,
        "temperatur": temp,
        "sensoren": s,
        "luftfeuchte": lf
    }


def _get_room_offsets(haus, raum):

    offsets = dict()
    for mod in set(haus.get_modules()) & set(raum.get_modules()):
        try:
            module = importlib.import_module(mod.strip() + '.views')
        except ImportError:
            continue

        raum_offset = haus_offset = None

        try:
            ignore_cache = module.calculate_always_anew()
        except AttributeError:
            ignore_cache = False

        if not ignore_cache:
            haus_offset = ch.get_module_offset_haus(mod, haus.id)

        if haus_offset is None:
            try:
                haus_offset = module.get_offset(haus)
            except AttributeError:
                haus_offset = 0.0

        if not ignore_cache:
            raum_offset = ch.get_module_offset_raum(mod, haus.id, raum.id)

        if raum_offset is None:
            try:
                raum_offset = module.get_offset(haus, raum=raum)
            except AttributeError:
                raum_offset = 0.0

#### module.get_name() muss ueberall einen unicode String zurueckgeben, den man einfach so ausdrucken kann! wird get_name sonst noch irgendwo verwendet?

        try:
            offsets[module.get_name()] = {}
            offsets[module.get_name()]["haus"] = haus_offset if (isinstance(haus_offset, int) or isinstance(haus_offset, float)) else 0.0
            offsets[module.get_name()]["raum"] = raum_offset if (isinstance(raum_offset, int) or isinstance(raum_offset, float)) else 0.0
        except Exception:
            logging.exception("exc")

    try:
        offsets["api"] = {}
        api_raum = ch.get('api_roomoffset_%s' % raum.id)
        if api_raum:
            offsets['api']["raum"] = sum(api_raum.values())
        api_haus = ch.get('api_houseoffset_%s' % haus.id)
        if api_haus:
            offsets['api']["haus"] = sum(api_haus.values())
    except:
        pass

    return offsets
