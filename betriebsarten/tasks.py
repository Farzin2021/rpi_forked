# -*- coding: utf-8 -*-
from celery import shared_task
from heizmanager.models import Haus, Raum, Regelung
import pytz
from datetime import datetime, timedelta


def do_process(haus):

    # result value
    kuhlen = 1
    aus = 0
    heizen = 2
    # result-value priority
    priority = {
        aus: 2,
        kuhlen: 3,
        heizen: 1
    }

    berlin = pytz.timezone("Europe/Berlin")
    now = datetime.now(berlin)

    b_params = haus.get_spec_module_params('betriebsarten')
    b_items = b_params.get('items', dict())
    room_action_d = dict()
    for entity_id, params in b_items.items():
        try:
            reg = Regelung.objects.get(id=int(params.get('fps_abfrage', 0)))
        except Regelung.DoesNotExist:
            continue

        # result
        result = reg.do_ausgang(0).content
        result_text = "kein Ergebnis"
        if result == '<1>':
            result = params['cooling']
            result_text = 'Aktiv'
        elif result == '<0>':
            result = str(heizen)
            result_text = 'nicht Aktiv'
        else:
            result = None

        # saving result
        b_params['last_check'] = now.strftime('%Y-%m-%d %H:%M')
        b_params['next_check'] = (now + timedelta(hours=2)).strftime('%Y-%m-%d %H:%M')
        b_items[entity_id]['result'] = result_text
        b_params['items'] = b_items
        haus.set_spec_module_params('betriebsarten', b_params)
        if result is None:
            continue

        # collecting a dict of room and their action that should be done, action would be cooling heating aus
        for room_id in params['rooms']:
            if room_id not in room_action_d:
                room_action_d[room_id] = result
            else:
                if priority[int(result)] > priority[int(room_action_d[room_id])]:
                    room_action_d[room_id] = result

    # set result of cooling to the rooms
    for r_id, action_id in room_action_d.items():
        raum = Raum.objects.get(id=int(r_id))
        b_r_params = raum.get_spec_module_params('betriebsarten')
        b_r_params[r_id] = action_id
        raum.set_spec_module_params('betriebsarten', b_r_params)

    return True


@shared_task()
def periodic_betriebsarten_evaluation():
    for haus in Haus.objects.all():
        # module activation check
        if 'betriebsarten' in haus.get_modules():
            do_process(haus)
