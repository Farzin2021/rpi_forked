# -*- coding: utf-8 -*-

import logging
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Raum, Haus, Regelung, AbstractSensor
import heizmanager.cache_helper as ch
import pytz
from datetime import datetime
from operator import itemgetter
from django.http import HttpResponse
from django.http import JsonResponse
from heizmanager.mobile.m_setup import set_regelung_entity, get_regelung_entity


def get_name():
    return u'Wetter'


def is_togglable():
    return True


def get_cache_ttl():
    return 3600


def calculate_always_anew():
    return False


def is_hidden_offset():
    return False


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/wetter/'>Wettereinstellungen</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Sorgt dafür, dass vor einem sonnigen Tag weniger geheizt wird."
    desc_link = "http://support.controme.com/modul-wetter/"
    return desc, desc_link


def get_global_settings_page(request, haus):

    if request.method == "GET":
        active = False
        if 'wetter' in haus.get_modules():
            active = True

        params = haus.get_module_parameters()
        context = dict()
        context['r_m_active'] = params.get('wetter_right_menu', True)
        context['active'] = active
        context['haus'] = haus
        return render_response(request, "m_settings_wetter.html", context)

    elif request.method == "POST":

        if 'right_menu_ajax' in request.POST:
            params = haus.get_module_parameters()
            params['wetter_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(params)
            return JsonResponse({'message': 'done'})

        if 'wetterparameter' in request.POST and request.POST['wetterparameter'] == '1':
            activateall = False
            if 'activateall' in request.POST and request.POST['activateall'] == 'on':
                activateall = True
            warm = 0
            if len(request.POST['hwarm']):
                warm = int(request.POST['hwarm'], 10)
            kalt = 0
            if len(request.POST['hkalt']):
                kalt = int(request.POST['hkalt'], 10)
            set_params(haus, warm, kalt)
            for etage in haus.etagen.all():
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    set_params(raum, warm, kalt)
                    if activateall:
                        activate(raum)
                    ch.set_module_offset_raum('wetter', haus.id, raum.id,
                                              get_offset(haus, raum=raum), ttl=get_cache_ttl())

        return render_redirect(request, '/config')


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_wetter.html", {'haus': haus})


def get_local_settings_page_haus(request, haus):

    if request.method == "GET":

        eundr = []
        for etage in haus.etagen.all():
            e = {etage: []}
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                e[etage].append(raum)
            eundr.append(e)

        params = haus.get_module_parameters()

        reg_hparams = params.get('differenzregelung', {})
        regs = Regelung.objects.filter_for_user(request.user).filter(regelung='differenzregelung')
        diff_params = []
        for d_reg in regs:
            dreg_id = d_reg.get_parameters().values()[0]
            wetter_params = d_reg.get_wetter_params(haus)
            diff_params.append((d_reg.id, dreg_id, reg_hparams[dreg_id], wetter_params))

        reg_hparams = params.get('vorlauftemperaturregelung', {})
        regs = Regelung.objects.filter_for_user(request.user).filter(regelung='vorlauftemperaturregelung')
        vor_params = []
        for d_reg in regs:
            mid = d_reg.get_parameters().values()[0]
            wetter_params = d_reg.get_wetter_params(haus)
            vor_params.append((d_reg.id, mid, reg_hparams[mid], wetter_params))

        diff_params.sort(key=lambda tup: tup[1])
        vor_params.sort(key=lambda tup: tup[1])

        return render_response(request, "m_wetter.html", {"eundr": eundr, "haus": haus,'diff_params': diff_params,
                                                                             'vor_params': vor_params})

    elif request.method == "POST":

        if request.POST.get('singleroom', False):
            name = request.POST['name']
            obj_id = long(request.POST.get('id', 0))
            if name.startswith("reg"):
                object = "regelung"
                name = name.replace("reg_", '')
            else:
                object = "raum"

            if name == "anhebung":
                anhebung = request.POST['number']
                absenkung = -1
            if name == "absenkung":
                absenkung = request.POST['number']
                anhebung = -1

            if object == "regelung":
                new_params = {'absenkung': absenkung, 'anhebung': anhebung}
                reg_obj = Regelung.objects.get(id=obj_id)
                params = reg_obj.get_wetter_params(haus)

                set_params_regelung(haus, reg_obj.get_parameters().values()[0], reg_obj.regelung, params,
                                    new_params)
            else:
                raum = Raum.objects.get(id=long(request.POST.get('id', 0)))
                set_params(raum, absenkung, anhebung)
            return HttpResponse()

        else:
            logging.warning(request.POST)
            absenkung = request.POST.get('absenkung', -1)
            if 'absenkung_checkbox' not in request.POST:
                absenkung = -1
            anhebung = request.POST.get('anhebung', -1)
            if 'anhebung_checkbox' not in request.POST:
                anhebung = -1
            raeume = request.POST.getlist('rooms')
            for raum_id in raeume:
                raum = Raum.objects.get(id=long(raum_id))
                set_params(raum, absenkung, anhebung)

            new_params = {'absenkung':int(absenkung), 'anhebung': int(anhebung)}
            regs = request.POST.getlist('regs')
            for reg_id in regs:
                reg_obj = Regelung.objects.get(id=reg_id)
                params = reg_obj.get_wetter_params(haus)
                set_params_regelung(haus, reg_obj.get_parameters().values()[0], reg_obj.regelung, params, new_params)
            return render_redirect(request, "/m_wetter/%s/" % haus.id)


def get_local_settings_link(request, raum):
    if 'wetter' not in raum.etage.haus.get_modules():
        return ""

    if 'wetter' in raum.get_modules():
        offset = get_offset(raum.etage.haus, raum=raum)
        state = "%.2f" % offset
    else:
        state = "deaktiviert"

    return "Wetterkorrektur", state, 75, "/m_raum/%s/wetter/" % raum.id


def get_local_settings_link_regelung(haus, modulename, objid):
    offset = get_offset_regelung(haus, modulename, objid)
    return get_name(), 'wetter', offset, 75, '/m_%s/%s/show/%s/wetter/' % (modulename, haus.id, objid)


def get_local_settings_page_regelung(request, haus, modulename, objid, params):
    r_params = get_regelung_entity(haus, objid, modulename)
    if request.method == "GET":
        active = False
        if params:
            active = True
        page_params = {}
        gactive = False
        if 'wetter' in haus.get_modules():
            gactive = True
        page_params['active'] = active
        page_params['gactive'] = gactive
        page_params['wetter_params'] = r_params.get("module_params", {}).get("wetter", {})
        page_params["regelung_obj"] = r_params
        page_params["regelung"] = modulename
        page_params["regelung_id"] = objid
        page_params["haus"] = haus
        return render_response(request, "m_regelung_wetter.html", page_params)

    if request.method == "POST":
        if 'wetter' in request.POST and request.POST['wetter'] == '1':
            warm = 0
            if len(request.POST['warm']):
                warm = int(request.POST['warm'], 0)
            kalt = 0
            if len(request.POST['kalt']):
                kalt = int(request.POST['kalt'], 0)
            wetter_params = {"warm": warm, "kalt": kalt}
            set_regelung_entity(haus, modulename, objid, wetter_params, 'wetter')
        else:
            set_regelung_entity(haus, modulename, objid, {}, 'wetter')
        return render_redirect(request, '/m_%s/%s/show/%s/' % (modulename, haus.id, objid))


def get_offset_regelung(haus, module, objid):
    reg_obj = get_regelung_entity(haus, objid, module)
    if 'module_params' not in reg_obj or 'wetter' not in reg_obj['module_params']:
        return 0.0
    if not reg_obj['module_params']['wetter']:
        return 0.0

    offset = calculate(haus, reg_obj['module_params']['wetter']['warm'], reg_obj['module_params']['wetter']['kalt'])
    ch.set_module_offset_regelung('wetter', haus.id, module, objid, offset, ttl=get_cache_ttl())
    return offset


def get_local_settings_page(request, raum):
    if request.method == "GET":
        active = False
        if 'wetter' in raum.get_modules():
            active = True
        gactive = False
        if 'wetter' in raum.etage.haus.get_modules():
            gactive = True
        params = get_params(raum)
        return render_response(request, "m_raum_wetter.html",
                               {"haus": raum.etage.haus, "raum": raum, "active": active, "gactive": gactive,
                                "w": params[0], "k": params[1]})
    if request.method == "POST":

        if 'disable_wetter' in request.POST:
            modules = raum.get_modules()
            modules.remove('wetter')
            raum.set_modules(modules)

        if 'enable_wetter' in request.POST:
            modules = raum.get_modules()
            modules.insert(2, u'wetter')
            raum.set_modules(modules)

        if request.POST.get('singleroom', False):
            name = request.POST['name']
            obj_id = long(request.POST.get('id', 0))
            number = request.POST['number']
            raum = Raum.objects.get(id=obj_id)
            warm = -1
            kalt = -1
            if name == 'wetter':
                if number == '1':
                    activate(raum)
                else:
                    deactivate(raum)
            
            if name == "warm":
                warm = int(number)
            
            if name == "kalt":
                kalt = int(number)

            set_params(raum, warm, kalt)

            ch.set_module_offset_raum('wetter', raum.etage.haus.id, raum.id,
                                    get_offset(raum.etage.haus, raum=raum), ttl=get_cache_ttl())
        return HttpResponse()


def calculate(haus, warm, kalt):
    try:
        forecast = get_forecast(haus, 8)
        if forecast is None:
            logging.warning("couldn't retrieve weather data")
            return 0.0
        cnt = float(forecast[0][1])
    except AttributeError:  # vermutlich, weils haus kein lat/lng hat
        return 0.0
    except Exception as e:
        import traceback
        logging.error('Exception!: %s' % traceback.format_exc())
        return 0.0

    steigung = [0.0, 0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16, 0.18, 0.2]
    hours = {-13: 2, -5: 3, 3: 4, 11: 5, 20: 6, 99: 7}

    zeit = None
    for h in reversed(sorted(hours.keys())):
        if cnt < h:
            zeit = hours[h]

    if zeit is None or zeit >= len(forecast):
        return 0.0

    if float(forecast[zeit][1]) < cnt:  # es wird kaelter
        o = float(abs(int(cnt) - int(forecast[zeit][1]))) * steigung[kalt]
        offset = o if o <= 1.0 else 1.0
    elif float(forecast[zeit][1]) > cnt:  # es wird waermer
        o = float(abs(int(cnt) - int(forecast[zeit][1]))) * (steigung[warm] * -1.0)
        offset = o if o >= -1.0 else -1.0
    else:
        offset = 0.0

    return offset


def get_offset(haus, raum=None):
    # hier gibts keinen Unterschied zwischen Raum und Haus. Wenn raum=None, dann 0.0.
    params = {}
    if raum and 'wetter' in raum.get_modules() and 'wetter' in haus.get_modules():
        params = raum.get_module_parameters()
        params = params['wetter']['wetter_params']
    else:
        ch.set_module_offset_haus('wetter', haus.id, 0.0, ttl=get_cache_ttl())
        return 0.0

    warm = int(params.split(',')[0])
    kalt = int(params.split(',')[1])
    offset = calculate(haus, warm , kalt)
    ch.set_module_offset_raum('wetter', haus.id, raum.id, offset, ttl=get_cache_ttl())
    return offset


def get_forecast(haus, forecast_len):
    # forecast_len gibt nur eine untere grenze fuer die laenge des zurueckgegebenen forecasts an.

    forecast = ch.get('weather_%s' % haus.id)
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    if forecast == -1:  # try again later
        forecast = None

    elif forecast is None or isinstance(forecast, float):
        params = haus.get_module_parameters()
        forecast = _get_forecast(params.get('wetter', dict()).get('lat'), params.get('wetter', dict()).get('lng'), 72)
        if forecast is None:
            return None
        elif forecast == -1:
            ch.set('weather_%s' % haus.id, forecast, time=600)
        else:
            forecast = [tuple(f) for f in forecast]

        ch.set('weather_%s' % haus.id, forecast, time=14400)

    elif 0 < forecast_len <= len(forecast) and forecast[0][0] == now.hour:
        pass

    elif 0 < forecast_len < len(forecast) and forecast[1][0] == now.hour:
        forecast = forecast[1:]
        if len(forecast) > 48:
            ch.set('weather_%s' % haus.id, forecast, time=14400)

    else:
        params = haus.get_module_parameters()
        forecast = _get_forecast(params.get('wetter', dict()).get('lat'), params.get('wetter', dict()).get('lng'), 72)
        if forecast is None:
            return None
        elif forecast == -1:
            ch.set('weather_%s' % haus.id, forecast, time=600)
        else:
            forecast = [tuple(f) for f in forecast]

        ch.set('weather_%s' % haus.id, forecast, time=14400)

    return _adapt_forecast_to_actual_temp(haus, forecast)


def _adapt_forecast_to_actual_temp(haus, forecast):
    if forecast is None or forecast == -1:
        return forecast

    s = AbstractSensor.get_sensor(haus.get_module_parameters().get('ruecklaufregelung', dict()).get('atsensor', None))
    try:
        at = s.get_wert()[0]
    except (AttributeError, TypeError):
        return forecast

    diff = at - float(forecast[0][1])

    return [(f[0], float(f[1])+diff) for f in forecast]


def get_10dayforecast(haus, forecast_len):

    return get_forecast(haus, forecast_len)


def _get_forecast(lat, lng, hours):
    if not lat or not lng or (not float(lat) and not float(lng)):
        logging.warning("oh oh %s %s" % (lat, lng))
        return None

    # import socket
    # try:
    #     s = socket.create_connection(("8.8.8.8", 53), 1)
    # except Exception as e:
    #     logging.exception("socket error")
    #     return -1

    import requests
    import ast

    data = {'lat': lat, 'lng': lng, 'hours': hours}
    try:
        r = requests.post("https://controme-main.appspot.com/weather/get/", data=data, timeout=12)
        forecast = ast.literal_eval(r.content)
    except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
        forecast = -1
    except SyntaxError:
        forecast = -1
    except:
        logging.exception("_get_forecast fehler")
        forecast = None
    return forecast


def get_params(hausoderraum):
    params = hausoderraum.get_module_parameters()
    try:
        return params['wetter']['wetter_params'].split(',')[0], params['wetter']['wetter_params'].split(',')[1]
    except KeyError:
        # params nicht gesetzt
        return 5, 5


def set_params(hausoderraum, warm, kalt):
    w, k = get_params(hausoderraum)
    if int(warm) == -1:
        warm = w
    if int(kalt) == -1:
        kalt = k

    params = hausoderraum.get_module_parameters()
    params.setdefault('wetter', dict())['wetter_params'] = ','.join([str(warm), str(kalt)])
    hausoderraum.set_module_parameters(params)


def set_params_regelung(haus, objid, modulename, params, new_params):
    w, k = params.get('warm', 0), params.get('kalt', 0)
    warm = new_params['absenkung']
    kalt = new_params['anhebung']
    if int(warm) == -1:
        warm = w
    if int(kalt) == -1:
        kalt = k
    params['warm'] = int(warm)
    params['kalt'] = int(kalt)
    set_regelung_entity(haus, modulename, objid, params, 'wetter')


def deactivate(hausoderraum):
    mods = hausoderraum.get_modules()
    try:
        mods.remove('wetter')
        hausoderraum.set_modules(mods)
    except ValueError:  # nicht in modules
        pass


def activate(hausoderraum):
    if not 'wetter' in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['wetter'])
