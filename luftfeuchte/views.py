# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
import heizmanager.cache_helper as ch
from heizmanager.models import Haus
from heizmanager.models import Raum
from django.http import HttpResponse
from django.http import JsonResponse
# from django.utils.translation import ugettext_lazy as _
# from django.utils.encoding import force_text


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def is_hidden_offset():
    return False


def calculate_always_anew():
    return False


def get_offset(haus, raum=None):

    if raum is None:
        return 0.0

    params = raum.get_module_parameters()

    params = raum.get_humidity_params()
    initial = params['initial']
    intensity = params['intensity']

    if intensity and initial:
        humidity = raum.get_humidity()
        offset = _calculate_offset(humidity, initial, intensity)
    else:
        offset = 0.0

    ch.set_module_offset_raum("luftfeuchte", haus.id, raum.id, offset, ttl=600)

    return offset


def get_name():
    return 'luftfeuchte'


def _calculate_offset(humidity, initial, intensity):
    if humidity is None or humidity[0] == -1:
        return 0.00
    offset = (float(humidity[0]) - float(initial)) * float(intensity) * -1
    return offset


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/luftfeuchte/'>Luftfeuchte</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Kompensiert die Abhängigkeit der Wohlfühltemperatur von der Luftfeuchte."
    desc_link = "http://support.controme.com/luftfeuchte/"
    return desc, desc_link


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    params = haus.get_module_parameters()

    if request.method == "GET":
        context['haus'] = haus
        context['r_m_active'] = params.get('luftfeuchte_right_menu', True)
        return render_response(request, "m_settings_luftfeuchte.html", context)

    if request.method == "POST":
        if 'right_menu_ajax' in request.POST:
            params = haus.get_module_parameters()
            params['luftfeuchte_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(params)
            return JsonResponse({'message': 'done'})

        return render_redirect(request, '/m_setup/%s/luftfeuchte/' % haus.id)


def get_local_settings_link(request, raum):

    humidity = raum.get_humidity()

    if humidity is None:
        pass

    elif humidity is -1:
        return "Luftfeuchte", "Luftfeuchte", "Kein Wert", 75, "#"

    else:
        offset = ch.get_module_offset_raum("luftfeuchte", raum.etage.haus_id, raum.id)
        if offset is None:
            params = raum.get_humidity_params()
            initial = params['initial']
            intensity = params['intensity']
            offset = _calculate_offset(humidity, initial, intensity)
        activate(raum)
        return "Luftfeuchte", "Luftfeuchte", "%.2f" % offset, 75, "/m_raum/%s/luftfeuchte/" % raum.id


def get_local_settings_page(request, raum):

    if request.method == "GET":

        hparams = raum.get_humidity_params()
        haus = raum.etage.haus
        return render_response(request, "m_raum_luftfeuchte.html",
                               {"raum": raum, "haus": haus, 'initial': hparams['initial'], 'intensity': hparams['intensity']})

    elif request.method == "POST":  # and request.user == raum.etage.haus.eigentuemer:

        if request.POST.get('singleroom', False):
            name = request.POST['name']
            obj_id = long(request.POST.get('id', 0))
            number = request.POST['number']
            raum = Raum.objects.get(id=obj_id)
            raum.set_humidity_params(**{name: float(number)})
        return HttpResponse()


def get_local_settings_page_haus(request, haus):

    if request.method == "GET":
        eundr = []
        for etage in haus.etagen.all():
            e = {etage: []}
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                e[etage].append(raum)
            eundr.append(e)
        return render_response(request, "m_luftfeuchte.html", {"haus": haus, "eundr": eundr})

    elif request.method == "POST":

        if 'submit' in request.POST:  # manual form submission

            initial = float(request.POST.get('initial', '0'))
            intensity = float(request.POST.get('intensity', '0'))
            rooms = request.POST.getlist('rooms')

            for room_id in rooms:
                room_id = int(room_id)
                raum = Raum.objects.get(id=room_id)
                raum.set_humidity_params(initial, intensity)

            return render_redirect(request, '/m_luftfeuchte/%s/' % haus.id)

        else:  # ajax submission
            raum_id = request.POST.get('raum_id', '0')

            try:
                raum = Raum.objects.get(id=int(raum_id))
            except (Raum.DoesNotExist, ValueError, TypeError):
                return HttpResponse('error')

            field_name = request.POST.get('field_name')

            if field_name == "initial":
                field_value = int(request.POST.get('field_value', '0'))
                raum.set_humidity_params(field_value, None)

            elif field_name == "intensity":
                field_value = float(request.POST.get('field_value', '0'))
                raum.set_humidity_params(None, field_value)

            return HttpResponse('done')


def activate(hausoderraum):
    if 'luftfeuchte' not in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['luftfeuchte'])
