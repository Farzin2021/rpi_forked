# -*- coding: utf-8 -*-

from datetime import datetime
from heizmanager.render import render_response, render_redirect
import logging
from django.http import HttpResponse
import pytz
import json
from fabric.api import local
import sqlite3
import shutil, os
from django.utils.text import slugify


PATH = "/home/pi/rpi"
# PATH = "/Users/max/Documents/hm/rpi"


def get_name():
    return 'Backupcontrol'


def get_cache_ttl():
    return 86400


def is_togglable():
    return True


def get_offset(haus, raum=None):
    return 0.0


def is_hidden_offset():
    return False


def calculate_always_anew():
    return False


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/backupcontrol/'>Backup Control</a>" % haus.id


def get_global_description_link():
    desc = u"Automatische und manuelle Sicherungskopien aller Einstellungen."
    desc_link = "http://support.controme.com/backup-control/"
    return desc, desc_link



def get_global_settings_page(request, haus):

    backuplist = list(reversed(sorted_ls(PATH + "/backupcontrol/backups/")))
    berlin = pytz.timezone("Europe/Berlin")
    now = datetime.now(berlin)
    now = now.strftime("%Y%m%d%H%M%S")
    params = haus.get_module_parameters()
    bparams = params.get('backupcontrol', {'active': True}) # “Automatische Backups aktivieren” as default

    if request.method == "GET":

        logging.warning(request.GET)

        if 'dbdl' in request.GET:
            db = open(PATH + "/db.sqlite3")
            response = HttpResponse(db, content_type='application/text')
            response['Content-Disposition'] = 'attachment; filename=dbdl.zip'
            return response

        elif 'backupnow' in request.GET:
            local("cp %s/db.sqlite3 %s/backupcontrol/backups/%s_manual_backup.db" % (PATH, PATH, now))
            backuplist = list(reversed(sorted_ls(PATH + "/backupcontrol/backups/")))
            return render_response(request, "m_settings_backupcontrol.html",
                                   {"haus": haus, "bklist": backuplist, "active": bparams['active']})

        elif 'aactive' in request.GET:
            params.setdefault('backupcontrol', dict())
            if request.GET['aactive'] == 'true':
                params['backupcontrol']['active'] = True
            else:
                params['backupcontrol']['active'] = False
            haus.set_module_parameters(params)
            return HttpResponse()

        else:
            return render_response(request, "m_settings_backupcontrol.html",
                                   {"haus": haus, "bklist": backuplist, "active": bparams['active']})

    if request.method == "POST":

        if 'dbul' in request.POST:

            db = request.FILES.get('db', None)
            if db is None:
                message = u"Bitte Datenbank zum Hochladen auswählen."
                return render_response(request, "m_settings_backupcontrol.html",
                                       {"haus": haus, "bklist": backuplist, "message": message,
                                        "active": bparams['active']})

            filename = slugify(request.FILES['db'].__dict__['_name'])
            dest_path = "%s/backupcontrol/backups/%s_%s" % (PATH, now, filename)
            with open(dest_path, 'wb') as dest:
                shutil.copyfileobj(db, dest)

            # check valid datase / schema compatibility
            if not is_compatible_db(dest_path):
                message = u"Hochgeladene Datenbank ist beschädigt oder inkompatibel zum aktuellen Softwarestand."
                local("rm %s" % dest_path)
                return render_response(request, "m_settings_backupcontrol.html",
                                       {"haus": haus, "bklist": backuplist, "message": message,
                                        "active": bparams['active']})

            # check if uploaded db fits this miniserver
            elif not is_correct_db(dest_path):
                message = u"Hochgeladene Datenbank gehört zu einem anderen Miniserver. " \
                          u"Kontaktieren Sie bitte support@controme.com."
                local("rm %s" % dest_path)
                return render_response(request, "m_settings_backupcontrol.html",
                                       {"haus": haus, "bklist": backuplist, "message": message,
                                        "active": bparams['active']})

            else:
                local("cp %s/db.sqlite3 %s/backupcontrol/backups/%s_manual_backup.db" % (PATH, PATH, now))
                local(u"""cp %s %s/db.sqlite3""" % (dest_path, PATH))
                local("echo 'flush_all' | nc -q 1 localhost 11211")

            backuplist = list(reversed(sorted_ls(PATH + "/backupcontrol/backups/")))
            return render_response(request, "m_settings_backupcontrol.html",
                                   {"haus": haus, "bklist": backuplist, "message": "Upload erfolgreich.",
                                    "active": bparams['active']})

        if 'select_backup_file' in request.POST:

            filename = request.POST.get('select_backup_file')
            src_path = u"%s/backupcontrol/backups/%s" % (PATH, filename)

            if is_compatible_db(src_path):
                # solange es keine moeglichkeit gibt, dbs manuell zu loeschen, sprudelt uns das alles voll.
                # local("cp %s/db.sqlite3 %s/backupcontrol/backups/%s_automatic_backup.db" % (PATH, PATH, now))
                local(u"""cp %s %s/db.sqlite3""" % (src_path, PATH))
                local("echo 'flush_all' | nc -q 1 localhost 11211")
                message = "Datenbank erfolgreich wiederhergestellt."
            else:
                message = u"Ausgewählte Datenbank ist inkompatibel zum aktuellen Softwarestand."

            return render_response(request, "m_settings_backupcontrol.html",
                                   {"haus": haus, "bklist": backuplist, "message": message,
                                    "active": bparams['active']})

        return render_redirect(request, "/m_setup/%s/backupcontrol/" % haus.id)


def sorted_ls(path):
    mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
    try:
        return list(sorted(os.listdir(path), key=mtime))
    except OSError:
        local("mkdir %s/backupcontrol/backups" % PATH)
        return []


def is_compatible_db(path):

    db = sqlite3.connect(PATH + "/db.sqlite3")
    c = db.cursor()
    c.execute("select * from django_migrations")
    current_migrations = c.fetchall()
    c.close()
    db.close()

    try:
        db = sqlite3.connect(path)
        c = db.cursor()
        c.execute("select * from django_migrations")
        backup_migrations = c.fetchall()
        c.close()
        db.close()
    except sqlite3.DatabaseError:
        backup_migrations = [('databaseerror', 0, 0)]

    if {(c[1], c[2]) for c in current_migrations} == {(b[1], b[2]) for b in backup_migrations} or len(backup_migrations) > len(current_migrations):
        return True
    else:
        return False


def is_correct_db(path):

    import heizmanager.network_helper as nh
    mac = nh.get_mac()

    try:
        db = sqlite3.connect(path)
        cur = db.cursor()
        cur.execute("select name from heizmanager_rpi where name='%s'" % mac)
        res = cur.fetchone()
        cur.close()
        db.close()
    except (sqlite3.DatabaseError, sqlite3.OperationalError):
        return False
    else:
        try:
            if mac == res[0]:
                return True
            else:
                return False
        except TypeError:
            return False
