# -*- coding: utf-8 -*-
import ast
import _ast
import operator
import heizmanager.cache_helper as ch

operators = {
    _ast.Not: operator.not_,
    _ast.Add: operator.add,
    _ast.Sub: operator.sub,
    _ast.Mult: operator.mul,
    _ast.Div: operator.div,
    _ast.BitOr: operator.or_,
    _ast.BitAnd: operator.and_,
    _ast.Lt: operator.lt,
    _ast.LtE: operator.le,
    _ast.Eq: operator.eq,
    _ast.NotEq: operator.ne,
    _ast.Gt: operator.gt,
    _ast.GtE: operator.ge,
    _ast.Mod: operator.mod
}


class EvalVisitor(ast.NodeVisitor):
    def __init__(self, **kwargs):
        self._namespace = kwargs

    def visit_Name(self, node):
        if node.id not in self._namespace:
            raise KeyError("unbekannte variable")  # todo hier koennte man natuerlich die variable benennen
        return self._namespace[node.id]

    def visit_Num(self, node):
        return node.n

    def visit_NameConstant(self, node):
        return node.value

    def visit_UnaryOp(self, node):
        val = self.visit(node.operand)
        return int(operators[type(node.op)](val))

    def visit_bool(self, node):
        return int(node)

    def visit_BoolOp(self, node):
        lhs = self.visit(node.values[0])
        for _ in node.values[1:]:
            rhs = self.visit(node.values[1])
            lhs = lhs and rhs
        return lhs

    def visit_BinOp(self, node):
        lhs = self.visit(node.left)
        rhs = self.visit(node.right)
        if type(node.op) in [_ast.BitOr, _ast.BitAnd]:
            a = operators[type(node.op)](bool(lhs), bool(rhs))
        else:
            a = operators[type(node.op)](lhs, rhs)
        return a

    def visit_Compare(self, node):
        lhs = self.visit(node.left)
        rhs = self.visit(node.comparators[0])
        if len(node.ops) > 1:
            rhnode = ast.Compare()
            rhnode.left = operators[type(node.ops[0])](lhs, rhs)
            rhnode.comparators = node.comparators[1:]
            rhnode.ops = node.ops[1:]
            return self.visit(rhnode)
        else:
            hysterese = self._namespace.get('hysterese', 0)
            if hysterese != 0:
                last = ch.get('fps_%s_%s%s%s' % (self._namespace.get('fpsid'), node.left.id if hasattr(node.left, "id") else node.left.__dict__['col_offset'], str(type(node.ops[0]).__name__), node.comparators[0].id if hasattr(node.comparators[0], "id") else node.comparators[0].__dict__['col_offset']))  # todo fpsid muss uebergeben werden!
                if last is not None:
                    ret = int(operators[type(node.ops[0])](lhs, rhs))
                    if last == 1 and ret == 0:
                        if operators[type(node.ops[0])] in {operator.le, operator.lt}:
                            if int(operators[type(node.ops[0])](lhs-hysterese, rhs)):
                                ret = last
                            else:
                                pass
                        else:  # gt, ge, eq, ne
                            if int(operators[type(node.ops[0])](lhs, rhs-hysterese)):
                                ret = last
                            else:
                                pass
                    elif last == 0 and ret == 1:
                        if operators[type(node.ops[0])] in {operator.le, operator.lt}:
                            if int(operators[type(node.ops[0])](lhs+hysterese, rhs)):
                                pass
                            else:
                                ret = last
                        else:  # gt, ge, eq, ne
                            if int(operators[type(node.ops[0])](lhs, rhs+hysterese)):
                                pass
                            else:
                                ret = last
                    else:
                        ret = last
                else:
                    ret = int(operators[type(node.ops[0])](lhs, rhs))

                ch.set('fps_%s_%s%s%s' % (self._namespace.get('fpsid'), node.left.id if hasattr(node.left, "id") else node.left.__dict__['col_offset'], str(type(node.ops[0]).__name__), node.comparators[0].id if hasattr(node.comparators[0], "id") else node.comparators[0].__dict__['col_offset']), ret)
                return ret
            else:
                return int(operators[type(node.ops[0])](lhs, rhs))

    def visit_IfExp(self, node):
        ret = self.visit(node.test)
        return self.visit(node.body) if ret else self.visit(node.orelse)

    def visit_Assign(self, node):
        ret = dict((name.id, self.visit(node.value)) for name in node.targets)
        self._namespace.update(ret)  # hiermit funktionieren variablendeklarationen. kann auch ueberschreiben!
        return ret

    def visit_Expr(self, node):
        return self.visit(node.value)

    def visit_Module(self, node):
        ret = {}
        # man kann hier mehrere statements aneinander reihen, durch ';' getrennt
        # vorsicht (u.a. wegen last()), dass jedes statement nur einmal ausgewertet wird
        ret.update({'A0': self.visit(node.body[0])})
        return ret

    def visit_Call(self, node):

        if node.func.id == 'last':
            if len(node.args) != 1:
                raise ValueError(u"Funktion last() nimmt 1 Parameter, Sie übergeben %s" % len(node.args))
            if node.args[0].__dict__.get('id') is None:
                raise ValueError(u"Funktion last() nimmt eine Variable als Parameter, Sie übergeben eine %s" % ('Zeichenkette' if 's' in node.args[0].__dict__ else 'Zahl'))
            if node.args[0].__dict__.get('id', '') not in self._namespace and node.args[0].__dict__.get('id', '') != 'A0':
                raise ValueError(u"Funktion last(): unbekannter Parameter '%s'" % node.args[0].__dict__.get('id'))

            return None

        elif node.func.id == 'max':
            try:
                return max([getattr(self, 'visit_%s' % type(n).__name__)(n) for n in node.args])
            except:
                raise ValueError(u"Funktion max(): illegaler Parameter")

        elif node.func.id == 'min':
            try:
                return min([getattr(self, 'visit_%s' % type(n).__name__)(n) for n in node.args])
            except:
                raise ValueError(u"Funktion min(): illegaler Parameter")

        elif node.func.id == 'avg':
            try:
                return sum([getattr(self, 'visit_%s' % type(n).__name__)(n) for n in node.args]) / len(node.args)
            except:
                raise ValueError(u"Funktion avg(): illegaler Parameter")

        return None

    def generic_visit(self, node):
        raise ValueError("malformed node or string: " + repr(node) + " " + str(type(node)))


def parse(formula, variables):
    a = ast.parse(formula)
    v = EvalVisitor(**variables)
    # print "fps/parser::v.visit(a)", v.visit(a)
    print "fps/parser::ast.dump(a)", ast.dump(a)
    print "fps/parser::variables", variables
    return v.visit(a)
