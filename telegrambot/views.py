# -*- coding: utf-8 -*-

from heizmanager.render import render_redirect, render_response
import json
from django.contrib.auth.models import User
from fabric.api import local
import heizmanager.cache_helper as ch
from heizmanager.models import sig_new_output_value, sig_new_temp_value
import logging, json
from django.http import HttpResponse


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/telegrambot/'>Chat-Bot</a>" % haus.id


def get_global_description_link():
    desc = "Nachrichten per Messenger auf Smartphones Mac und PC."
    desc_link = "http://support.controme.com/chat-und-notifications-per-telegram/"
    return desc, desc_link


def get_global_settings_page(request, haus):

    if request.method == "GET":

        try:
            with open('telegrambot/tgbotconfig.json', 'r') as config:
                config = json.load(config)
        except (IOError, ValueError):
            config = {}

        apikey = config.get("API_KEY", "")
        if len(apikey):
            apikey = apikey[:10] + "*****"
        allowed_users = config.get('allowed_users', dict())
        users = User.objects.all()
        ret = []
        for usr in users:
            ret.append({
                "id": usr.id,
                "email": usr.email,
                "tgname": [k for k, v in allowed_users.items() if v.get('email') == usr.email]
            })

        return render_response(request, "m_settings_telegrambot.html", {"haus": haus, 'apikey': apikey, "users": ret})

    elif request.method == "POST":

        try:
            with open('telegrambot/tgbotconfig.json', 'r') as config:
                config = json.load(config)
        except (IOError, ValueError):
            config = {}

        apikey = request.POST.get('apikey', '')
        if apikey.strip() != '' and not apikey.strip().endswith("*****"):
            try:
                local("python telegrambot/check_apikey.py %s" % apikey)
            except:
                err = u"Illegaler / ungültiger API Key."
                return render_response(request, "m_settings_telegrambot.html", {"haus": haus, "apikey": apikey, "err": err})
            config['API_KEY'] = apikey
        else:
            config['API_KEY'] = ''

        config.setdefault('allowed_users', dict())
        users = User.objects.all()
        allowed_users = set()
        for key in request.POST:
            if key.startswith('user_') and len(request.POST[key]):
                usrid = int(key.split('_')[1])
                tgusrname = request.POST[key]
                if tgusrname[0] == '@':
                    tgusrname = tgusrname[1:]
                for usr in users:
                    if usr.id == usrid:
                        config['allowed_users'][tgusrname] = {
                            "tg_id": config['allowed_users'].get(tgusrname, dict()).get('tg_id', ''),
                            "email": usr.email
                        }
                        allowed_users.add(tgusrname)

        for usrname in config['allowed_users'].keys():
            if usrname not in allowed_users:
                del config['allowed_users'][usrname]
                try:
                    del config['notifications'][usrname]
                except:
                    pass

        json.dump(config, open("telegrambot/tgbotconfig.json", "wb"))
        try:
            local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart telegrambot")
        except:
            pass
        return render_redirect(request, "/m_setup/%s/telegrambot/" % haus.id)


def connect_signals(request):  # todo wahrscheinlich sollte das einfach einen request nehmen, weil dann koennen wir es direkt aus der bot.py aufrufen
    try:
        with open('telegrambot/tgbotconfig.json', 'r') as config:
            config = json.load(config)
    except Exception as e:
        logging.exception("exception opening tgbotconfig")
        config = {}

    alarms = set()
    for user, notifications in config.get('notifications', dict()).items():
        for varn in notifications.get('varn', list()):
            alarms.add(varn.keys()[0])

    if len(alarms):
        sig_new_temp_value.connect(handle_temp_value)
        sig_new_output_value.connect(handle_output_value)
        ch.set('tgbotalarms', alarms, 600)
    else:
        try:
            sig_new_temp_value.disconnect(receiver=handle_temp_value)
            sig_new_output_value.disconnect(receiver=handle_output_value)
        except Exception as e:
            logging.exception("error disconnecting signals in tgbot")
        ch.set('tgbotalarms', set(), 600)

    if request is None:
        return HttpResponse(json.dumps(list(alarms)))
    else:
        return HttpResponse("")


def handle_temp_value(sender, **kwargs):
    varn = ch.get('tgbotalarms') or json.loads(connect_signals(None).content)
    if not isinstance(varn, set):
        return
    if kwargs['name'] in varn:
        local("/usr/bin/python /home/pi/rpi/telegrambot/bot.py %s %s %s" % ("sensor", kwargs['name'], kwargs['value']))
    else:
        return


def handle_output_value(sender, **kwargs):
    varn = ch.get('tgbotalarms') or json.loads(connect_signals(None).content)
    if varn is None:
        return
    if kwargs['name'] in varn:
        local("/usr/bin/python /home/pi/rpi/telegrambot/bot.py %s %s %s" % ("ausgang", kwargs['name'], kwargs['value']))
    else:
        return
