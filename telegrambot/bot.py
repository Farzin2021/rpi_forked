# -*- coding: utf-8 -*-

import telepot
import logging
from telepot.delegate import include_callback_query_chat_id, pave_event_space, per_chat_id, create_open
import json
import requests
import os
from telepot.helper import SafeDict
from apscheduler.schedulers.background import BackgroundScheduler
from fabric.api import local
import sys
import copy
try:
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates
except ImportError:
    plt = mdates = None
from datetime import datetime
import time


notifications = SafeDict()
scheduler = BackgroundScheduler()
haus_id = None


def get_data(query, username, config, entityid=None):

    url = "http://127.0.0.1/get/json/v1/%s/%s/%s"
    email = config.get('allowed_users', dict()).get(username, dict()).get('email')
    if not email:
        logging.warning("no known email address for current user")
        return []
    headers = {'from': email}

    global haus_id
    data = requests.get(url % (haus_id, query, "" if entityid is None else "%s/" % entityid), headers=headers).content
    data = json.loads(data)
    return data


def send_alarm(bot, chat_id, username, alarm):

    global notifications, haus_id
    with open('telegrambot/tgbotconfig.json', 'r') as config:
        config = json.load(config)

    ret = ""
    ctrl_marker = get_data("marker", username, config)
    for ctrldev in ctrl_marker:
        notifications.setdefault('sent', dict())

        if ctrldev['marker'] != 'green':
            if notifications.get(username, dict()).get('ctrl', False):
                if not notifications['sent'].get(ctrldev['name'], False):
                    ret += u'%s überträgt %s\n' % (ctrldev['name'], 'nicht' if ctrldev['marker'] == 'red' else 'unregelmäßig')
                notifications['sent'][ctrldev['name']] = True

        else:
            notifications['sent'][ctrldev['name']] = False

        if notifications.get(username, dict()).get('sens', False):
            for name, _marker in ctrldev['sensoren'].items():
                if not notifications['sent'].get("rf_%s_%s" % (ctrldev['name'], name), False):
                    if _marker != 'green':
                        try:
                            _name = int(name)
                            _name = "Funksensor %s an %s" % (_name, ctrldev['name'])
                        except:
                            _name = name.replace('_', '-')
                        ret += u'%s überträgt %s\n' % (_name, 'nicht' if _marker == 'red' else 'unregelmäßig')
                        notifications['sent']["rf_%s_%s" % (ctrldev['name'], name)] = True
                    else:
                        notifications['sent']["rf_%s_%s" % (ctrldev['name'], name)] = False
                else:
                    if _marker == 'green':
                        notifications['sent']["rf_%s_%s" % (ctrldev['name'], name)] = False
            for name, _marker in ctrldev['aktoren'].items():
                if not notifications['sent'].get("rf_%s_%s" % (ctrldev['name'], name), False):
                    if _marker != 'green':
                        _name = "Funkaktor %s an %s" % (name, ctrldev['name'])
                        notifications['sent']["rf_%s_%s" % (ctrldev['name'], name)] = True
                        ret += u'%s überträgt %s\n' % (_name, 'nicht' if _marker == 'red' else 'unregelmäßig')
                    else:
                        notifications['sent']["rf_%s_%s" % (ctrldev['name'], name)] = False
                else:
                    if _marker == 'green':
                        notifications['sent']["rf_%s_%s" % (ctrldev['name'], name)] = False

    if len(ret):
        url = 'https://api.telegram.org/bot%s/sendMessage' % config['API_KEY']
        requests.post(url, data={'text': ret, 'chat_id': chat_id, 'parse_mode': "Markdown"})


def send_motion_alarm(hausid, raumid):

    global haus_id
    haus_id = hausid
    with open('telegrambot/tgbotconfig.json', 'r') as config:
        config = json.load(config)

    for username, details in config.get('allowed_users', dict()).items():
        if config.get('notifications', dict()).get(username, dict()).get('movm', False):
            rooms = get_data("rooms", username, config)
            for etage in rooms:
                for room in etage['raeume']:
                    if str(room['id']) == raumid:  # der benutzer sieht diesen raum
                        url = 'https://api.telegram.org/bot%s/sendMessage' % config['API_KEY']
                        txt = "Bewegungsmelderalarm in Raum _%s - %s_" % (etage['etagenname'], room['name'])
                        requests.post(url, data={'text': txt, 'chat_id': details['tg_id'], 'parse_mode': "Markdown"})


def send_var_alarm(typ, var, value):

    with open('telegrambot/tgbotconfig.json', 'r') as config:
        config = json.load(config)

    try:
        val = float(value)
    except:
        return

    config_changed = False
    for username, alarms in config.get('notifications', dict()).items():
        varn = alarms.get('varn', list())
        for item in varn:
            varname, cmpval = item.items()[0]
            if varname == var:

                lastsent = config.get('notifications', dict()).get(username, dict()).get('varn_sent', dict()).get(varname)

                sendalarm = False
                if '*' in var and lastsent is not None and ((val < cmpval[0] and lastsent > cmpval[0]) or (val > cmpval[0] and lastsent < cmpval[0])):
                    sendalarm = True

                if '*' not in var and lastsent is not None and val != lastsent:
                    sendalarm = True

                if lastsent is None or sendalarm:
                    config['notifications'].setdefault(username, dict())
                    config['notifications'][username].setdefault('varn_sent', dict())
                    config['notifications'][username]['varn_sent'][varname] = val
                    config_changed = True
                    url = 'https://api.telegram.org/bot%s/sendMessage' % config['API_KEY']
                    if '*' in var:
                        txt = 'Sensor %s misst %s %sK' % (cmpval[1], 'ueber' if val > cmpval[0] else "unter", cmpval[0])
                    else:
                        txt = 'Ausgang %s and %s steht jetzt auf %s' % (var.split('_')[1], var.split('_')[0], val)
                    requests.post(url, data={'text': txt, 'chat_id': config.get('allowed_users', dict()).get(username, dict()).get('tg_id')})

    if config_changed:
        json.dump(config, open("telegrambot/tgbotconfig.json", "wb"))


class ContromeBot(telepot.helper.ChatHandler):

    class SollState:
        DEFAULT = 0
        AWAITINGSOLLRAUM = 1
        AWAITINGSOLLTEMP = 2

    def __init__(self, *args, **kwargs):
        super(ContromeBot, self).__init__(*args, **kwargs)
        with open('telegrambot/tgbotconfig.json', 'r') as config:
            self.config = json.load(config)
        self.state = ContromeBot.SollState.DEFAULT
        self.awaiting_comparison_value_for_sensor = None
        self.sollraumauswahl = None
        global haus_id
        self.haus_id = haus_id

    def send_message(self, chat_id, text, parse_mode='Markdown', reply_markup=None):
        url = 'https://api.telegram.org/bot%s/sendMessage' % self.config['API_KEY']
        requests.post(url, data={'text': text, 'chat_id': chat_id, 'parse_mode': parse_mode, 'reply_markup': reply_markup})

    def send_photo(self, chat_id, photo_name):
        url = 'https://api.telegram.org/bot%s/sendPhoto' % self.config['API_KEY']
        requests.post(url, data={'chat_id': chat_id}, files={"photo": open(photo_name, 'rb')})

    def on_chat_message(self, msg):
        if self._is_allowed_user(msg):
            content_type, chat_type, chat_id = telepot.glance(msg)
            if content_type == "text":
                msg_text = msg['text'].lower()
                global notifications

                if self.state == ContromeBot.SollState.AWAITINGSOLLTEMP:
                    soll = None
                    offset = 0.0
                    try:
                        if '-' in msg_text or '+' in msg_text:
                            offset = float(msg_text.replace(',', '.'))
                        else:
                            soll = float(msg_text.replace(',', '.'))
                    except:
                        self.send_message(chat_id, u'Ungültiger Wert.')
                    else:
                        data = get_data("temps", msg['from']['username'], self.config)
                        if self.sollraumauswahl is None:
                            self.send_message(chat_id, u"Kein Raum ausgewählt.")
                        elif self.sollraumauswahl == "alle":
                            for etage in data:
                                for raum in etage['raeume']:
                                    r = requests.post("http://127.0.0.1/set/json/v1/%s/soll/%s/" % (self.haus_id, raum['id']), data={'soll': (soll or raum['solltemperatur']) + offset})
                            self.send_message(chat_id, "Erledigt.")
                        elif self.sollraumauswahl.startswith("etage"):
                            etage_id = self.sollraumauswahl.split('_')[1]
                            for etage in data:
                                if etage['id'] == int(etage_id):
                                    for raum in etage['raeume']:
                                        r = requests.post("http://127.0.0.1/set/json/v1/%s/soll/%s/" % (self.haus_id, raum['id']), data={'soll': (soll or raum['solltemperatur']) + offset})
                                    self.send_message(chat_id, "Erledigt.")
                                    break
                        else:
                            for etage in data:
                                for raum in etage['raeume']:
                                    if int(self.sollraumauswahl) == raum['id']:
                                        r = requests.post("http://127.0.0.1/set/json/v1/%s/soll/%s/" % (self.haus_id, raum['id']), data={'soll': (soll or raum['solltemperatur']) + offset})
                                        self.send_message(chat_id, "Erledigt.")
                                        break
                    self.state = ContromeBot.SollState.DEFAULT

                elif self.state == ContromeBot.SollState.AWAITINGSOLLRAUM:
                    # haette ein callback sein muessen
                    self.state = ContromeBot.SollState.DEFAULT

                elif self.awaiting_comparison_value_for_sensor is not None:
                    try:
                        cmp = float(msg_text.replace(',', '.'))
                    except:
                        self.send_message(chat_id, "Ungültiger Wert.")
                    else:
                        notifications.setdefault(msg['from']['username'], dict())
                        notifications[msg['from']['username']].setdefault('varn', list())
                        notifications[msg['from']['username']]['varn'].append({self.awaiting_comparison_value_for_sensor[0]: (cmp, self.awaiting_comparison_value_for_sensor[1])})
                        self.config.setdefault('notifications', dict())
                        self.config['notifications'].setdefault(msg['from']['username'], dict())
                        self.config['notifications'][msg['from']['username']]['varn'] = copy.deepcopy(notifications[msg['from']['username']]['varn'])
                        json.dump(self.config, open("telegrambot/tgbotconfig.json", "wb"))
                        self.awaiting_comparison_value_for_sensor = None
                        self.send_message(chat_id, "Benachrichtigung angelegt.")
                        requests.get("http://127.0.0.1/set/tgbot/signals/")

                elif msg_text.startswith("/soll"):
                    data = get_data("rooms", msg['from']['username'], self.config)
                    raeume = [[{'text': 'alle', 'callback_data': 'alle'}]]
                    for etage in data:
                        raeume.append([{'text': etage['etagenname'], "callback_data": "etage_%s" % etage['id']}])
                        _r = []
                        for raum in etage['raeume']:
                            if len(_r) == 2:
                                raeume.append(_r)
                                _r = []
                            _r.append({'text': "%s" % raum['name'], "callback_data": str(raum['id'])})
                        if len(_r):
                            if len(_r) < 2:
                                _r.append({'text': " ", "callback_data": " "})
                            raeume.append(_r)
                    markup = {'inline_keyboard': raeume}
                    self.state = ContromeBot.SollState.AWAITINGSOLLRAUM
                    self.send_message(chat_id, u"Bitte Raum auswählen.", reply_markup=json.dumps(markup))

                elif msg_text.startswith('/temps'):
                    data = get_data("temps", msg['from']['username'], self.config)
                    ret = ""
                    for etage in data:
                        ret += "\n*%s*\n" % etage['etagenname']
                        for raum in etage['raeume']:
                            ret += "  *%s*\n" % raum['name']
                            ret += "    Ziel: %.2f, Ist: %s\n" % (raum['solltemperatur']+raum['total_offset'], "%.2f" % raum['temperatur'] if raum['temperatur'] else "-")
                    self.send_message(chat_id, ret)

                elif msg_text.startswith("/tempdiag"):
                    data = get_data("rooms", msg['from']['username'], self.config)
                    raeume = []
                    for etage in data:
                        _r = []
                        for raum in etage['raeume']:
                            if len(_r) == 2:
                                raeume.append(_r)
                                _r = []
                            _r.append({'text': "%s" % raum['name'], "callback_data": "tempdiag_%s" % raum['id']})
                        if len(_r):
                            if len(_r) < 2:
                                _r.append({'text': " ", "callback_data": " "})
                            raeume.append(_r)
                    markup = {'inline_keyboard': raeume}
                    self.state = ContromeBot.SollState.AWAITINGSOLLRAUM
                    self.send_message(chat_id, u"Bitte Raum auswählen.", reply_markup=json.dumps(markup))

                elif msg_text.startswith("/hr"):
                    data = get_data("hr", msg['from']['username'], self.config)
                    ret = ""
                    for item in data:
                        ret += "*%s*\n" % item['name']
                        if item['name'].startswith("Diff"):
                            if 'S1' in item:
                                ret += "  S1: %s\n" % item['S1']
                            if 'S2' in item:
                                ret += "  S2: %s\n" % item['S2']
                            if 'S3' in item:
                                ret += "  S3: %s\n" % item['S3']
                            ret += "  Ziel: %s\n" % item['ziel']
                            ret += "  Ausgang: %s\n\n" % "-" if item['beam'] is None else '1' if item['beam'] else '0'
                        elif item['name'].startswith('Vorl'):
                            if item.get('state') is not None:
                                if item['state'].get('inflow_cold') is not None:
                                    ret += "  Eingang RL: %s\n" % item['state'].get('inflow_cold')
                                if item['state'].get('inflow_hot') is not None:
                                    ret += "  Eingang Warm: %s\n" % item['state'].get('inflow_hot')
                                if item['state'].get('outflow') is not None:
                                    ret += "  Ausgang: %s\n" % item['state'].get('outflow')
                                ret += "  Ziel: %s\n" % item['state'].get('setpoint')
                                ret += "  Position: %s\n\n" % item['state'].get('position')
                            else:
                                ret += "  Fehler!"
                    self.send_message(chat_id, ret)

                elif msg_text == "/status":
                    data = get_data("marker", msg['from']['username'], self.config)
                    ret = u''
                    for ctrldev in data:
                        if ctrldev['marker'] != 'green':
                            ret += u'%s überträgt %s\n' % (ctrldev['name'], 'nicht' if ctrldev['marker'] == 'red' else 'unregelmäßig')
                        if ctrldev['marker'] != 'red':
                            for name, _marker in ctrldev['sensoren'].items():
                                if _marker != 'green':
                                    ret += u'%s überträgt %s\n' % (name, 'nicht' if _marker == 'red' else 'unregelmäßig')
                            for name, _marker in ctrldev['aktoren'].items():
                                if _marker != 'green':
                                    ret += u'%s überträgt %s\n' % (name, 'nicht' if _marker == 'red' else 'unregelmäßig')
                    if not len(ret):
                        ret = u'Alle Geräte übertragen regelmäßig.'
                    else:
                        ret += u'Alle anderen Geräte übertragen regelmäßig.'
                    self.send_message(chat_id, ret)

                elif msg_text == "/system":
                    res = os.popen("sudo supervisorctl -c /etc/supervisor/supervisord.conf status").read()
                    if res.count('RUNNING') == res.count("\n"):
                        self.send_message(chat_id, u'Läuft alles.')
                    else:
                        ret = ''
                        for line in res.strip().split('\n'):
                            if 'RUNNING' not in line:
                                if 'FATAL' in line or 'EXITED' in line:
                                    ret += '%s ist gecrashed\n' % line.split(' ', 1)[0]
                                elif 'STOPPED' in line:
                                    ret += '%s wurde angehalten\n' % line.split(' ', 1)[0]
                                elif 'STARTING' in line:
                                    ret += '%s startet gerade\n' % line.split(' ', 1)[0]
                                elif 'STOPPING' in line:
                                    ret += '%s stoppt gerade\n' % line.split(' ', 1)[0]
                                else:
                                    ret += '%s in unbekanntem Zustand\n' % line.split(' ', 1)[0]
                        self.send_message(chat_id, ret)

                elif msg_text.startswith("/alarm"):

                    un = notifications.get(msg['from']['username'])
                    movm = sens = ctrl = False
                    varn = list()
                    if un is not None:
                        movm = un.get('movm', False)
                        sens = un.get('sens', False)
                        ctrl = un.get('ctrl', False)
                        varn = un.get('varn', list())
                    ret = "*Benachrichtigungen* für\n\n"
                    ret += "Bewegungsmelder: %s\n" % ("an" if movm else "aus")
                    ret += "Sensorenzustand: %s\n" % ("an" if sens else "aus")
                    ret += "Gateway/Funk: %s\n" % ("an" if ctrl else "aus")

                    for i, item in enumerate(varn):
                        name, val = item.items()[0]
                        if '_' in name:
                            name = str(name)
                            ret += "\n%s: Ausgang %s an %s" % (i+1, name.split('_')[1], name.split('_')[0])
                        elif '*' in name:
                            ret += "\n%s: Sensor %s um %sK" % (str(i+1), str(val[1]).replace('_', '\_'), str(val[0]))

                    ikb = [
                        [{'text': 'Bewegungsmelder %s' % ('aktivieren' if not movm else "deaktivieren"), "callback_data": "toggle_movm"}],
                        [{"text": "Sensoren %s" % ("aktivieren" if not sens else "deaktivieren"), "callback_data": "toggle_sens"}],
                        [{"text": "Gateway/Funk %s" % ("aktivieren" if not ctrl else "deaktivieren"), "callback_data": "toggle_ctrl"}],
                        [{"text": "Variablen Alarm bearbeiten", "callback_data": "varn_edit"}]
                    ]
                    markup = {"inline_keyboard": ikb}
                    self.send_message(chat_id, ret, reply_markup=json.dumps(markup))

                elif msg_text.startswith('/offset'):
                    pass

                else:
                    ret = "*Hilfe*\n\n"
                    ret += "*/soll*\nErmöglicht die Änderung der Solltemperatur in einzelnen Räumen, Etagen oder dem ganzen Haus.\n\n"
                    ret += "*/temps*\nZeigt die aktuellen Ist- und Zieltemperaturen für jeden Raum an.\n\n"
                    ret += "*/tempdiag*\nZeigt Loggingdiagramme an.\n\n"
                    ret += "*/alarm*\n(De-)Aktivierung von Benachrichtigungen beim Ausfall von Sensoren, Gateways und Funkcontrollern. Geprüft wird vierstelstündlich, benachrichtigt pro Gerät und Ausfall einmalig.\n"
                    ret += "Zudem können Benachrichtigungen bei Auslösung von Bewegungsmelderalarmen (de-)aktiviert werden.\n\n"
                    ret += "*/hr*\nZeigt eine Übersicht über Differenz- & Vorlauftemperaturregelungen.\n\n"
                    ret += "*/status*\nZeigt den momentanen Übertragungsstatus von Sensoren, Gateways und Funkcontrollern an.\n\n"
                    ret += "*/system*\nZeigt den Status der Systemprozesse des Miniservers an."
                    self.send_message(chat_id, ret)

                    markup = {'keyboard': [
                        ["/soll", "/temps", "/tempdiag", "/alarm"],
                        ["/hr", "/status", "/system", "/hilfe"]
                    ], 'resize_keyboard': True}
                    self.send_message(chat_id, u"Bitte Aktion auswählen.", reply_markup=json.dumps(markup))

            else:
                return
        else:
            return

    def on_callback_query(self, msg):
        query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
        global notifications

        if query_data.startswith("varn"):

            if query_data.endswith("_edit"):
                ikb = [
                    [{'text': "Regel hinzufügen", "callback_data": "varn_add_1"}]
                ]
                varn = notifications.get(msg['message']['chat']['username'], dict()).get('varn', list())
                for i, item in enumerate(varn):
                    ikb.append([{'text': 'Regel %s entfernen' % (i+1), 'callback_data': 'varn_remove_%s' % i}])
                markup = {'inline_keyboard': ikb}
                self.bot.answerCallbackQuery(query_id, "Bitte Aktion auswaehlen.")
                self.send_message(msg['message']['chat']['id'], "Bitte Aktion auswählen", reply_markup=json.dumps(markup))

            elif "_add_" in query_data:

                step = query_data.split('_')[2]

                if step == '1':
                    ikb = []

                    data = get_data('allouts', msg['from']['username'], self.config, None)
                    for item in data:
                        for gw, outs in item.items():
                            ikb.append([{'text': u"Ausgänge an %s:" % gw, 'callback_data': 'varn_add_0_%s' % gw}])
                            ikb.append([])
                            for out in outs:
                                ikb[-1].append({'text': out, 'callback_data': 'varn_add_2_a_%s_%s' % (gw, out)})

                    data = get_data('allsensors', msg['from']['username'], self.config, None)
                    for item in data:
                        for ctrl, sensors in item.items():
                            ikb.append([{'text': 'Sensoren an %s' % ctrl, 'callback_data': 'varn_add_0_%s' % ctrl}])
                            for sensor in sensors:
                                ikb.append([{'text': sensor[1], 'callback_data': 'varn_add_2_s_%s' % sensor[0]}])

                    markup = {'inline_keyboard': ikb}

                    self.bot.answerCallbackQuery(query_id, "Ausgang oder Sensor auswaehlen")
                    self.send_message(msg['message']['chat']['id'], 'Bitte Ausgang oder Sensor auswählen', reply_markup=json.dumps(markup))

                elif step == "2":

                    qd = query_data.split('_')

                    if qd[3] == 'a':
                        out = qd[4] + '_' + qd[5]
                        notifications.setdefault(msg['message']['chat']['username'], dict())
                        notifications[msg['message']['chat']['username']].setdefault('varn', list())
                        notifications[msg['message']['chat']['username']]['varn'].append({out: 0})
                        self.bot.answerCallbackQuery(query_id, "Benachrichtigungen aktualisiert.")
                        self.send_message(msg['message']['chat']['id'], "Benachrichtigungen aktualisiert.")

                    elif qd[3] == 's':
                        sensor = qd[4]
                        data = get_data('allsensors', msg['from']['username'], self.config, None)
                        for item in data:
                            for ctrl, sensors in item.items():
                                for _sensor in sensors:
                                    if sensor == _sensor[0]:
                                        name = _sensor[1]
                                        break
                        self.awaiting_comparison_value_for_sensor = (sensor, name)
                        self.bot.answerCallbackQuery(query_id, "Vergleichswert angeben")
                        self.send_message(msg['message']['chat']['id'], 'Bitte Vergleichswert angeben')

            elif "_remove_" in query_data:
                idx = int(query_data.rsplit('_', 1)[1])
                varname = notifications.get(msg['message']['chat']['username'], dict()).get('varn', list())[idx].keys()[0]
                del notifications.get(msg['message']['chat']['username'], dict()).get('varn', list())[idx]
                if varname in notifications.get(msg['message']['chat']['username'], dict()).get('varn_sent', dict()):
                    del notifications.get(msg['message']['chat']['username'], dict()).get('varn_sent', dict())[varname]
                self.bot.answerCallbackQuery(query_id, "Alarm entfernt.")
                self.send_message(msg['message']['chat']['id'], "Alarm entfernt.")
                requests.get("http://127.0.0.1/set/tgbot/signals/")

            if notifications.get(msg['message']['chat']['username'], dict()).get('varn'):
                self.config.setdefault('notifications', dict())
                self.config['notifications'].setdefault(msg['message']['chat']['username'], dict())
                self.config['notifications'][msg['message']['chat']['username']]['varn'] = copy.deepcopy(notifications[msg['message']['chat']['username']]['varn'])
                json.dump(self.config, open("telegrambot/tgbotconfig.json", "wb"))

        elif query_data.startswith("toggle_"):  # /alarm
            query = query_data.split('_')[1]
            username = msg['message']['chat']['username']
            notifications.setdefault(username, dict())
            notifications[username][query] = not notifications[username].get(query, False)

            if notifications[username][query]:
                scheduler.add_job(
                    send_alarm,
                    'interval',
                    seconds=900,
                    id='%s_%s' % (username, query),
                    kwargs={'bot': self.bot,
                            'chat_id': msg['message']['chat']['id'],
                            'username': username,
                            'alarm': query}
                )
                self.config.setdefault('notifications', dict())
                self.config['notifications'].setdefault(username, dict())
                self.config['notifications'][username][query] = True
                json.dump(self.config, open("telegrambot/tgbotconfig.json", "wb"))
            else:
                scheduler.remove_job("%s_%s" % (username, query))
                self.config['notifications'][username][query] = False
                json.dump(self.config, open("telegrambot/tgbotconfig.json", "wb"))

            self.bot.answerCallbackQuery(query_id, "Benachrichtigungen aktualisiert.")
            self.send_message(msg['message']['chat']['id'], "Benachrichtigungen aktualisiert.")

        elif query_data.startswith("tempdiag_"):
            query = query_data.split('_')[1]
            data = get_data("roomlogs", msg['from']['username'], self.config, query)

            fig, ax = plt.subplots(figsize=(14, 6))

            xlim_min = xlim_max = None  # was ist, wenn das None bleibt bzw wenn keine Daten vorhanden sind?
            for d in data:
                if d[0] == "lf":
                    continue
                xdata = [datetime.utcfromtimestamp(_d[0]/1000) for _d in d[1]]
                ydata = [_d[1] for _d in d[1]]
                if len(xdata):
                    if xlim_min is None:
                        xlim_min = xdata[0]
                        xlim_max = xdata[-1]
                    else:
                        xlim_min = min(xlim_min, xdata[0])
                        xlim_max = max(xlim_max, xdata[-1])
                ax.plot(xdata, ydata, label=d[0])

            hours = mdates.HourLocator(interval=6)
            ax.xaxis.set_major_locator(hours)
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M\n%d.%m.'))

            fig.autofmt_xdate()
            if xlim_min is not None:
                plt.xlim([xlim_min, xlim_max])
            fig.tight_layout()
            fig.subplots_adjust(bottom=0.2)
            plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.12),
                       fancybox=False, shadow=False, ncol=5)
            # plt.title("%s %s")  # todo
            plt.savefig('testplot.png')

            self.send_photo(msg['message']['chat']['id'], "testplot.png")
            self.bot.answerCallbackQuery(query_id, u"Diagramm erstellt.")

        else:  # /soll
            if self.state == ContromeBot.SollState.AWAITINGSOLLRAUM:
                self.state = ContromeBot.SollState.AWAITINGSOLLTEMP
                self.sollraumauswahl = query_data
                self.bot.answerCallbackQuery(query_id, "Bitte Temperatur eingeben!")
                self.send_message(msg['message']['chat']['id'], "Bitte Temperatur eingeben!")
            else:
                self.bot.answerCallbackQuery(query_id, "Unbekannter Raum")

    def _is_allowed_user(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)
        user_ids = [v['tg_id'] for v in self.config.get('allowed_users', dict()).values()]
        if chat_id not in user_ids:
            if msg.get('from', dict()).get('username') in self.config.get('allowed_users', dict()).keys():
                self.config['allowed_users'][msg['from']['username']]['tg_id'] = chat_id
                json.dump(self.config, open("telegrambot/tgbotconfig.json", "wb"))
                return True
            else:
                return False
        else:
            return True


def main():

    while True:
        try:
            with open('telegrambot/tgbotconfig.json', 'r') as config:
                config = json.load(config)
                if config.get('API_KEY') and not len(config.get('API_KEY')):
                    time.sleep(10)
                    continue
                break
        except IOError as ioe:
            time.sleep(10)

    global haus_id
    while True:
        try:
            # r = local("sqlite3 /home/pi/rpi/db.sqlite3 'select id from heizmanager_haus;'", capture=True)
            r = local("sqlite3 db.sqlite3 'select id from heizmanager_haus;'", capture=True)
            if len(r):
                haus_id = r
                break
        except:
            continue

    bot = telepot.DelegatorBot(config['API_KEY'], [
        include_callback_query_chat_id(
            pave_event_space())(per_chat_id(types='private'),
                                create_open,
                                ContromeBot,
                                timeout=3600)
    ])

    scheduler.start()
    global notifications
    for username, alarms in config.get('notifications', dict()).items():
        for alarm, active in alarms.items():
            if alarm == 'varn':
                notifications.setdefault(username, dict())
                notifications[username][alarm] = active
            elif alarm == 'varn_sent':
                continue
            elif active:
                scheduler.add_job(
                    send_alarm,
                    'interval',
                    seconds=900,
                    id='%s_%s' % (username, alarm),
                    kwargs={'bot': bot,
                            'chat_id': config['allowed_users'][username]['tg_id'],
                            'username': username,
                            'alarm': alarm}
                )
                notifications.setdefault(username, dict())
                notifications[username][alarm] = True

    try:
        bot.message_loop(run_forever="bot running...")
    except KeyboardInterrupt:
        pass
    except Exception:
        # Error Handling
        logging.exception("fehler")
        try:
            from datetime import datetime
            import traceback
            typ, value, trace = sys.exc_info()
            branch = local("git rev-parse --abbrev-ref HEAD", capture=True)
            res = local("git log -1 --date=raw", capture=True)
            luiso = [r[8:-6] for r in res.split('\n') if r.startswith("Date: ")][0]
            lu = datetime.utcfromtimestamp(long(luiso)).strftime('%d.%m.%Y %H:%M:%S')
            ret = {
                'exc_type': typ.__name__,
                'exc_msg': typ.__doc__,
                'exc_value': str(value).translate(None, "'").translate(None, '"'),
                'exc_trace': traceback.extract_tb(trace),
                'branch': branch,
                'rev': lu
            }
            enc = local("""python -c "from rpi.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % {"error": ret}, capture=True)
            err = enc
            with open('/sys/class/net/eth0/address') as f:
                mac = f.read()
                macaddr = mac.strip().replace(':', '-').lower()
            requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % err)
        except:
            pass

    scheduler.shutdown()


if __name__ == "__main__":
    if len(sys.argv) == 3:
        send_motion_alarm(sys.argv[1], sys.argv[2])
    elif len(sys.argv) == 4:
        send_var_alarm(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        main()
            
# - offset setzen
