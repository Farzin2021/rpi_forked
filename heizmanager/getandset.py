# -*- coding: utf-8 -*-

import cache_helper as ch
from django.http import HttpResponse
from models import Raum, Sensor, Gateway, GatewayAusgang, HausProfil, Luftfeuchtigkeitssensor, AbstractSensor
import logging
from datetime import datetime, timedelta
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from models import sig_new_temp_value, sig_new_output_value
from heizmanager.mobile.m_temp import LocalPage, get_module_offsets
import pytz
from aussensensoren import aussensensoren
from benutzerverwaltung.decorators import is_admin
import heizmanager.network_helper as nh
import time


logger = logging.getLogger("logmonitor")
ENCRYPTED_GATEWAY_VERSIONS = ["4.00", "4.10", "4.20", "4.21", "4.22", "4.23", "5.00", "5.01", "5.02", "5.03", "6.00"]


def tset(request, sensorssn, ist, run_defer=True, linie=0, gw_id=None):

    try:
        if sensorssn.startswith('26_'):
            sensor = Luftfeuchtigkeitssensor.objects.get(name=sensorssn.lower())
        elif sensorssn in {"pt_10_00", "ui_10", "ui_20"}:
            if request and request.path.startswith('/set/'):
                mac = request.path.split('/')[2].lower()
                try:
                    s = Sensor.objects.get(name='%s#%s' % (mac, sensorssn.replace('_', '')))
                except Sensor.DoesNotExist:
                    try:
                        gw = Gateway.objects.get(name=mac)
                    except Gateway.DoesNotExist:
                        return HttpResponse()
                    s = Sensor(name='%s#%s' % (mac, sensorssn.replace('_', '')), gateway=gw, haus_id=gw.haus_id, description="", linie=-1, raum=None)
                    s.save()
                else:
                    if not s.gateway_id:
                        try:
                            gw = Gateway.objects.get(name=mac)
                        except Gateway.DoesNotExist:
                            return HttpResponse()
                        s.gateway_id = gw.id
                        s.save()
                sensor_offset = 0.0 if s.offset is None else s.offset
                ch.set_new_temp('%s#%s' % (mac, sensorssn.replace('_', '')), ist, s.haus_id, sensor_offset=sensor_offset)
                profil = HausProfil.objects.get(haus_id=long(s.haus_id))
                modules = profil.get_modules()
                now = datetime.now()
                sig_new_temp_value.send(sender="tset", name=s.get_identifier(), value=(float(ist)+sensor_offset), modules=modules, timestamp=now)
                
                return HttpResponse()
        else:
            sensor = Sensor.objects.get(name=sensorssn.lower())
        if sensor.gateway_id != gw_id and gw_id is not None:
            sensor.gateway_id = gw_id
            sensor.save()
    except Sensor.DoesNotExist:
        if request and request.path.startswith('/set/'):
            mac = request.path.split('/')[2]
            try:
                gw = Gateway.objects.get(name=mac)
            except Gateway.DoesNotExist:
                return HttpResponse()
            sensoren = Sensor.objects.filter(haus_id=gw.haus_id)
            for sensor in sensoren:
                if sensor.name.count('_00') == 4:
                    if sensorssn.startswith(sensor.name.replace('_00', '').lower().strip()):
                        if sensor.raum and sensor.raum.regelung.regelung == "ruecklaufregelung":
                            params = sensor.raum.regelung.get_parameters()
                            if sensor.name in params['rlsensorssn']:
                                outs = params['rlsensorssn'][sensor.name]
                                del params['rlsensorssn'][sensor.name]
                                params['rlsensorssn'][sensorssn] = outs
                                sensor.raum.regelung.set_parameters(params)
                        sensor.name = sensorssn
                        sensor.save()
                        break
            else:
                return HttpResponse()

        else:
            return HttpResponse()
    except Luftfeuchtigkeitssensor.DoesNotExist:
        return HttpResponse()
    except Sensor.MultipleObjectsReturned:
        _all = Sensor.objects.filter(name=sensorssn.lower())
        for _s in _all[1:]:
            _s.delete()
        sensor = _all[0]
    if sensor.linie != linie:
        sensor.linie = linie
        sensor.save()
    hausid = sensor.haus_id
    sensor_offset = 0.0 if sensor.offset is None else sensor.offset

    filtertemps = (".0", ".00", ".06", ".12", ".19", ".25", ".31", ".38", ".44", ".5", ".50", ".56", ".62", ".69", ".75", ".81", ".88", ".94", ".0625", ".125", ".1875", ".3125", ".375", ".4375", ".5625", ".625", ".6875", ".8125", ".875", ".9375")
    if not isinstance(sensor, Luftfeuchtigkeitssensor) and not str(ist).endswith(filtertemps):
        last = ch.get_last_temp(sensorssn)
        if last:
            try:
                ch.set_new_temp(sensorssn, last[0], hausid, sensor_offset=0.0)
            except (TypeError, KeyError):
                pass
        return HttpResponse()

    i = float(ist)
    if i < 0:
        try:
            if sensor.gateway.name.startswith(("b8-", "dc-")): 
                pass
            else:
                i = float(int((i+2) if i % 1.0 != 0.0 else (i+1))) - i % 1.0
        except AttributeError:  # gateway == None
            pass
        ist = str(i)

    try:
        i = float(ist)
        if i > 4000:
            ist = str(i - 4095.0)
        i = float(ist)
        if i > 100:
            if sensorssn.lower() in aussensensoren:
                ist = str(i - 256.0)
            elif 100 < i < 125 and ((sensor.gateway and sensor.gateway.name.startswith(("b8-27-eb", "dc-a6-32", "e4-5f-01"))) or sensor.gateway is None):
                pass
            else:
                ist = i/8.0
    except ValueError:
        pass

    ch.set_new_temp(sensorssn, ist, hausid, sensor_offset=sensor_offset)
    profil = HausProfil.objects.get(haus_id=long(hausid))

    gwdelay = profil.get_gateway_delay()
    if gwdelay < 90:
        _invalidate_cache(True, sensorssn)
        # sensortestactive = profil.get_sensortest()
        # if sensortestactive:
        #     st = SensorTest.objects.get_or_create(sensorssn=sensorssn.lower())[0]
        #     st.haus_id = hausid
        #     st.add_value(ist)
        #     st.save()

    modules = profil.get_modules()
    now = datetime.now()
    sig_new_temp_value.send(sender="tset", name=sensor.get_identifier(), value=(float(ist)+sensor_offset), modules=modules, timestamp=now)

    return HttpResponse()


@csrf_exempt
def tset_all(request, macaddr):

    try:
        gw = Gateway.objects.get(name=macaddr.lower())
    except Gateway.DoesNotExist:
        return HttpResponse()

    if 'data' in request.POST:  # verschluesseltes GW
        data = request.POST['data'].replace(' ', '+')
        try:
            _data = gw.crypt_keys.decrypt(data).encode('hex')
        except TypeError:
            logging.exception("typeerror")
            return HttpResponse()
        if gw.version < "4.21":
            _data = _data[:(len(_data)/22)*22]
        else:
            _data = _data[:(len(_data)/24)*24]

        if gw.version >= "4.21" and len(_data) % 12 != 0:
            data = dict()
        elif gw.version < "4.21" and len(_data) % 11 != 0:
            data = dict()

        else:
            import re
            data = dict()
            if gw.version >= "4.21":
                steps = 24
            else:
                steps = 22
            for d in range(len(_data))[::steps]:
                ssn = '_'.join([b for b in re.findall('..', _data[d:d+16])])
                if gw.version >= "4.21":
                    val = str(_data[d+16:d+18]).decode('hex') + str(int(_data[d+18:d+20], 16)) + '.' + str(int(_data[d+20:d+22], 16))
                    linie = int(_data[d+22:d+24], 16)
                else:
                    val = str(int(_data[d+16:d+18], 16)) + '.' + str(int(_data[d+18:d+20], 16))
                    linie = int(_data[d+20:d+22], 16)
                if ssn == "00_00_00_00_00_00_00_00":
                    break
                try:
                    data[ssn] = (float(val), linie)
                except ValueError:
                    continue

    elif macaddr.startswith(("b8-27-eb-", "dc-a6-32", "e4-5f-01")):
        import json
        _data = json.loads(request.body)
        data = {}
        for _d in _data:
            try:
                if _d.values()[0]['value'] is not None:
                    data[_d.keys()[0]] = [_d.values()[0]['value'], _d.values()[0]['linie']]
            except KeyError:
                pass

    else:  # unverschluesseltes GW
        data = dict((ssn, (request.POST[ssn], 0)) for ssn in request.POST)

    filtertemps = (".0", ".00", ".06", ".12", ".19", ".25", ".31", ".38", ".44", ".5", ".50", ".56", ".62", ".69", ".75", ".81", ".88", ".94", ".0625", ".125", ".1875", ".3125", ".375", ".4375", ".5625", ".625", ".6875", ".8125", ".875", ".9375")
    logger.warning("0|%s|%s" % (macaddr, ";".join(["%s,%s,%s" % (k, v[0], v[1]) for k, v in data.items() if str(v[0]).endswith(filtertemps) or k.startswith(('ui', 'pt', '26'))])))

    last = None
    for sensorssn in data:
        tset(request, sensorssn, data[sensorssn][0], run_defer=False, linie=data[sensorssn][1], gw_id=gw.id)
        if last is None:
            last = ch.get_last_temp(sensorssn)

    return HttpResponse("received")


def get(request, macaddr, ausgang):
    if request and "/m_raum/" not in request.META.get('HTTP_REFERER', ""):
        ch.set_gw_ping(macaddr)

    try:
        gw = Gateway.objects.get(name=macaddr)
        invert = gw.haus.profil.get().get_gateway_invertouts(macaddr.lower())
    except Gateway.DoesNotExist:
        return HttpResponse("<0>")
    gatewayausgang = get_gwout_by_no(macaddr, ausgang, gateway=gw)

    params = gw.get_parameters()
    max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))

    try:
        ver = gw.version
        if ver in ENCRYPTED_GATEWAY_VERSIONS and request and "/m_raum/" not in request.META.get('HTTP_REFERER', ''):
            return HttpResponse("<>")  # damit man get_all nicht umgehen kann!
    except AttributeError:
        pass  # altes GW

    try:
        if isinstance(gw, Gateway) and "6.00" > gw.version >= "5.00" and gw.version != "nix":
            ret = ch.get("%s_%s" % (gw.name, ausgang))
            if ret is None:
                ret = "<>"
        else:
            ret = gatewayausgang.get_regelung().do_ausgang(ausgang).content
            if ch.update_gateway(macaddr):
                ret.replace('>', '!>')

        reg = gatewayausgang.get_regelung() if gatewayausgang else None
        if reg and reg.regelung in {'ruecklaufregelung', 'zweipunktregelung'}:
            raum = reg.get_raum()
            if raum is None:
                return HttpResponse("<>")

            mode = raum.get_betriebsart()

            try:
                val = calc_opening(ret, mode, gw, ausgang, raum, max_open.get(int(ausgang), 99), invert)
                ret = "<%s>" % str(val)

            except ValueError:
                logging.exception("exc")
            except Exception:
                logging.exception("exc")

        return HttpResponse(ret)
    except Exception as e:  # gatewayausgang == None, regelung == None, ?
        if "'NoneType' object has no attribute 'get_regelung'" in str(e):
            logging.info(e)
        else:
            logging.error('error while responding to /get/')
            import traceback
            logging.error(traceback.format_exc())
        if ch.update_gateway(macaddr):
            return HttpResponse('<0!>')
        else:
            return HttpResponse('<0>')


def get_gwout_by_no(name, ausgangno, gateway=None):
    try:
        if gateway is None:
            gateway = Gateway.objects.get(name=name.lower())
        ausgaenge = GatewayAusgang.objects.filter(gateway=gateway)
        for ausgang in ausgaenge:
            if str(ausgangno).strip() in ausgang.ausgang.split(', '):
                return ausgang
        else:
            return None
    except (Gateway.DoesNotExist, UnicodeEncodeError):
        return None


def calc_opening(sget, betriebsart, version, outno, raum, max_open=99, invert=False, nolog=False):

    def _invert(_val):
        if "6.00" > version >= "5.00" and int(outno) < 15:
            # analog
            _val = max_open - _val
        elif "6.00" > version >= "5.00" and int(outno) == 15:
            # digital
            if _val == 0:
                _val = 1
            else:
                _val = 0
        elif version == "nix" and int(outno) in {13, 14}:
            # analog
            _val = 99 - val
        elif version == "nix" and int(outno) < 13:
            # digital
            if _val == 0:
                _val = 1
            else:
                _val = 0
        else:  # fbhgw, minigw
            # digital
            if _val == 0:
                _val = 1
            else:
                _val = 0

        return _val

    sget = sget.translate(None, "<>!")
    val = int(float(sget))  # todo das kann einen ValueError werfen, wenn sget == "<>". den muss der aufrufer fangen.
    try:
        max_open = min(int(max_open), 99)
    except:
        max_open = 99

    if betriebsart == "0":  # aus
        val = 0

    elif betriebsart == "1":  # kuehlen
        val = _invert(val)

    elif betriebsart == "2":  # heizen
        # hier aktuell nix machen
        pass

    if invert:
        val = _invert(val)

    if (betriebsart in {"0", "1"} or invert) and raum and not nolog:
        logger.warning("%s|%s" % (raum.id, u"Ausgang %s durch Invertierung/Betriebsarten auf %s gesetzt." % (outno, val)))

    return val


def get_all(request, macaddr):
    ch.set_gw_ping(macaddr)
    
    ret = ch.get("%s_outs" % macaddr)
    if not ret:

        ret = '<'
        now = datetime.now()

        try:
            gw = Gateway.objects.get(name=macaddr)
            invert = gw.haus.profil.get().get_gateway_invertouts(macaddr.lower())
        except Gateway.DoesNotExist:
            # return HttpResponse("<0;0;0;0;0;0;0;0;0;0;0;0;0;0;0>")
            return HttpResponse()

        if gw.is_heizraumgw():
            return HttpResponse()

        params = gw.get_parameters()
        max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))

        max_outs = 3 if gw.version >= "6.00" and gw.version != "nix" else 16

        if params.get('ahaparams', {}).get('is_active'):
            ch.set("%s_outs" % macaddr, None)
            outs = params['ahaparams']['outs']

            try:
                start = datetime.strptime(params['ahaparams'].get('start'), "%Y-%m-%d %H:%M")
                berlin = pytz.timezone("Europe/Berlin")
                start = berlin.localize(start)
                if (datetime.now(berlin) - start).total_seconds() < 1800:
                    for i in range(1, max_outs):
                        max_open[i] = 0
            except:
                pass

            for i in range(1, max_outs):
                if i in outs:
                    val = min(99, int(max_open[i]))
                    ret += str(val)
                    ret += ';'
                    gatewayausgang = get_gwout_by_no(macaddr, i, gateway=gw)
                    ch.set("lastout_%s_%s" % (gatewayausgang.regelung_id, i), (str(val), None))
                    ch.set('%s_%s' % (macaddr, i), "<%s>" % val)
                else:
                    if i == 15:
                        try:
                            gatewayausgang = get_gwout_by_no(macaddr, i, gateway=gw)
                            sget = gatewayausgang.get_regelung().do_ausgang(i).content
                            sget = int(sget.translate(None, "<>!"))
                        except:
                            sget = 0
                        ret += '%s;' % str(sget)
                        ch.set('%s_%s' % (macaddr, i), "<%s>" % sget)
                    else:
                        ret += '0;'
                        ch.set('%s_%s' % (macaddr, i), "<0>")

            ret = ret[:-1]
            ret += ">"
            logging.warning("aha for %s returning %s" % (macaddr, ret))
            return HttpResponse(ret)

        for i in range(1, max_outs):

            gatewayausgang = None
            try:
                sget = ch.get('%s_%s' % (macaddr, i))
                gatewayausgang = get_gwout_by_no(macaddr, i, gateway=gw)
                if not sget:

                    if "6.00" > gw.version >= "5.00" and gw.version != "nix":
                        if gatewayausgang.get_regelung().regelung in ['pumpenlogik', 'nullregelung', 'gatewaymonitor', 'fps', 'vorlauftemperaturregelung', 'differenzregelung']:
                            sget = gatewayausgang.get_regelung().do_ausgang(i).content
                        else:
                            sget = "<>"
                        sget = sget.replace("!", "")
                        if i == 15:
                            if sget != "<>" and sget != "<0>":
                                sget = "<1>"
                        elif gatewayausgang.get_regelung().regelung in ["fps", "vorlauftemperaturregelung"]:
                            # nix machen, damit der wert beibehalten wird
                            sget = int(float(sget.translate(None, "<>!")))
                            sget = "<%s>" % max(0, min(99, sget))
                        else:
                            sgettrans = {"<0>": "<0>", "<1>": "<99>", "<>": "<>"}
                            sget = sgettrans.get(sget, "<0>")
                    else:
                        sget = gatewayausgang.get_regelung().do_ausgang(i).content

                    if gatewayausgang.get_regelung().regelung == "pumpenlogik":
                        ch.set('%s_%s' % (macaddr, i), sget, time=300)  # fuer den moment mal nur pl cachen

            except Exception as e:
                if "'NoneType' object has no attribute 'get_regelung'" not in str(e):
                    import sys
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    err = "exception %s / %s in get_all: %s" % (exc_type, exc_obj, exc_tb.tb_lineno)
                    logging.warning(err)
                sget = "<0>"

            reg = gatewayausgang.get_regelung() if gatewayausgang else None
            if reg and reg.regelung in {'ruecklaufregelung', 'zweipunktregelung'}:
                raum = reg.get_raum()
                if raum is None:
                    ret += ";"
                    continue

                mode = raum.get_betriebsart()

                try:
                    val = calc_opening(sget, mode, gw.version, i, raum, max_open.get(i, 99), invert)
                    ret += str(val)

                    ziel = ch.get('ziel_%s_%s' % (macaddr, i)) or 0
                    if not ziel:
                        ziel = raum.solltemp + get_module_offsets(raum, only_clear_offsets=True)
                        ch.set('ziel_%s_%s' % (macaddr, i), ziel, time=300)
                    sig_new_output_value.send(sender='get_all', name='%s_%s' % (macaddr, i), value=val, ziel=ziel, parameters={'empty': True}, timestamp=now, modules=None)

                    ch.set("lastout_%s_%s" % (reg.id, i), (str(val), now, False, ziel))

                except ValueError:
                    logging.exception("exc")
                    ret += ""
                except Exception:
                    logging.exception("exc")

            else:
                try:
                    ret += str(calc_opening(sget, "2", gw.version, i, None, max_open.get(i, 99), invert))
                except ValueError:
                    ret += ""

            ret += ';'
        ret = ret[:-1]
        if ch.update_gateway(macaddr):
            ret += '!>'
        else:
            ret += '>'

        logging.warning("returning %s" % ret)
        if not ("6.00" > gw.version >= "5.00" and gw.version != "nix") and request.start_time + 9 < time.time():
            # 010v nicht cachen, weil per celery task berechnet
            # auch nicht cachen, wenn die antwort hier rechtzeitig zurueckgegeben werden kann
            ch.set("%s_outs" % macaddr, ret, time=300)

    else:
        try:
            logging.warning("returning cached ret: %s" % ret)
        except TypeError:
            ch.set("%s_outs" % macaddr, None)
            time.sleep(30)  # timeout beim gateway erzwingen
    
    return HttpResponse(ret)


def get_backup(request, macaddr, version=None):

    ret = ''
    inc_outs = set()
    printable = set("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

    try:
        gw = Gateway.objects.get(name=macaddr.lower())
        # invert = gw.haus.profil.get().get_gateway_invertouts(macaddr.lower())
    except Gateway.DoesNotExist:
        return HttpResponse()

    max_outs = 3 if gw.version >= "6.00" and gw.version != "nix" else 16

    for i in range(1, max_outs):
        ausgang = get_gwout_by_no(macaddr.lower(), str(i), gateway=gw)
        if ausgang is None:
            continue
        if ausgang.id in inc_outs:
            continue
        else:
            inc_outs.add(ausgang.id)

        _invalidate_get_cache(macaddr, str(i))

        reg = ausgang.get_regelung()
        if reg.regelung == "pumpenlogik":
            params = reg.get_parameters()
            sensoren = Sensor.objects.filter(gateway=ausgang.gateway)
            ssn = "00_00_00_00_00_00_00_00"
            for sensor in sensoren:
                last = sensor.get_wert()
                if last:
                    ssn = sensor.name
                    break
            else:
                if len(sensoren):
                    ssn = sensoren[0].name

            name = filter(lambda x: x in printable, params["name"])

            if params.get('default', 1) == 1:
                soll = -100
            else:
                soll = 255
            stellantrieb = ausgang.stellantrieb
            # if invert:
            #     stellantrieb = 'NC' if stellantrieb.count('O') else 'NO'
            ret += '<%s;%s;%s;%s;%s;%s;%s>' % (reg.id, name, "Pumpenlogik", soll,
                                               ','.join(ausgang.ausgang.split(', ')), ssn, stellantrieb)

        else:
            try:
                sensor = reg.get_raum().get_mainsensor()
            except (Raum.DoesNotExist, Sensor.DoesNotExist, AttributeError):
                logging.exception("settingserror")
                continue
            else:
                if sensor is None or sensor not in ausgang.gateway.get_sensoren():
                    # reine rlr
                    if reg.regelung == 'ruecklaufregelung':
                        regparams = reg.get_parameters()
                        for sensorid, ausgaenge in regparams.get('rlsensorssn', dict()).items():
                            if i in ausgaenge:
                                sensor = AbstractSensor.get_sensor(sensorid)
                                if sensor is None:
                                    continue
                                else:
                                    break
                        else:
                            continue
                    else:
                        continue
                else:
                    # ms vorhanden und am gw
                    pass

                raumid = ausgang.get_regelung().get_raum().id
                raumname = filter(lambda x: x in printable, ausgang.get_regelung().get_raum().name)
                etagenname = filter(lambda x: x in printable, ausgang.get_regelung().get_raum().etage.name)
                soll = ausgang.get_regelung().get_raum().solltemp
                aausgang = ausgang.ausgang
                sensorssn = sensor.get_name_for_gw()
                stellantrieb = ausgang.stellantrieb
                # if invert:
                #     stellantrieb = 'NC' if stellantrieb.count('O') else 'NO'
                ret += '<%s;%s;%s;%s;%s;%s;%s>' % (raumid, raumname, etagenname, soll, ','.join(aausgang.split(', ')),
                                                   sensorssn, stellantrieb)
    if len(ret):
        ret += '>'
    ch.del_gateway_update(macaddr.lower())
    ch.set_gw_ping(macaddr)

    try:
        gateway = Gateway.objects.get(name=macaddr)
        if gateway.crypt_keys and gateway.version != "nix" and gateway.version in ENCRYPTED_GATEWAY_VERSIONS:
            logging.warning("enc")
            logging.warning(ret)
            ret = gateway.crypt_keys.encrypt(ret)
    except Gateway.DoesNotExist:
        pass

    return HttpResponse(ret)


def get_setting(request, macaddr, version):
    # gibt einem Gateway seine aktuellen Settings zurueck

    try:
        gateway = Gateway.objects.get(name=macaddr.lower())
    except Gateway.DoesNotExist:
        return HttpResponse()

    try:  # uebertrag auf .version, .ck, .remote_id und .rpi fuer alte gw
        if gateway.version == "nix":
            gateway.version = version
            gateway.save()
        elif version > gateway.version:
            gateway.version = version
            gateway.save()
    except AttributeError:
        gateway.version = version
        gateway.rpi = None
        gateway.crypt_keys = None
        gateway.remote_id = 0
        gateway.save()

    profil = HausProfil.objects.get(haus_id=gateway.haus_id)
    if profil is None:
        return HttpResponse()

    gwdelay = profil.get_gateway_delay()
    if gwdelay < 90:
        install_mode_set = profil.get_module_parameters().get('install_mode_set', (datetime.now() - timedelta(seconds=1801)).isoformat())
        if install_mode_set < (datetime.now() - timedelta(seconds=1800)).isoformat():
            gwdelay = 180
            profil.set_gateway_delay(gwdelay)

    if profil.get_safe_mode():
        safe_mode = 1
    else:
        safe_mode = 0
    autarker_betrieb = 1

    ch.set_gw_ping(macaddr)

    if version >= "4.10":  # verschluesselt, autark

        localip = nh.get_ip()
        ret = '<%s;%s;%s;%s>' % (gwdelay*1000, localip, safe_mode, autarker_betrieb)

    else:
        ret = '<>'

    logging.warning("returning: %s" % ret)

    if gateway and gateway.crypt_keys and (gateway.version in ENCRYPTED_GATEWAY_VERSIONS):  # version, damit nicht alte GW zufaellig CK bekommen und dann nichts mehr verstehen
        ret = gateway.crypt_keys.encrypt(ret)
        logging.warning("enc")

    return HttpResponse(ret)


@csrf_exempt
def get_nonce(request, macaddr):

    import base64
    try:
        from Crypto import Random
        nonce = Random.new().read(16)
    except ImportError:
        from os import urandom
        nonce = urandom(16)

    if request.method == "GET":  # gibt nonce zurueck
        nonce = base64.b64encode(nonce)
        ch.set_nonce(macaddr, nonce)

        logging.warning("returning b64nonce %s" % nonce)

        return HttpResponse('~' + nonce)

    elif request.method == "POST":  # verifiziert noncen, safe_mode fuer gateways
        n = request.POST.get("nonce", None)
        gw = Gateway.objects.get(name=macaddr)
        if n and ch.verify_nonce(macaddr, n):
            ret = "<1;%s>" % base64.b64encode(nonce)  # nehmen wir die ueberzaehlige nonce als padding her
        else:
            ret = "<0;%s>" % base64.b64encode(nonce)  # nehmen wir die ueberzaehlige nonce als padding her
        ret = gw.crypt_keys.encrypt(ret)

        return HttpResponse('~' + ret)


def set_gateways_soll(request, raumid, soll):
    try:
        raum = Raum.objects.get(pk=long(raumid))
        raum.solltemp = float(soll)
        logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur über URL auf %.2f gesetzt" % raum.solltemp))
        raum.save()
    except Exception:
        return HttpResponse('error, wrong input')
    return HttpResponse('done')


def _invalidate_get_cache(macaddr, ausgaenge):  # nach cache_helper umziehen?
    from django.http import HttpRequest
    from django.core.cache import cache
    from django.conf import settings
    from django.utils.cache import _generate_cache_header_key

    for ausgang in ausgaenge.split(','):
        request = HttpRequest()
        request.method = "GET"
        request.path = '/get/%s/%s/' % (macaddr, ausgang.strip())
        key_prefix = settings.CACHE_MIDDLEWARE_KEY_PREFIX
        try:
            key = _generate_cache_header_key(key_prefix, request)
            if cache.has_key(key):
                cache.delete(key)
        except KeyError:
            logging.error("keyerror invalidating cache")

    _invalidate_getall_cache(macaddr)


def _invalidate_cache(install_mode, sensorssn):
    # Invalidate view cache for install mode. Helper for set().
    if install_mode:

        try:
            sensor = Sensor.objects.get(name=str(sensorssn).lower())
        except Sensor.DoesNotExist:
            return

        try:
            ausgang = sensor.raum.regelung.gatewayausgang
            aausgang = ausgang
        except (Raum.DoesNotExist, GatewayAusgang.DoesNotExist, AttributeError):
            return

        if aausgang:
            _invalidate_get_cache(aausgang.gateway.name, aausgang.ausgang)


def _invalidate_getall_cache(macaddr):
    # Cache Invalidation fuer den /get/MAC/all/ page cache.
    # Wird automatisch von _invalidate_cache aufgerufen.
    # Sonst keine Aufrufe noetig?
    from django.http import HttpRequest
    from django.core.cache import cache
    from django.conf import settings
    from django.utils.cache import _generate_cache_header_key

    request = HttpRequest()
    request.method = "GET"
    request.path = '/get/%s/all/' % macaddr
    key_prefix = settings.CACHE_MIDDLEWARE_KEY_PREFIX
    try:
        key = _generate_cache_header_key(key_prefix, request)
        if cache.has_key(key):
            cache.delete(key)
    except KeyError:
        logging.error("keyerror invalidating cache")


@is_admin
def get_db(request):

    import os
    db = open(os.path.dirname(__file__) + "/../db.sqlite3")
    response = HttpResponse(db, content_type='application/text')
    response['Content-Disposition'] = 'attachment; filename=dbdl.zip'
    return response


@is_admin
@csrf_exempt
def set_db(request):
    if request.method == "GET":
        return HttpResponse("<html><body><form action='/dbul/' method='post' enctype='multipart/form-data'><input type='file' name='db'/><input type='submit' value='Hochladen'/></form></body></html>")

    elif request.method == "POST":
        import shutil, os
        from fabric.api import local
        db = request.FILES['db']
        dir = os.path.dirname(__file__) + "/../"
        with open(dir+"db1.sqlite3", 'wb') as dest:
            shutil.copyfileobj(db, dest)

        try:
            local("""sqlite3 %sdb1.sqlite3 ".schema auth_user" """ % dir)
        except:
            local("rm %sdb1.sqlite3" % dir)
            return HttpResponse("Fehler: Datenbank konnte nicht gelesen werden")

        local("cp %sdb.sqlite3 %sdb.backup" % (dir, dir))
        local("mv %sdb1.sqlite3 %sdb.sqlite3" % (dir, dir))

        mig1 = local("sqlite3 %sdb.sqlite3 'select name from django_migrations'" % dir, capture=True)
        mig2 = local("sqlite3 %sdb.backup 'select name from django_migrations'" % dir, capture=True)
        if mig1 != mig2:
            try:
                local("python /home/pi/rpi/manage.py migrate heizmanager")
                try:
                    result = local("sqlite3 /home/pi/rpi/db.sqlite3 'select * from benutzerverwaltung_objectpermission'")
                    local("python /home/pi/rpi/manage.py migrate --fake benutzerverwaltung")
                except:
                    local("python /home/pi/rpi/manage.py makemigrations benutzerverwaltung")
                    local("python /home/pi/rpi/manage.py migrate benutzerverwaltung")
                    local("""sqlite3 /home/pi/rpi/db.sqlite3 "update users_userprofile set role = 'A'" """)

                try:
                    result = local("sqlite3 /home/pi/rpi/db.sqlite3 'select * from geolocation_geodevice'")
                    local("python /home/pi/rpi/manage.py migrate --fake geolocation")
                except:
                    local("python /home/pi/rpi/manage.py makemigrations geolocation")
                    local("python /home/pi/rpi/manage.py migrate geolocation")

                result = local("sqlite3 /home/pi/rpi/db.sqlite3 '.schema users_userprofile'", capture=True)
                if 'parameters' not in result:
                    try:
                        local("ls /home/pi/rpi/users/migrations/0002*py")
                        local("""sqlite3 /home/pi/rpi/db.sqlite3 'delete from django_migrations where app="users"' """)
                        local("python /home/pi/rpi/manage.py migrate --fake users 0001")
                        local("python /home/pi/rpi/manage.py migrate users")
                    except:
                        # todo jetzt haben wir ein problem. eigentlich muesste man jetzt zu master wechseln.
                        local("rm -rf /home/pi/rpi/users/migrations/")
                        local("python /home/pi/rpi/manage.py makemigrations users")
                        local("python /home/pi/rpi/manage.py migrate users --fake-initial")
                        local("""sqlite3 /home/pi/rpi/db.sqlite3 "alter table users_userprofile add column 'parameters' text NULL" """)

                local("python /home/pi/rpi/manage.py migrate --fake raumgruppen")
                local("python /home/pi/rpi/manage.py migrate --fake users")
                local("python /home/pi/rpi/manage.py migrate --fake vsensoren")

                result = local("sqlite3 /home/pi/rpi/db.sqlite3 '.schema auth_user'", capture=True)
                if '"last_login" datetime NULL' not in result:
                    local("python /home/pi/rpi/manage.py migrate")
            except:
                local("cp %sdb.backup %sdb.sqlite3" % (dir, dir))
                return HttpResponse("Fehler: Datenbank inkompatibel")

        try:
            ret = local("""sqlite3 %sdb.sqlite3 "select name from heizmanager_rpi order by id limit 1" """ % dir, capture=True)
            mac = nh.get_mac()
            if mac not in ret:
                from rpi.verification_code import verification_code
                from rpi.aeskey import key
                try:
                    rpi_id, key_id = local("""sqlite3 '%sdb.sqlite3' 'select id, crypt_keys_id from heizmanager_rpi order by id limit 1'""" % dir, capture=True).split('|')
                    local("""sqlite3 '%sdb.sqlite3' 'update heizmanager_rpi set name="%s", verification_code="%s" where id=%s'""" % (dir, mac, verification_code, rpi_id))
                    local("""sqlite3 '%sdb.sqlite3' 'update heizmanager_cryptkeys set aeskey="%s" where id=%s'""" % (dir, key, key_id))
                except:
                    local("cp %sdb.backup %sdb.sqlite3" % (dir, dir))
                    return HttpResponse("Fehler: Datenbank konnte nicht angepasst werden")

                try:
                    local("""sqlite3 '%sdb.sqlite3' 'update heizmanager_rfcontroller set name="%s" where name="%s"'""" % (dir, mac, ret.strip()))
                except:
                    pass

            local("echo 'flush_all' | nc -q 1 localhost 11211")
            local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi")
            return HttpResponse("Datenbank erfolgreich hochgeladen")
        except:
            local("cp %sdb.backup %sdb.sqlite3" % (dir, dir))
            return HttpResponse("Fehler: Datenbank konnte nicht gelesen werden (2)")


@is_admin
def get_log(request, day=None):

    try:
        if day is None:
            log = open("/var/log/uwsgi/uwsgi.log")
        else:
            try:
                if day == '1':
                    log = open("/var/log/uwsgi/uwsgi.log.%s" % day)
                else:
                    log = open("/var/log/uwsgi/uwsgi.log.%s.gz" % day)
            except IOError:
                return HttpResponse("file not found: /var/log/uwsgi/uwsgi.log.%s.gz" % day)
        response = HttpResponse(log, content_type='application/text')
        response['Content-Disposition'] = 'attachment; filename=log.txt'
        return response
    except IOError:
        return HttpResponse("no log found")
