# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import caching.base
import heizmanager.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CryptKeys',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('aeskey', models.CharField(max_length=32, null=True, blank=True)),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Etage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('position', models.IntegerField()),
            ],
            options={
                'ordering': ['position'],
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Gateway',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('description', models.CharField(max_length=64)),
                ('version', models.CharField(default=b'nix', max_length=8)),
                # ('parameters', models.TextField(blank=True)),
                ('crypt_keys', models.ForeignKey(to='heizmanager.CryptKeys', null=True)),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='GatewayAusgang',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ausgang', models.CharField(default=b'', max_length=45, blank=True)),
                ('stellantrieb', models.CharField(max_length=2, choices=[(b'NO', b'normal offen'), (b'NC', b'normal geschlossen')])),
                ('gateway', models.ForeignKey(related_name=b'ausgaenge', to='heizmanager.Gateway')),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Haus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=60)),
                ('besitzer', heizmanager.fields.ListField(verbose_name=models.ForeignKey(to=settings.AUTH_USER_MODEL))),
                ('eigentuemer', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='HausProfil',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('install_mode', models.BooleanField(default=False)),
                ('modules', models.TextField(blank=True)),
                ('module_parameters', models.TextField(blank=True)),
                ('haus', models.ForeignKey(related_name=b'profil', to='heizmanager.Haus')),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Raum',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=63)),
                ('solltemp', models.FloatField(null=True)),
                ('isttemp', models.FloatField(null=True)),
                ('position', models.IntegerField()),
                ('icon', models.CharField(max_length=30, blank=True)),
                ('modules', models.TextField(blank=True)),
                ('module_parameters', models.TextField(blank=True)),
                ('etage', models.ForeignKey(related_name=b'raeume', to='heizmanager.Etage')),
            ],
            options={
                'ordering': ['position'],
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Regelung',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('regelung', models.CharField(max_length=63)),
                ('parameter', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='RFAktor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('description', models.CharField(max_length=64)),
                ('mainsensor', models.BooleanField(default=False)),
                ('protocol', models.CharField(max_length=16, choices=[(b'zwave', b'ZWave')])),
                ('type', models.CharField(max_length=16, choices=[(b'hkt', 'Heizk\xf6rper Thermostat'), (b'ctrl', b'Controller'), (b'undef', b'undefined'), (b'sensor', b'Sensor'), (b'multisensor', b'Mehrfachsensor'), (b'relais', b'Relais'), (b'wp', b'Funksteckdose'), (b'repeater', b'Range Extender'), (b'wt', b'Wandthermostat')])),
                ('parameters', models.TextField()),
                ('hidden', models.BooleanField(default=False)),
                ('value', models.FloatField(null=True)),
                ('values', models.TextField()),
                ('battery', models.IntegerField(default=-1)),
                ('locked', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='RFAusgang',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('aktor', heizmanager.fields.ListField(verbose_name=models.ForeignKey(to='heizmanager.RFAktor'))),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='RFController',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('description', models.CharField(max_length=64)),
                ('protocol', models.CharField(max_length=16, choices=[(b'zwave', b'ZWave')])),
                ('type', models.CharField(max_length=16, choices=[(b'hkt', 'Heizk\xf6rper Thermostat'), (b'ctrl', b'Controller'), (b'undef', b'undefined'), (b'sensor', b'Sensor'), (b'multisensor', b'Mehrfachsensor'), (b'relais', b'Relais'), (b'wp', b'Funksteckdose'), (b'repeater', b'Range Extender'), (b'wt', b'Wandthermostat')])),
                ('parameters', models.TextField()),
                ('hidden', models.BooleanField(default=False)),
                ('haus', models.ForeignKey(related_name=b'rfcontroller_related', to='heizmanager.Haus', null=True)),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='RFSensor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('description', models.CharField(max_length=64)),
                ('mainsensor', models.BooleanField(default=False)),
                ('protocol', models.CharField(max_length=16, choices=[(b'zwave', b'ZWave')])),
                ('type', models.CharField(max_length=16, choices=[(b'hkt', 'Heizk\xf6rper Thermostat'), (b'ctrl', b'Controller'), (b'undef', b'undefined'), (b'sensor', b'Sensor'), (b'multisensor', b'Mehrfachsensor'), (b'relais', b'Relais'), (b'wp', b'Funksteckdose'), (b'repeater', b'Range Extender'), (b'wt', b'Wandthermostat')])),
                ('parameters', models.TextField()),
                ('hidden', models.BooleanField(default=False)),
                ('values', models.TextField()),
                ('battery', models.IntegerField(default=-1)),
                ('controller', models.ForeignKey(to='heizmanager.RFController')),
                ('haus', models.ForeignKey(related_name=b'rfsensor_related', to='heizmanager.Haus', null=True)),
                ('raum', models.ForeignKey(related_name=b'rfsensoren', to='heizmanager.Raum', null=True)),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='RPi',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('description', models.CharField(max_length=64)),
                ('verification_code', models.CharField(max_length=16)),
                ('upgrade_version', models.CharField(max_length=128, blank=True)),
                ('local_address', models.CharField(max_length=64)),
                ('global_address', models.CharField(max_length=64)),
                ('local_ip', models.GenericIPAddressField(null=True)),
                ('autark', models.BooleanField(default=False)),
                ('crypt_keys', models.ForeignKey(to='heizmanager.CryptKeys', null=True)),
                ('haus', models.ForeignKey(related_name=b'rpi_related', to='heizmanager.Haus', null=True)),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('description', models.CharField(max_length=64)),
                ('mainsensor', models.BooleanField(default=False)),
                ('wert', models.FloatField(null=True)),
                ('offset', models.FloatField(null=True, blank=True)),
                ('linie', models.IntegerField(default=0)),
                ('gateway', models.ForeignKey(related_name=b'sensoren', to='heizmanager.Gateway', null=True)),
                ('haus', models.ForeignKey(related_name=b'sensor_related', to='heizmanager.Haus', null=True)),
                ('raum', models.ForeignKey(related_name=b'sensoren', to='heizmanager.Raum', null=True)),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Wetter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.TextField()),
                ('timestamp', models.DateField(auto_now=True)),
                ('haus', models.ForeignKey(to='heizmanager.Haus')),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AddField(
            model_name='rfcontroller',
            name='rpi',
            field=models.ForeignKey(to='heizmanager.RPi', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rfausgang',
            name='controller',
            field=models.ForeignKey(related_name=b'rfausgang', to='heizmanager.RFController'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rfausgang',
            name='regelung',
            field=models.OneToOneField(related_name=b'rfausgang', null=True, to='heizmanager.Regelung'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rfaktor',
            name='controller',
            field=models.ForeignKey(to='heizmanager.RFController'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rfaktor',
            name='haus',
            field=models.ForeignKey(related_name=b'rfaktor_related', to='heizmanager.Haus', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rfaktor',
            name='raum',
            field=models.ForeignKey(related_name=b'rfaktoren', to='heizmanager.Raum', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='raum',
            name='regelung',
            field=models.OneToOneField(related_name=b'raum', null=True, to='heizmanager.Regelung'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='gatewayausgang',
            name='regelung',
            field=models.OneToOneField(related_name=b'gatewayausgang', null=True, to='heizmanager.Regelung'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='gateway',
            name='haus',
            field=models.ForeignKey(related_name=b'gateway_related', to='heizmanager.Haus', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='gateway',
            name='rpi',
            field=models.ForeignKey(to='heizmanager.RPi', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='etage',
            name='haus',
            field=models.ForeignKey(related_name=b'etagen', to='heizmanager.Haus'),
            preserve_default=True,
        ),
    ]
