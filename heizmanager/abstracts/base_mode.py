from abc import ABCMeta, abstractmethod
import logging
import importlib


class BaseMode:
    """
    this class is for implementing in the modules that are providing modes
        every class that wants to implement this class show follow a naming convention
        the name MUST be like this: Nameofthemodule(capital)+Mode like QuickuiMode,
    """

    __metaclass__ = ABCMeta

    @staticmethod
    def get_active_mode_raum(raum, max_priority=-1):
        haus = raum.etage.haus
        active_mod = None
        try:
            from quickui.views import QuickuiMode
            mode_quickui = QuickuiMode.get_mode_raum(raum)
            if mode_quickui is not None:
                return mode_quickui
        except Exception as e:
            logging.exception("exception importing / calling gmr")
            pass

        for mod in set(haus.get_modules()) & set(['temperaturszenen']):
            try:
                module = importlib.import_module(mod.strip() + '.views')
            except ImportError:
                continue

            try:
                modeCls = getattr(module, mod.capitalize() + 'Mode')
                ret = modeCls.get_mode_raum(raum)
            except AttributeError:
                continue
            except Exception as e:
                logging.exception("exception calling get_mode on %s for raum %s" % (mod, raum.id))
                ret = {'priority': -1, 'use_offsets': False}

            if ret is not None and ret.get('priority', -1) > max_priority:
                max_priority = ret['priority']
                active_mod = {'mod': mod.strip(), 'verbose_name': None, 'icon': None}
                active_mod.update(ret)

        return active_mod

    @staticmethod
    def get_active_mode_regelung(haus, objid, modulename, *args, **kwargs):
        """
        goes through modules and checks priorities,...
        to decide which mode can be returned for the given regelung
        :param haus: Haus inctance
        :param objid: id of regelung object
        :param modulename: the name of relevant module
        :return: a dict containing info of activated mode
        """
        raise NotImplementedError

    @abstractmethod
    def set_mode_raum(self, raum, *args, **kwargs):
        """
        set a mode for the given raum
        :param raum: Raum instance
        :param priority: a digit to be checked for priority before setting mode
        :param mode: mode id or mode instance
        :return: must return something, True or a dict containing mode info that is activated
        """
        raise NotImplementedError

    @abstractmethod
    def get_mode_raum(self, raum, *args, **kwargs):
        """
        get activated mode of the raum
        :param raum: Raum instance
        :return: a dict containing data of activated mode of the given raum
        """
        raise NotImplementedError

    @abstractmethod
    def set_mode_regelung(self, haus, objid, modulename, *args, **kwargs):
        """
        set a mode for the given regelung
        :param raum: Raum instance
        :param mode: mode id or mode instance
        :return: activated mode or True
        """
        raise NotImplementedError

    @abstractmethod
    def get_mode_regelung(self, haus, objid, modulename, *args, **kwargs):
        """
        get activated mode of the regelung
        :param raum: Raum instance
        :param mode: mode id or mode instance
        :return: a dict containing data of activated mode of the given regelung
        """
        raise NotImplementedError

    @abstractmethod
    def set_mode_temperature_raum(self, raum, temperature):
        """
        sets the temperature a given mode has
        :param temperature:
        :return: -
        """
        raise NotImplementedError

    @abstractmethod
    def is_module_activated(self):
        """
        :return: Bolean, check the module is activated
        """
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def get_capabilities(haus):
        """
        :return: a dict containing some info about which data module is providing, like priority
        """
        raise NotImplementedError
