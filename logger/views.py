# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
from heizmanager.models import Raum, AbstractSensor, Haus, GatewayAusgang
import logging
from django.http import HttpResponse
from django.http import JsonResponse
from heizmanager.models import sig_get_outputs_from_output
from models import SimpleOutputLog, SimpleSensorLog
import json
from django.core.serializers.json import DjangoJSONEncoder
import csv
import pytz
from datetime import datetime, timedelta
from sqlite3 import OperationalError
from itertools import chain
from fabric.api import local
from django.http import Http404, StreamingHttpResponse
from wsgiref.util import FileWrapper
import os
from django.core.files.storage import FileSystemStorage


def get_name():
    return u'Logger'


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/logger/'>Logging</a>" % haus.id


def get_global_description_link():
    desc = u"Leistungsfähiges Logging aller Sensordaten und Ausgangszustände."
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-logging/"
    return desc, desc_link


def download(request):
    file_path = "/home/pi/rpi/usbmount/"
    if 'download' in request.GET:
        file_path = "%s%s" % (file_path, request.GET['download'])
        if os.path.exists(file_path):
            filename, file_extension = os.path.splitext(file_path)
            # checking file type
            if file_extension == '.db':
                content_type = 'application/x-sqlite3'
            else:
                content_type = "text/csv"
            # streaming
            chunk_size = 8192
            response = StreamingHttpResponse(FileWrapper(open(file_path, 'rb'), chunk_size),
                                             content_type=content_type)
            response['Content-Length'] = os.path.getsize(file_path)
            response['Content-Disposition'] = 'filename=' + os.path.basename(file_path)
            return response
        else:
            raise Http404
    else:
        raise Http404


def get_global_settings_page(request, haus):

    hparams = haus.get_module_parameters()
    params = hparams.get('logger', {'vhz': 14, 'sensor_intervall': 180})

    if request.method == "GET":
        if 'reset' in request.GET and "on /home/pi/rpi/usbmount" in local("mount", capture=True):
            try:
                local("rm /home/pi/rpi/usbmount/db_log.db")
                local("python manage.py makemigrations logger")
                local("python manage.py migrate logger --database logger")
            except:
                pass
            return render_redirect(request, "/m_setup/%s/logger/" % haus.id)
        else:
            params['haus'] = haus
            params['r_m_active'] = hparams.get('logger_right_menu', True)
            return render_response(request, "m_settings_logger.html", params)

    elif request.method == "POST":

        if 'right_menu_ajax' in request.POST:
            params = haus.get_module_parameters()
            params['logger_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(params)
            return JsonResponse({'message': 'done'})

        try:
            vhz = float(request.POST.get("vhz"))
        except:
            vhz = 14
        try:
            sensor_intervall = float(request.POST.get('sensor_intervall'))
        except:
            sensor_intervall = 180
        err = u""
        if vhz < 1 or vhz > 31:
            err += u"Für Vorhaltenzeit bitte einen Wert von 1 bis 31 angeben.<br/>"
        if sensor_intervall < 0 or sensor_intervall > 3600:
            err += u"Für Speicherintervall bitte einen Wert zwischen 0 und 3600 angeben."
        if len(err):
            params['haus'] = haus
            params['error'] = err
            return render_response(request, "m_settings_logger.html", params)
        params['vhz'] = vhz
        params['sensor_intervall'] = sensor_intervall
        hparams['logger'] = params
        haus.set_module_parameters(hparams)
        return render_redirect(request, "/m_setup/%s/logger/" % haus.id)


def _localize_ts(ts):
    # hrgw schickt einen timestamp als string, der in localtz ist und so in die datenbank geht.
    # muss also lokalisiert werden.
    local = pytz.timezone("Europe/Berlin")
    try:
        ts = datetime.strptime(ts, "%Y-%m-%dT%H:%M")
    except ValueError:
        return None
    else:
        ts = local.localize(ts, is_dst=None).astimezone(pytz.UTC)
        return ts.strftime("%Y-%m-%dT%H:%M")


def get_local_settings_page_haus(request, haus, action=None):

    file_path = "/home/pi/rpi/usbmount/"

    if request.method == "GET":
        # csv
        if action == "csv" or action == 'xcsv':

            if 'ul' in request.GET:
                return render_response(request, 'm_logger_csv_upload.html', {'haus': haus})

            if 'delete' in request.GET and action == 'xcsv':
                filename = request.GET.get('delete')
                filename = file_path + filename
                if os.path.exists(filename):
                    os.remove(filename)

            if os.path.exists(file_path):
                files = os.listdir(file_path)
                csv_files = [fn for fn in files if fn.split('.')[-1] == 'csv' or fn == 'db_log.db']
                return render_response(request, 'm_logger_csv.html', {'haus': haus, 'csv_files': csv_files, 'action': action})

            return render_response(request, 'm_logger_csv.html', {'haus': haus, 'path_error': True})

        if 'gl' in request.GET and 'von' in request.GET and 'bis' in request.GET:
            gl = request.GET['gl']
            # zr = int(request.GET['zr'])
            von = request.GET['von'].replace(' ', '')
            bis = request.GET['bis'].replace(' ', '')
            if len(von) > 16:
                von = von.rsplit(':', 1)[0]
            if len(bis) > 16:
                bis = bis.rsplit(':', 1)[0]
            if 'T' not in von:
                if len(von) == 15:
                    von = von[:10] + 'T' + von[10:]
                elif len(von) == 16:
                    von = von[:10] + 'T' + von[11:]
            if 'T' not in bis:
                if len(bis) == 15:
                    bis = bis[:10] + 'T' + bis[10:]
                elif len(von) == 16:
                    bis = bis[:10] + 'T' + bis[11:]
            if von > bis or not len(von) or not len(bis) or ':' not in von or ':' not in bis:
                return HttpResponse(json.dumps({}), content_type='application/json')

            if 'raum' in gl:
                raumid = gl.split('_')[1]
                raum = Raum.objects.get_for_user(request.user, pk=long(raumid))
                regelung = raum.get_regelung()
                cb2outs = sig_get_outputs_from_output.send(sender='logger', raum=raum, regelung=regelung)
                logging.warning(cb2outs)

                # was wird bei einer rlr mit rts geloggt? die zielwerte fuer die ruecklaeufe, oder?

                for cb, outs in cb2outs:
                    if len(outs):
                        name = '%s_%s' % (outs[0][0].name, outs[0][4][0])
                        break
                else:
                    # nichts gefunden, vielleicht hkt? sollten wir hier nicht machen.
                    # hkt muesste man aber eigentlich ohne grosse konsequenzen in
                    #   sig_get_outputs_from_output aufnehmen koennen. ggf. mal machen.
                    from rf.models import RFAktor
                    hkts = RFAktor.objects.filter(type__in=["hkt","hktGenius","hktControme", "hkta52001"], raum=raum, haus=haus)
                    for hkt in hkts:
                        name = "%s_%s" % (hkt.controller.name, hkt.name)
                        ret = SimpleOutputLog.objects.getlogs(name, von, bis)
                        if len(ret):
                            ret = ['Zielwert', ret]
                            return HttpResponse(json.dumps(ret, cls=DjangoJSONEncoder), content_type='application/json') 
                    else:
                        return HttpResponse(json.dumps(["Zielwert", []]), content_type='application/json')
                 
                ret = SimpleOutputLog.objects.getlogs(name, von, bis)
                logging.warning(len(ret))
                ret = ['Zielwert', ret]

            elif gl.startswith('mischerout_'):
                ret = SimpleOutputLog.objects.getlogs(gl, _localize_ts(von), _localize_ts(bis))
                ret = ['Zielwert', ret]

            elif gl.startswith('mischer_'):
                ret = SimpleSensorLog.objects.getlogs(gl, _localize_ts(von), _localize_ts(bis))
                ret = ["_".join(gl.split('_')[2:]), ret]

            elif gl.startswith('dreg_'):
                ret = SimpleSensorLog.objects.getlogs(gl, _localize_ts(von), _localize_ts(bis))
                ret = [gl.rsplit('_', 1)[1], ret]

            elif gl.startswith('dregout_'):
                ret = SimpleOutputLog.objects.getlogs(gl, _localize_ts(von), _localize_ts(bis))
                ret = ['Zielwert', ret]

            elif gl.startswith('out_'):
                _ret = SimpleOutputLog.objects.getlogs(gl.split('_', 1)[1], von, bis, val="value")
                ret = [gl, []]
                if len(_ret):
                    prev_val = _ret[0][1]
                    ret[1].append(_ret[0])
                for ts, val in _ret[1:]:
                    if val != prev_val:
                        prev_val = val
                        ret[1].append((ts, val))
                if len(_ret):
                    ret[1].append(_ret[-1])

                print ret

            else:
                try:
                    ret = SimpleSensorLog.objects.getlogs(gl, von, bis)
                except OperationalError as e:
                    if 'no such table' not in str(e):
                        raise
                    else:
                        ret = []
                else:
                    logging.warning(len(ret))
                    sensor = AbstractSensor.get_sensor(gl.replace('\\', ''))
                    if sensor:
                        ret = [sensor.description or sensor.name, ret]
                    else:
                        pass

            return HttpResponse(json.dumps(ret, cls=DjangoJSONEncoder), content_type='application/json')

        else:

            err = ""
            try:
                if os.uname()[1] != "contromeproserver" :
                    ret = local("mount | grep usbmount", capture=True)
            except:
                err = "Kein Loggingstick vorhanden."
            else:
                try:
                    ret = local("ls /home/pi/rpi/usbmount", capture=True)
                    if 'db_log.db' not in ret:
                        err = "Datenbank nicht gefunden."
                except:
                    pass  # nicht in err schreiben, kann folge von erstem check sein
                else:
                    try:
                        ret = local('sqlite3 ~/rpi/usbmount/db_log.db ".schema logger_simplesensorlog"', capture=True)
                        if not len(ret):
                            err = "Fehlerhafte Datenbank."
                    except:
                        pass  # s.o.

            if len(err):
                return render_response(request, 'm_logger.html', {'haus': haus, 'err': err})

            rooms_sensors_ids = []
            ret = []
            for etage in haus.etagen.all():
                ret.append((etage.name, []))
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    sensoren = raum.get_sensoren()
                    rooms_sensors_ids.extend([s.get_identifier() for s in sensoren])

                    rlonly = False
                    params = raum.get_regelung().get_parameters()
                    ms = raum.get_mainsensor()
                    if (ms is None and params.get('rlsensorssn', False)) or params.get('ignorems', False):
                        rlonly = True

                    out = raum.regelung.get_ausgang()
                    if out is not None and isinstance(out, GatewayAusgang):
                        outs = [(out.gateway.name, o) for o in sorted([int(_o) for _o in out.ausgang.split(',')])]
                    else:
                        outs = []

                    ret[-1][1].append((raum.name, sensoren, outs, ms, rlonly, raum.id, raum.icon))

            sensors_info = AbstractSensor.get_all_sensors_info(exclude=['RFAktor', 'RFSensor', 'VSensor', 'KNXSensor'])
            sensors = [s for s in sensors_info if s['sensor_id'] not in rooms_sensors_ids]

            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)
            yd = (now-timedelta(days=1)).strftime("%Y-%m-%dT00:00")
            now = now.strftime("%Y-%m-%dT%H:%M")

            return render_response(request, 'm_logger.html', {'haus': haus, 'data': ret, 'bis': now, 'von': yd, 'other_sensors': sensors})

    if request.method == "POST" and request.user.userprofile.get().role != 'K':

        if action == 'xcsv':
            file = request.FILES['csv_file']
            if os.path.exists(file_path + file.name):
                return render_response(request, 'm_logger_csv_upload.html', {'haus': haus, 'error': 'file_exists'})

            if file.name[-3:] != 'csv':
                return render_response(request, 'm_logger_csv_upload.html', {'haus': haus, 'error': 'not_csv'})

            fs = FileSystemStorage()
            fs.save(file_path + file.name, file)
            return render_redirect(request, "/m_logger/%s/csv/" % haus.id)

        # hier auch noch die zielwertkurve mit reinnehmen

        # zr = int(request.POST['zeitraum'])
        von = request.POST['von'].replace(' ', '')
        bis = request.POST['bis'].replace(' ', '')
        if len(von) > 16:
            von = von.rsplit(':', 1)[0]
        if len(bis) > 16:
            bis = bis.rsplit(':', 1)[0]
        if 'T' not in von:
            if len(von) == 15:
                von = von[:10] + 'T' + von[10:]
            elif len(von) == 16:
                von = von[:10] + 'T' + von[11:]
        if 'T' not in bis:
            if len(bis) == 15:
                bis = bis[:10] + 'T' + bis[10:]
            elif len(von) == 16:
                bis = bis[:10] + 'T' + bis[11:]
        if von > bis:
            return HttpResponse(json.dumps({}), content_type='application/json')

        tempvals = []
        outvals = []
        logging.warning(request.POST)
        for k in request.POST:
            if request.POST[k] == 'on' and '*' in k:  # das zweite, damit keine zieltemperatur runtergeladen wird
                tempvals.append(k.replace('\\', ''))
            elif request.POST[k] == 'on' and k.startswith("out_"):
                outvals.append(k.split('_', 1)[1])
            elif request.POST[k] == 'on' and k.endswith("_ziel"):
                raumid = k.split('_')[1]
                raum = Raum.objects.get_for_user(request.user, pk=long(raumid))
                regelung = raum.get_regelung()
                cb2outs = sig_get_outputs_from_output.send(sender='logger', raum=raum, regelung=regelung)

                for cb, outs in cb2outs:
                    if len(outs):
                        name = '%s_%s' % (outs[0][0].name, outs[0][4][0])
                        outvals.append(name)
                else:
                    # nichts gefunden, vielleicht hkt? sollten wir hier nicht machen.
                    # hkt muesste man aber eigentlich ohne grosse konsequenzen in
                    #   sig_get_outputs_from_output aufnehmen koennen. ggf. mal machen.
                    from rf.models import RFAktor
                    hkts = RFAktor.objects.filter(type__in=["hkt","hktGenius","hktControme", "hkta52001"], raum=raum, haus=haus)
                    for hkt in hkts:
                        name = "%s_%s" % (hkt.controller.name, hkt.name)
                        outvals.append(name)

        if not len(tempvals) and not len(outvals):
            return render_redirect(request, "/m_logger/%s/" % haus.id)
        tempret = SimpleSensorLog.objects.downloadlogs(tempvals, von, bis)
        outret = SimpleOutputLog.objects.downloadlogs(outvals, von, bis)
        outretziel = SimpleOutputLog.objects.downloadlogs(outvals, von, bis, val="ziel")
        outretziel = [(row[0], row[1] + "_ziel", row[2]) for row in outretziel]
        ret = chain(list(tempret), list(outret), list(outretziel))
        ret = sorted(ret, key=lambda x: x[0])

        response = HttpResponse(content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=sensorlogs.csv'  # freundlicherer Name?
        writer = csv.writer(response)
        writer.writerow(['Zeit', 'Name', 'Wert'])

        id2names = dict((name, AbstractSensor.get_sensor(name)) for name in tempvals)
        id2names.update(dict((name, name) for name in outvals))
        id2names.update(dict((name+"_ziel", name+"_ziel") for name in outvals))
        for row in ret:
            writer.writerow([row[0][:-7], id2names[row[1]], row[2]])

        return response


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_logger.html", {'haus': haus})
