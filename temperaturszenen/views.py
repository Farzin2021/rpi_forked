# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
from datetime import datetime, timedelta
import heizmanager.cache_helper as ch
from heizmanager.models import Haus, Raum, Etage
import logging
from django.http import HttpResponse
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt
import pytz
import uuid
import functools
import copy
from heizmanager.abstracts.base_mode import BaseMode
from heizmanager.abstracts.base_time_task import TimeTask
import ast

logger = logging.getLogger("logmonitor")


class TemperaturszenenMode(BaseMode):

    def __init__(self, mode_id):
        self.mode_id = mode_id

    @staticmethod
    def set_mode_all_rooms(hausid, priority, mode_id):
        haus = Haus.objects.get(id=hausid)
        mode = TemperaturszenenMode(mode_id)
        for etage in haus.etagen.all():
            for raum in etage.raeume.all():
                mode.set_mode_raum(raum, priority)
        return True

    def set_mode_raum(self, raum, priority=1):
        """
        set ts mode, it'd fetch current active mode first then check with priority if given mode has more
        prio, will set it, if not will set it in the background
        :param raum:
        :param priority:
        :param activate_in_background: when it's true mode will be changed but won't affect on raum temperature
        :return: true if mode has been changed
        """
        raum = Raum.objects.filter(pk=raum.id)[0]

        activated_mode = self.get_active_mode_raum(raum)
        activate_in_background = False
        if activated_mode is not None and priority < activated_mode.get('priority', -1):
            activate_in_background = True
        room_params = raum.get_module_parameters()
        room_mode_params = room_params.get("temperaturmodi", {})
        room_mode_params[self.mode_id] = room_mode_params.get(self.mode_id, {})
        if room_mode_params.get(self.mode_id, {}).get('enabled', True) or self.mode_id == 1:
            room_mode_params['activated_mode'] = self.mode_id
            room_params['temperaturmodi'] = room_mode_params
            raum.set_module_parameters(room_params)
            if not activate_in_background:
                solltemp = float(str(room_mode_params.get(self.mode_id, {}).get('solltemp', 21.0)).replace(',', '.'))
                raum.solltemp = solltemp
                raum.save()
                logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur auf %.2f gesetzt" % raum.solltemp))
            return True
        return False

    def set_mode_temperature_raum(self, raum, temperature):
        room_params = raum.get_module_parameters()
        temp_modes = copy.deepcopy(room_params.get('temperaturmodi', {}))

        if self.mode_id not in temp_modes:
            temp_modes[self.mode_id] = {}

        if temp_modes[self.mode_id].get('enabled') is None:
            # set default
            temp_modes[self.mode_id]['enabled'] = True

        if temp_modes[self.mode_id].get('enabled', True) or self.mode_id == 1:
            temp_modes[self.mode_id]['solltemp'] = temperature
            room_params['temperaturmodi'] = temp_modes
            raum.set_module_parameters(room_params)
            if temp_modes.get('activated_mode') == self.mode_id:
                # this mode is currently active
                raum.solltemp = temperature
                raum.save()
                logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur auf %.2f gesetzt (T1)" % raum.solltemp))

        return True

    def get_raum_mode_params(self, raum):
        room_mode_params = ast.literal_eval(Raum.objects.filter(pk=raum.id)
                                            .values_list("module_parameters")[0][0]).get('temperaturmodi', {})
        room_mode_params[self.mode_id] = room_mode_params.get(self.mode_id, {})
        return room_mode_params

    def enable_mode_raum(self, raum):
        # mode #1 is a fixed mode and always enabled
        if self.mode_id == 1:
            return True
        room_mode_params = self.get_raum_mode_params(raum)
        room_mode_params[self.mode_id]['enabled'] = True
        raum.set_spec_module_params('temperaturmodi', room_mode_params)
        solltemp = room_mode_params[self.mode_id].get('solltemp', 21.0)
        if room_mode_params.get('activated_mode') == self.mode_id:
            raum.solltemp = solltemp
            raum.save()
            logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur auf %.2f gesetzt" % raum.solltemp))
        return True

    def disable_mode_raum(self, raum):
        # mode #1 is a fixed mode and always enabled
        if self.mode_id == 1:
            return True
        room_mode_params = self.get_raum_mode_params(raum)
        room_mode_params[self.mode_id]['enabled'] = False
        raum.set_spec_module_params('temperaturmodi', room_mode_params)
        return True

    def is_mode_enabled_raum(self, raum):
        return self.get_raum_mode_params(raum).get('enabled', True)

    @classmethod
    def get_mode_raum(cls, raum, *args, **kwargs):
        haus = raum.etage.haus
        hparams = haus.get_module_parameters()
        temp_mode_params = hparams.get("temperaturmodi", None)
        # check was activated once
        if temp_mode_params is None:
            return None

        room_mode_params = ast.literal_eval(Raum.objects.filter(pk=raum.id).values_list("module_parameters")[0][0]).get('temperaturmodi', {})
        activated_mode_id = room_mode_params.get('activated_mode', 1)
        is_enabled = room_mode_params.get(activated_mode_id, {}).get("enabled", True) or activated_mode_id == 1
        if not is_enabled:
            return
        soll = room_mode_params.get(activated_mode_id, {}).get("solltemp", 21.0)
        activated_mode_name = temp_mode_params.get(activated_mode_id, {}).get("name", '-')
        ret = dict()
        ret['soll'] = soll
        ret['activated_mode_text'] = activated_mode_name
        ret['activated_mode_id'] = activated_mode_id
        ret['activated_mode'] = TemperaturszenenMode(activated_mode_id)
        ret.update(TemperaturszenenMode.get_capabilities(haus))
        return ret

    def set_mode_regelung(self, haus, objid, modulename, *args, **kwargs):
        pass

    def get_mode_regelung(self, haus, objid, modulename, *args, **kwargs):
        pass

    def is_module_activated(self):
        pass

    @staticmethod
    def get_capabilities(haus):
        ret = dict()
        hparams = haus.get_module_parameters()
        ret['priority'] = 1
        ret['use_offsets'] = True
        if hparams.get('is_switchingmode_enabled_%s' % haus.id, True):
            ret['icon'] = 'icon-clock'
        else:
            ret['icon'] = 'icon-clock-gray'
        ret['verbose_name'] = 'temperaturszenen'
        ret['mod'] = 'temperaturszenen'
        return ret


class TemperaturszenenTask(TimeTask):

    method_factory = {
        'set-mode': TemperaturszenenMode.set_mode_all_rooms
    }

    @staticmethod
    def get_module_tasks(haus):
        pass


def get_cache_ttl():
    return 1800


def calculate_always_anew():
    return False


def get_offset(haus, raum=None):
    return 0.0


def get_name():
    return 'Temperaturszenen'


def get_local_settings_link(request, raum):
    pass


def get_raum_from_id(func):
    @functools.wraps(func)
    def wrapper_decorator(raum, *args, **kwargs):
        if isinstance(raum, int):
            try:
                raum = Raum.objects.get(id=raum)
                return func(raum, *args, **kwargs)
            except Raum.DoesNotExist:
                return False
        return func(raum, *args, **kwargs)
    return wrapper_decorator


@get_raum_from_id
def set_room_mode_temperature(raum, mode_id, temp):
    tz_mode = TemperaturszenenMode(mode_id)
    return tz_mode.set_mode_temperature_raum(raum, float(temp))


@get_raum_from_id
def set_room_mode_offset(raum, mode_id, offset):
    tz_mode = TemperaturszenenMode(mode_id)
    mode_solltemp = tz_mode.get_raum_mode_params(raum).get(mode_id, {}).get('solltemp')
    if mode_solltemp:
        new_temp = float(mode_solltemp) + float(offset)
        return tz_mode.set_mode_temperature_raum(raum, float(mode_solltemp) + float(offset))
    return False


@get_raum_from_id
def set_room_mode_enable_status(raum, mode_id, status):
    status = bool(int(status))
    tz_mode = TemperaturszenenMode(mode_id)
    if status:
        return tz_mode.enable_mode_raum(raum)
    else:
        return tz_mode.disable_mode_raum(raum)


def get_from_factory(action):
    action_factory = {
        'set_temperature': set_room_mode_temperature,
        'set_offset': set_room_mode_offset,
        'set_enable': set_room_mode_enable_status,
    }
    return action_factory[action]


def activate(obj):
    if 'temperaturszenen' not in obj.get_modules():
        obj.set_modules(obj.get_modules() + ['temperaturszenen'])


def deactivate(obj):
    modules = obj.get_modules()
    try:
        modules.remove('temperaturszenen')
        obj.set_modules(modules)
    except ValueError:  # nicht in modules
        pass


def get_local_settings_page(request, raum):
    haus = raum.etage.haus
    hparams = haus.get_module_parameters()
    mode_hparams = copy.deepcopy(hparams.get('temperaturmodi', {}))
    room_params = raum.get_module_parameters()
    room_mode_params = copy.deepcopy(room_params.get("temperaturmodi", {}))

    if request.method == "GET":
        page_param = {}
        page_param['haus'] = haus
        page_param['raum'] = raum
        mode_items = []

        for index, (mode_id, mode_params) in enumerate(mode_hparams.iteritems(), 1):
            room_mode_params[mode_id] = room_mode_params.get(mode_id, {})
            room_mode_params[mode_id]['solltemp'] = room_mode_params[mode_id].get('solltemp', 21)
            room_mode_params[mode_id]['enabled'] = room_mode_params[mode_id].get('enabled', True)
            mode_items.append((mode_id, mode_params, mode_params.get('order', index), room_mode_params[mode_id]))

        mode_items.sort(key=lambda tup: tup[2])

        page_param['modes'] = mode_items
        return render_response(request, "m_raum_temperaturszenen.html", page_param)

    if request.method == "POST":
        mode_id = int(request.POST.get('mode_id', 0))
        if not mode_id and mode_id != 0:
            return HttpResponse(status=400)

        if 'mode-active' in request.POST:
            tz_mode = TemperaturszenenMode(mode_id)
            is_checked = bool(request.POST['is_checked'])
            if is_checked:
                tz_mode.enable_mode_raum(raum)
            else:
                tz_mode.disable_mode_raum(raum)
            return HttpResponse("done")

        if 'set_slider' in request.POST:
            solltemp = request.POST.get('slider_val', None)
            try:
                solltemp = float(str(solltemp).replace(',', '.'))
            except ValueError:
                return HttpResponse("error")

            TemperaturszenenMode(mode_id).set_mode_temperature_raum(raum, solltemp)
            return HttpResponse("done")

        if 'mode_activate' in request.POST:

            # ts is activated via room list / individual room

            if mode_id not in mode_hparams:
                return HttpResponse(json.dumps({'error': 'key not found'}), content_type="application/json")

            ret = {}
            if room_mode_params.get(mode_id, {}).get('enabled', True):

                a_s = ch.get("active_switch")
                if a_s is None or a_s != mode_id:
                    ch.set("active_switch", mode_id, time=20)
                    logger.warning("%s|%s" % (0, u"Temperaturszene %s manuell aktiviert." % mode_hparams[mode_id]['name']))

                TemperaturszenenMode(mode_id).set_mode_raum(raum)
                ret['activated_mode'] = mode_id
                ret['success'] = True
                ret['raum_id'] = raum.id
                ret['solltemp'] = room_mode_params.get(mode_id, dict()).get('solltemp', 21)
                mode_name = mode_hparams[mode_id].get('name', '-')
                if len(mode_name) > 10:
                    mode_name = mode_name[0:7] + '...'
                ret['activated_mode_name'] = mode_name
            return HttpResponse(json.dumps(ret), content_type="application/json")

        if 'mode_activate_temporary' in request.POST:

            # ts is activated via quickui

            raum = Raum.objects.filter(pk=raum.id)[0]
            room_params = raum.get_module_parameters()
            room_mode_params = copy.deepcopy(room_params.get("temperaturmodi", {}))

            from quickui.helpers import get_nearest_time
            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)
            all_time = int(request.POST.get('all_time'))
            show_time = get_nearest_time(now + timedelta(minutes=all_time)).strftime("%H:%M")

            mode_id = int(request.POST.get('mode_id'))
            mode_params = room_mode_params.get(mode_id, {})
            if mode_params.get('enabled', True) or mode_id == 1:
                ziel = mode_params.get('solltemp', 21.0)

                from quickui.views import QuickuiMode
                quim = QuickuiMode(ziel, all_time)
                quim.set_mode_raum(raum)

                return HttpResponse(json.dumps({'raum_id': raum.id, 'ziel': ziel, 'time': show_time}), content_type="application/json")
            else:
                return HttpResponse(status=404)


def get_local_settings_page_haus(request, haus):

    params = haus.get_module_parameters()
    temp_modes = params.get("temperaturmodi", {})
    rooms = []
    rooms_for_select = []
    if request.method == "GET":
        mode_items = []
        for etage in haus.etagen.all():
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                rooms_for_select.append((raum.id, raum.name))
                rparams = raum.get_module_parameters().get('temperaturmodi', {})
                mode_items = []
                for index, (mode_id, mode_params) in enumerate(temp_modes.iteritems(), 1):
                    mode_rparams = rparams.get(mode_id, {})
                    mode_rparams['solltemp'] = mode_rparams.get('solltemp', 21)
                    mode_rparams['enabled'] = mode_rparams.get('enabled', True)
                    mode_items.append((mode_id, mode_params, mode_params.get('order', index), mode_rparams))
                mode_items.sort(key=lambda tup: tup[2])
                rooms.append((raum, mode_items))

        page_param = {}
        page_param['haus'] = haus
        page_param['rooms'] = rooms
        page_param['select_rooms'] = rooms_for_select
        page_param['select_modes'] = mode_items
        return render_response(request, "m_temperaturszenen.html", page_param)

    if request.method == "POST":
        if 'filter' in request.POST:
            mode_id = request.POST.get('mode_id', 'all')
            raum_id = request.POST.get('raum_id', 'all')

            if raum_id == 'all':
                raum_id = False
            else:
                raum_id = int(raum_id)

            if mode_id == 'all':
                mode_id = False
            else:
                mode_id = int(mode_id)

            rooms = []
            for etage in haus.etagen.all():
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    if raum_id:
                        if raum_id != raum.id:
                            continue

                    for index, (modeid, mode_params) in enumerate(temp_modes.iteritems(), 1):
                        if mode_id:
                            if mode_id != modeid:
                                continue
                                
                        rparams = raum.get_module_parameters().get('temperaturmodi', {})
                        mode_rparams = rparams.get(modeid, {})
                        mode_rparams['mode_id'] = modeid
                        mode_rparams['raum_id'] = raum.id
                        rooms.append(mode_rparams)

            return HttpResponse(json.dumps(rooms), content_type="application/json")

        if 'multiple_action' in request.POST:
            action = request.POST['action']
            items = json.loads(request.POST.get('mode_ids', []))
            value = request.POST['value']
            successful_changes = []
            for item in items:
                if get_from_factory(action)(int(item['raum_id']), item['mode_id'], value):
                    successful_changes.append({
                        'raum_id': item['raum_id'],
                        'mode_id': item['mode_id'],
                        'value': value
                        })

            return HttpResponse(json.dumps(successful_changes), content_type="application/json")


def get_global_settings_page_help(request, haus):
    return None


def get_local_settings_link_haus(request, haus):
    pass


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/temperaturszenen/'>Temperaturszenen</a>" % haus.id


def get_global_description_link():
    desc = u"Beliebige Temperaturszenen anlegen und aktivieren."
    desc_link = "http://support.controme.com/temperaturszenen/"
    return desc, desc_link


@csrf_exempt
def get_global_settings_page(request, haus, action=None, entityid=None):
    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    if request.method == "GET":
        if action == "edit":
            params = haus.get_module_parameters()
            modes = params.get("temperaturmodi", {})
            # Get a sorted list for syncing select box
            sorted_lst = sorted(modes, key=lambda x: (modes[x].get('order', 99)))
            tempmode_items = [(item, modes[item]['name']) for item in sorted_lst]

            if entityid:

                modes = modes.get(int(entityid))
                modename = modes['name']
                qlink_uuid = modes.get('quick_link_uuid', "")
                syncing_mode_selected = modes.get('syncing_mode_selected', None)
                if int(entityid) == 1:
                    modename.replace('*', '')

                return render_response(request, "m_settings_temperaturszenen_edit.html",
                                       {"haus": haus, "objid": entityid, 'mode_name': modename,
                                        "qlink_uuid": qlink_uuid, 'modes': tempmode_items, 'syncing_mode_selected': syncing_mode_selected})
            else:
                return render_response(request, "m_settings_temperaturszenen_edit.html", {"haus": haus,
                                                                                          'modes': tempmode_items})

        elif action == "delete":

            try:
                entityid = int(entityid)
            except TypeError:
                return HttpResponse(status=400)

            params = haus.get_module_parameters()
            temp_modes = params.get("temperaturmodi", {})
            if entityid in temp_modes:
                del temp_modes[entityid]
                ch.delete('switches_mode_list_%s' % haus.id)
                switching_mode_list = params.get('switchingmode', [])
                switching_mode_list = filter(lambda x: x['mode_id'] != entityid, switching_mode_list)
                params['switchingmode'] = switching_mode_list
            else:
                return HttpResponse(status=400)

            params['temperaturmodi'] = temp_modes
            haus.set_module_parameters(params)

            #delete params from room
            for etage in haus.etagen.all():
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    r_params = raum.get_module_parameters()
                    if 'temperaturmodi' in r_params:
                        if entityid in r_params['temperaturmodi']:
                            del r_params['temperaturmodi'][entityid]
                            raum.set_module_parameters(r_params)
            return render_redirect(request, "/m_setup/%s/temperaturszenen/" % haus.id)

        elif action == "getmodeslist":
            params = haus.get_module_parameters()
            temp_modes = params.get("temperaturmodi", {})
            mode_items = []
            for index, (mode_id, mode_params) in enumerate(temp_modes.iteritems(), 1):
                mode_items.append((mode_id, mode_params, mode_params.get('order', index)))
            mode_items.sort(key=lambda tup: tup[2])
            return HttpResponse(json.dumps(mode_items), content_type="application/json")

        elif action == "generateurl":
            params = haus.get_module_parameters()
            temp_modes = params.get("temperaturmodi", {})
            uuid_str = str(uuid.uuid4())
            temp_modes[int(entityid)]['quick_link_uuid'] = uuid_str
            params['temperaturmodi'] = temp_modes
            haus.set_module_parameters(params)
            return HttpResponse(json.dumps({'uuid': uuid_str}), content_type="application/json")

        else:
            context = dict()
            params = haus.get_module_parameters()
            context['haus'] = haus
            context['r_m_active'] = params.get('temperaturszenen_right_menu', True)
            return render_response(request, "m_settings_temperaturszenen.html", context)

    elif request.method == "POST":
        if 'right_menu_ajax' in request.POST:
            params = haus.get_module_parameters()
            params['temperaturszenen_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(params)
            return JsonResponse({'message': 'done'})

        if action == "setmodeslist":
            modes = request.POST.getlist("modes[]")
            params = haus.get_module_parameters()
            temp_modes = params.get("temperaturmodi", {})

            for mode_id, mode_params in temp_modes.iteritems():
                temp_modes[mode_id]['order'] = int(modes.index(str(mode_id)))

            params['temperaturmodi'] = temp_modes
            haus.set_module_parameters(params)
            return HttpResponse("done")

        elif action == "edit":
            params = haus.get_module_parameters()
            temp_modes = params.get('temperaturmodi', {})

            if entityid:
                temp_modes[int(entityid)]['name'] = request.POST['modename']
                if int(entityid) == 1:
                    temp_modes[1]['name'] = temp_modes[1]['name'].split('*', 1)[0] + "*"
                params['temperaturmodi'] = temp_modes
                haus.set_module_parameters(params)
            else:
                try:
                    entityid = max(temp_modes, key=int) + 1
                except ValueError:
                    entityid = 1

                temp_modes[entityid] = {'name': request.POST['modename'], 'syncing_mode_selected': int(request.POST['syncing_mode_selected']), 'quick_link_uuid': str(uuid.uuid4())}
                params['temperaturmodi'] = temp_modes
                haus.set_module_parameters(params)

            # check if is there any syncing mode
            syncing_mode_selected = request.POST.get('syncing_mode_selected', -1)
            if syncing_mode_selected and syncing_mode_selected != -1:
                syncing_mode_selected = int(syncing_mode_selected)
                for etage in haus.etagen.all():
                    for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                        r_params = raum.get_module_parameters()
                        if 'temperaturmodi' in r_params:
                            if syncing_mode_selected in r_params['temperaturmodi']:
                                # syncing with selected mode
                                r_params['temperaturmodi'][int(entityid)] = r_params['temperaturmodi'].get(syncing_mode_selected, {})
                                raum.set_module_parameters(r_params)
            return render_redirect(request, "/m_setup/%s/temperaturszenen/" % haus.id)


def get_jsonapi(haus, usr, entityid=None):

    hparams = haus.get_module_parameters()
    mode_hparams = hparams.get('temperaturmodi', {})
    mode_lst = []

    # get params for sort
    for index, (mode_id, mode_params) in enumerate(mode_hparams.iteritems(), 1):
        mode_lst.append((mode_id, mode_params.get('name', ''), mode_params.get('order', index)))

    mode_lst.sort(key=lambda tup: tup[2])
    ret = []
    etagen = Etage.objects.filter(haus=haus)
    for tup in mode_lst:
        mode_id = tup[0]
        raeume = []
        for etage in etagen:
            for raum in Raum.objects.filter_for_user(usr, etage_id=etage.id).order_by("position"):
                room_params = raum.get_module_parameters()
                room_mode_params = room_params.get("temperaturmodi", {})
                if room_mode_params.get(mode_id, {}).get('enabled', True):
                    raeume.append({
                        "name": "%s / %s" % (etage.name, raum.name),
                        "temperatur": room_mode_params.get(mode_id, {}).get('solltemp', 20.0),
                        "active": True if room_mode_params.get('activated_mode', 1) == mode_id else False
                    })
        ret.append({'id': mode_id, 'Name': tup[1], 'raeume': raeume})

    return ret


def set_jsonapi(data, haus, usr, entityid):
    try:
        hparams = haus.get_module_parameters()
        tm = hparams['temperaturmodi'][int(entityid)]
    except KeyError:
        return {'message': "there is no mode with given id."}

    mode_id = int(entityid)
    duration = None
    if 'duration' in data:
        if data['duration'] == 'default':
            duration = hparams.get('quickui', {}).get('default_duration', 180)
        else:
            duration = int(data['duration'])

    return set_mode(haus, usr, mode_id, duration)


@csrf_exempt
def qlink_set_mode(request, hausid, qlink_uuid, duration=None):
    try:
        haus = Haus.objects.get(pk=long(hausid))
    except (Haus.DoesNotExist, TypeError):
        return {'error': 'haus with given hausid does not exits.'}

    mode_id = -1
    tempmode_params = haus.get_module_parameters().get("temperaturmodi", {})
    for k, v in tempmode_params.items():
        uuid = v.get('quick_link_uuid', '')
        if uuid == qlink_uuid:
            mode_id = k
            break

    user = haus.eigentuemer  # todo
    ret = set_mode(haus, user, mode_id, duration)
    return HttpResponse(json.dumps(ret), content_type='application/json')


def set_mode(haus, user, objectid, duration=None):

    # helper for qlink_set_mode and set_jsonapi above

    try:
        hparams = haus.get_module_parameters()
        tm = hparams['temperaturmodi'][int(objectid)]
    except KeyError:
        return {'error': "there is no mode with given id."}

    mode_id = int(objectid)

    if duration:
        from quickui.views import QuickuiMode

        all_time = int(duration)
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        # ex_time = (now + timedelta(minutes=all_time)).strftime("%Y-%m-%d %H:%M")
        show_time = (now + timedelta(minutes=all_time)).strftime("%H:%M")

        room_dict = {}
        for etage in haus.etagen.all():
            for raum in Raum.objects.filter_for_user(user, etage_id=etage.id):  # todo user is haus.eigentuemer, also ist das filtern hier bloedsinn
                room_params = raum.get_module_parameters()
                room_mode_params = room_params.get("temperaturmodi", {})
                mode_params = room_mode_params.get(mode_id, {})
                if mode_params.get('enabled', True) or mode_id == 1:
                    ziel = mode_params.get('solltemp', 21.00)

                    quim = QuickuiMode(ziel, all_time)
                    quim.set_mode_raum(raum)

                    logger.warning(u"%s|%s" % (raum.id, u"Temperaturszene %s via API aktiviert" % hparams['temperaturmodi'][mode_id]['name']))
                    room_dict[raum.id] = ziel

        return {'roomlist': room_dict, 'time': show_time}

    else:
        for etage in haus.etagen.all():
            for raum in Raum.objects.filter_for_user(user, etage_id=etage.id):

                room_params = raum.get_module_parameters()
                room_mode_params = room_params.get("temperaturmodi", {})
                mode_params = room_mode_params.get(mode_id, {})
                if mode_params.get('enabled', True):
                    TemperaturszenenMode(mode_id).set_mode_raum(raum)

        return {}
