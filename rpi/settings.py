"""
Django settings for rpi project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# SECRET_KEY = ''
from secret_key import SECRET_KEY

# SECURITY WARNING: don't run with debug turned on in production!

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'dataslave',
    'logger',
    'heizmanager',
    'config',

    # module:
    'nullregelung',
    'gatewaymonitor',
    'betriebsarten',
    'zeitschalter',
    'raumgruppen',
    'vsensoren',
    'wetter',
    'pumpenlogik',
    'gcal',
    'users',
    'solarpuffer',
    'hochtarifoptimierung',
    'rf',
    'ruecklaufregelung',
    'funksollregelung',
    'zweipunktregelung',
    'wochenkalender',
    'jsonapi',
    'jahreskalender',
    'vorlauftemperaturregelung',
    'differenzregelung',
    'luftfeuchte',
    'benutzerverwaltung',
    'geolocation',
    'aussentemperaturkorrektur',
    'backupcontrol',
    'timer',
    'heizflaechenoptimierung',
    'logmonitor',
    'fps',
    'vorlauftemperaturkorrektur',
    'telegrambot',
    'kontaktabsenkung',
    'temperaturszenen',
    'fernzugriff',
    'heizprogramm',
    'wetter_pro',
    'alexa_interface',
    'homebridge',
    'aha',
    'ki',
    'kaminofen',
    'knx',
    'fenster_offen',
    'wifi'
)

TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [
                os.path.join(os.path.dirname(__file__), '../heizmanager/templates'),
                os.path.join(os.path.dirname(__file__), '../heizmanager/templates/m'),
                os.path.join(os.path.dirname(__file__), '../zeitschalter/templates'),
                os.path.join(os.path.dirname(__file__), '../raumgruppen/templates'),
                os.path.join(os.path.dirname(__file__), '../vsensoren/templates'),
                os.path.join(os.path.dirname(__file__), '../wetter/templates'),
                os.path.join(os.path.dirname(__file__), '../pumpenlogik/templates'),
                os.path.join(os.path.dirname(__file__), '../gcal/templates'),
                os.path.join(os.path.dirname(__file__), '../users/templates'),
                os.path.join(os.path.dirname(__file__), '../solarpuffer/templates'),
                os.path.join(os.path.dirname(__file__), '../hochtarifoptimierung/templates'),
                os.path.join(os.path.dirname(__file__), '../rf/templates'),
                os.path.join(os.path.dirname(__file__), '../ruecklaufregelung/templates'),
                os.path.join(os.path.dirname(__file__), '../logger/templates'),
                os.path.join(os.path.dirname(__file__), '../praesenzregelung/templates'),
                os.path.join(os.path.dirname(__file__), '../wochenkalender/templates'),
                os.path.join(os.path.dirname(__file__), '../jahreskalender/templates'),
                os.path.join(os.path.dirname(__file__), '../vorlauftemperaturregelung/templates'),
                os.path.join(os.path.dirname(__file__), '../differenzregelung/templates'),
                os.path.join(os.path.dirname(__file__), '../luftfeuchte/templates'),
                os.path.join(os.path.dirname(__file__), '../benutzerverwaltung/templates'),
                os.path.join(os.path.dirname(__file__), '../geolocation/templates'),
                os.path.join(os.path.dirname(__file__), '../aussentemperaturkorrektur/templates'),
                os.path.join(os.path.dirname(__file__), '../backupcontrol/templates'),
                os.path.join(os.path.dirname(__file__), '../timer/templates'),
                os.path.join(os.path.dirname(__file__), '../heizflaechenoptimierung/templates'),
                os.path.join(os.path.dirname(__file__), '../logmonitor/templates'),
                os.path.join(os.path.dirname(__file__), '../fps/templates'),
                os.path.join(os.path.dirname(__file__), '../vorlauftemperaturkorrektur/templates'),
                os.path.join(os.path.dirname(__file__), '../telegrambot/templates'),
                os.path.join(os.path.dirname(__file__), '../kontaktabsenkung/templates'),
                os.path.join(os.path.dirname(__file__), '../quickui/templates'),
                os.path.join(os.path.dirname(__file__), '../fernzugriff/templates'),
                os.path.join(os.path.dirname(__file__), '../wetter_pro/templates'),
                os.path.join(os.path.dirname(__file__), '../betriebsarten/templates'),
                os.path.join(os.path.dirname(__file__), '../gatewaymonitor/templates'),
                os.path.join(os.path.dirname(__file__), '../alexa_interface/templates'),
                os.path.join(os.path.dirname(__file__), '../homebridge/templates'),
                os.path.join(os.path.dirname(__file__), '../aha/templates'),
                os.path.join(os.path.dirname(__file__), '../ki/templates'),
                os.path.join(os.path.dirname(__file__), '../kaminofen/templates'),
                os.path.join(os.path.dirname(__file__), '../offsets/templates'),
                os.path.join(os.path.dirname(__file__), '../knx/templates'),
                os.path.join(os.path.dirname(__file__), '../fenster_offen/templates'),
                os.path.join(os.path.dirname(__file__), '../wifi/templates'),
            ],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                    # list if you haven't customized them:
                    'django.contrib.auth.context_processors.auth',
                    'django.template.context_processors.request',
                    'django.template.context_processors.debug',
                    'django.template.context_processors.i18n',
                    'django.template.context_processors.media',
                    'django.template.context_processors.static',
                    'django.template.context_processors.tz',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    # 'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'heizmanager.profiling_middleware.ProfileMiddleware',
    'heizmanager.cached_authentication_middleware.Middleware',
    'heizmanager.login_required_middleware.LoginRequiredMiddleware',
)

ROOT_URLCONF = 'rpi.urls'

LOGIN_URL = '/accounts/m_login/'
LOGIN_EXEMPT_URLS = (
    r'^accounts/',
    r'^set/',
    r'^get/',
    r'^dbul/',
)

WSGI_APPLICATION = 'rpi.wsgi.application'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': None
    },
    'cache_machine': {
        'BACKEND': 'caching.backends.memcached.MemcachedCache',
        'LOCATION': [
            '127.0.0.1:11211'
        ],
        'KEY_PREFIX': 'dcm:',
        'CACHE_INVALIDATE_ON_CREATE': 'whole-model',
    },
}

SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Berlin'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

try:
    from settings_dev import *
except ImportError:
    DEBUG = False
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'logmonitor': {
                'format': '%(pathname)s.%(funcName)s|%(asctime)s|%(message)s',
                'datefmt': '%d.%m. %H:%M:%S',
                'converter': 'time.localtime'
            },
            'servicemonitorlog': {
                'format': '%(pathname)s.%(funcName)s|%(asctime)s|%(message)s',
                'datefmt': '%d.%m. %H:%M:%S',
                'converter': 'time.localtime'
            }
        },
        'handlers': {
            'logmonitor': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'formatter': 'logmonitor',
                'filename': '/var/log/uwsgi/uwsgi.log'
            },
            'servicemonitorlog': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'formatter': 'servicemonitorlog',
                'filename': '/var/log/uwsgi/service_monitor.log1'
            }
        },
        'loggers': {
            'logmonitor': {
                'handlers': ['logmonitor'],
                'propagate': False
            },
            'servicemonitorlog': {
                'handlers': ['servicemonitorlog'],
                'propagate': False
            }
        }
    }
    
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': '/home/pi/rpi/db.sqlite3',
        },
        'logger': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': '/home/pi/rpi/usbmount/db_log.db',
        }
    }

DATABASE_ROUTERS = ['logger.logger_dbrouter.LoggerRouter', 'heizmanager.hm_dbrouter.HeizmanagerRouter']
