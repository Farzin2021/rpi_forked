from celery import shared_task
from heizmanager.models import Haus
from heizmanager.mobile.m_temp import get_module_offsets
import heizmanager.cache_helper as ch
import pytz
from datetime import datetime


@shared_task
def calc_deviation():

    for haus in Haus.objects.all():

        if 'vorlauftemperaturkorrektur' not in haus.get_modules():
            continue

        deviation_dict = {}
        for etage in haus.etagen.all():
            for raum in etage.raeume.all():

                regparams = raum.regelung.get_parameters()
                ignorems = regparams.get('ignorems', False)
                if ignorems:
                    continue
                else:
                    target_temp = raum.solltemp

                ms = raum.get_mainsensor()
                if ms:
                    last_rt = ms.get_wert()
                    if last_rt is not None:
                        current_temp = last_rt[0]
                    else:
                        continue

                    _offsets = get_module_offsets(raum, only_clear_offsets=True, as_dict=True)
                    try:
                        del _offsets['heizflaechenoptimierung']
                    except:
                        pass
                    offset = sum(_offsets.values())

                    target_temp = float(target_temp) + float(offset)
                    deviation = current_temp - target_temp
                    deviation_dict[raum.id] = deviation

        params = haus.get_module_parameters()
        params.setdefault('vorlauftemperaturkorrektur', dict())
        params['vorlauftemperaturkorrektur']['deviation'] = deviation_dict
        haus.set_module_parameters(params)

        berlin = pytz.timezone("Europe/Berlin")
        now = datetime.now(berlin)
        ch.set('vorlauf_now_time', now, time=600)
